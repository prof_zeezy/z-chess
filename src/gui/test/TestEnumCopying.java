/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.test;

/**
 *
 * @author e.ubachukwu
 */
public class TestEnumCopying {

    public GameEnds gameOver = GameEnds.RESIGNATION;

    public static enum GameEnds {

        CHECKMATE, RESIGNATION, TIMEEXPIRY, DRAW
    }

    public TestEnumCopying() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestEnumCopying t = new TestEnumCopying();
        System.out.println(t.gameOver);
        t.gameOver = GameEnds.CHECKMATE;
        System.out.println(t.gameOver);
        TestEnumCopying tt = new TestEnumCopying();
        tt.gameOver = t.gameOver;
        System.out.println(tt.gameOver);
        tt.gameOver = GameEnds.DRAW;
        System.out.println(tt.gameOver);
        System.out.println(t.gameOver);
    }
    
}
