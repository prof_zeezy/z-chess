/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.components;

import gui.ChessBoardGUI;
import gui.helpers.BoardProperties;
import gui.helpers.ChessPieceImageGetter;
import java.awt.Cursor;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import model.ChessBoard;
import model.chesspiece.ChessPiece;
import model.interfaces.IBoardSquareOwner;

/**
 *
 * @author eziama
 */
public class BoardSquareWidget extends javax.swing.JPanel {

    private SelectionState currentState;
    IBoardSquareOwner owner;
    private boolean mousePressed;
    private ChessBoard board;
    private String label;
    private boolean isForPromotion;
    private JLabel jLabelPieceHolder;

    public enum SelectionState {

        ACTIVATED, HIGHLIGHTED, DEACTIVATED, MARKED
    };

    /**
     * Creates new form BoardSquare
     *
     * @param owner
     * @param board
     * @param label
     */
    public BoardSquareWidget(IBoardSquareOwner owner, ChessBoard board, String label) {
        initComponents();
        currentState = SelectionState.DEACTIVATED;
        this.owner = owner;
        this.board = board;
        this.label = label;
        
        initBoardSquareJLabel();
    }
    
    /**
     * For dealing with promotion pieces
     *
     * @param piece
     * @param owner
     */
    public BoardSquareWidget(IBoardSquareOwner owner, ChessPiece piece) {
        initComponents();
        this.owner = owner;
        this.currentState = SelectionState.DEACTIVATED;
        this.isForPromotion = true;
        initBoardSquareJLabel();
        renderPiece(piece);
    }

    private void initBoardSquareJLabel(){        

        jLabelPieceHolder = new javax.swing.JLabel() {
            @Override
            protected void paintComponent(Graphics g) {
                g.setColor(getBackground());
                super.paintComponent(g);
                g.fillRect(0, 0, getWidth(), getHeight());
                super.paintComponent(g);
            }

        };

        setLayout(new java.awt.BorderLayout());

        jLabelPieceHolder.setBackground(new java.awt.Color(255, 255, 255));
        jLabelPieceHolder.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelPieceHolder.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelPieceHolderMouseClicked(evt);
            }

            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabelPieceHolderMouseEntered(evt);
            }

            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabelPieceHolderMousePressed(evt);
            }

            @Override
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabelPieceHolderMouseReleased(evt);
            }
        });
        add(jLabelPieceHolder, java.awt.BorderLayout.CENTER);
        updateInterface();
    }

    public final void updateInterface() {
        jLabelPieceHolder.setOpaque(false);
        switch (currentState) {
            case ACTIVATED:
                // put border
                jLabelPieceHolder.setBackground(BoardProperties.squareHighlightColor);
                jLabelPieceHolder.setBorder(BoardProperties.squareSelectedBorder);
                jLabelPieceHolder.setOpaque(true);
                break;
            case DEACTIVATED:
                // remove border
                jLabelPieceHolder.setBorder(null);
                jLabelPieceHolder.setBackground(BoardProperties.squareTransparentColor);
                break;
            case HIGHLIGHTED:
                // put border
                jLabelPieceHolder.setBorder(BoardProperties.squareHighlightBorder);
                break;
            case MARKED:
                // transparent orange color
                jLabelPieceHolder.setBackground(BoardProperties.squareMarkedColor);
                break;
        }
        if (board != null) {
            ChessPiece piece = board.SQUARES.get(label).getPiece();
            renderPiece(piece);
        }
        // Removing the line below causes all manner of random visual artifacts to appear
        // on the board (because of the transparency)
        // Took me hours to figure out; retrospectively I should have known,
        // but maybe cause without the transparency everything worked fine, so I didn't
        // expect a difference.
        this.repaint();
    }

    private void renderPiece(ChessPiece piece) {
        jLabelPieceHolder.setIcon(null);
        if (piece != null) {
            String fn = ChessPieceImageGetter.getImageFilename(piece);
            try {
                ImageIcon i = new ImageIcon(getClass()
                        .getResource("/images/chess_pieces/x48/" + fn));
                jLabelPieceHolder.setIcon(i);
            }
            catch (Exception ex) {
                System.err.println("Can't find file " + fn);
            }
        }
    }

    @Override
    public final String toString() {
        return label;
    }

    public void setChessBoard(ChessBoard board) {
        this.board = board;
    }

    public SelectionState getCurrentState() {
        return currentState;
    }

    public boolean isActivated() {
        return currentState == SelectionState.ACTIVATED;
    }

    public boolean isDeactivated() {
        return currentState == SelectionState.DEACTIVATED;
    }

    public boolean isHighlighted() {
        return currentState == SelectionState.HIGHLIGHTED;
    }

    public void activate() {
        setCurrentState(SelectionState.ACTIVATED);
    }

    public void deactivate() {
        setCurrentState(SelectionState.DEACTIVATED);
    }

    public void highlight() {
        setCurrentState(SelectionState.HIGHLIGHTED);
    }

    public void unhighlight() {
        setCurrentState(SelectionState.DEACTIVATED);
    }

    /**
     * For showing squares that a piece can move to
     */
    public void mark() {
        setCurrentState(SelectionState.MARKED);
    }

    /**
     * Provides a central point for calling update()
     *
     * @param currentState
     */
    private void setCurrentState(SelectionState currentState) {
        this.currentState = currentState;
        updateInterface();
    }

    private void jLabelPieceHolderMouseEntered(java.awt.event.MouseEvent evt) {
        if (board != null && board.gameOver == null) {
            int cursor = board.SQUARES.get(label).hasPiece() ? Cursor.HAND_CURSOR : Cursor.DEFAULT_CURSOR;
            this.setCursor(Cursor.getPredefinedCursor(cursor));
        }
        else if (isForPromotion) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }
    }

    /**
     * This became necessary since mouseclick event requires the pointer to not
     * move between press and release, which in turn hurts user interaction This
     * sets a flag on this board square, which is checked by mousereleased to
     * know if to trigger the clicked event.
     *
     * @param evt
     */
    private void jLabelPieceHolderMousePressed(java.awt.event.MouseEvent evt) {
        // release any other pending pressed flags
        for (BoardSquareWidget w : ChessBoardGUI.SQUARE_WIDGETS.values()) {
            w.mousePressed = false;
        }
        // Set flag on this
        mousePressed = true;
    }

    private void jLabelPieceHolderMouseReleased(java.awt.event.MouseEvent evt) {
        if (mousePressed && board != null && board.gameOver == null) {
            owner.boardSquareClicked(this, false);
            mousePressed = false;
        }
    }

    private void jLabelPieceHolderMouseClicked(java.awt.event.MouseEvent evt) {
        // this is for promotions, where the UI issues won't matter
        if (isForPromotion) {
            owner.boardSquareClicked(this, false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setLayout(new java.awt.BorderLayout());
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
