/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import gui.components.BoardSquareWidget;
import gui.components.JDialogPromotion;
import gui.components.NewGameDialog;
import gui.components.Options;
import gui.helpers.BoardProperties;
import gui.helpers.ChessPieceImageGetter;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import model.BoardSquare;
import model.ChessBoard;
import model.ChessGameSetup;
import model.MovePly;
import model.MovePlyProposal;
import model.chesspiece.ChessPiece;
import model.intelligence.ComputerPlayer;
import model.interfaces.IBoardSquareOwner;
import model.interfaces.IMoveListener;
import model.interfaces.IUserInteractionEndpoint;
import util.Util;

/**
 *
 * @author eziama
 */
public class ChessBoardGUI extends javax.swing.JFrame implements IBoardSquareOwner,
        IUserInteractionEndpoint, IMoveListener {

    public static final int SQUARE_WIDTH = 80;
    public static final int SQUARE_WIDTH_SMALL = 35;
    public static int REPETITION_LIMIT = 3; // set to 0 to turn off
    public static Map<String, BoardSquareWidget> SQUARE_WIDGETS;
    private ChessBoard board;
    private Timer gameTimer;
    private static Timer statusBarMessageTimer;
    private ChessGameSetup gameSetup;
    private boolean moveInProgress;
    private final ImageIcon green;
    private final ImageIcon red;
    private ComputerPlayer computerPlayer;

    /**
     * Creates new form ChessGame
     */
    public ChessBoardGUI() {
        initComponents();
        // produce the chess board
        // produce the board squares
        SQUARE_WIDGETS = new LinkedHashMap<>();
        for (int i = 0; i < 64; i++) {
            // the squares are filled from top to bottom, from rank 8 to 1, file A to H
            String s = (char) ('A' + (i % 8)) + "" + (8 - (int) (i / 8));
            BoardSquareWidget squarePanel = new BoardSquareWidget(this, board, s);
            // x and y positions of the squares
            int x = (i % 8) * SQUARE_WIDTH;
            int y = (i / 8) * SQUARE_WIDTH;
            int pad = (i / 8);
            Color bg = ((i % 2) + pad) % 2 == 0 ? BoardProperties.squareLightColor
                       : BoardProperties.squareDarkColor;
            squarePanel.setBackground(bg);
            jPanelChessBoard.add(squarePanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(x, y, SQUARE_WIDTH, SQUARE_WIDTH));
            SQUARE_WIDGETS.put(s, squarePanel);
        }
        statusBarMessageTimer = new Timer(5000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jLabelStatusInfo.setText("");
                jLabelStatusInfo.setForeground(Color.black);
            }
        });
        green = (ImageIcon) jLabelWhiteTurnIndicator.getIcon();
        red = (ImageIcon) jLabelBlackTurnIndicator.getIcon();
        statusBarMessageTimer.setRepeats(false);
        this.setExtendedState(MAXIMIZED_BOTH);
    }

    /**
     * The callback function for the squares being clicked. It is synchronised
     * because from within it, the computer's move is requested after the move
     * just finishing. The computer uses a new thread to think, so the call for
     * moves within this function returns quickly. If the computer gets a new
     * move too quickly before the current thread is done, and since this
     * function is called again when the computer generates a move, it is
     * possible that the computer is ready to make another move
     *
     * @param targetSquareWidget
     * @param isComputerMove
     */
    @Override
    public void boardSquareClicked(BoardSquareWidget targetSquareWidget, boolean isComputerMove) {
        // If it's the computer's turn, and a human clicks (identified by isComputerPlayer = false),
        // then no-op it. Basically disable human interaction while the computer thinks.
        if (!isComputerMove) {
            if (isComputerTurn() || moveInProgress) {
                // computer is playing white or black and it's its turn, and this method didn't 
                // come as a human interaction
                return;
            }
        }
        BoardSquare targetSquare = board.SQUARES.get(targetSquareWidget.toString());
        ChessPiece piece = targetSquare.getPiece();
        // check if another is selected already, get the currently active square
        // by design it must have a piece if active
        BoardSquareWidget previouslyActiveSquare = null;
        List<BoardSquareWidget> highlightedSquares = new ArrayList<>();
        for (BoardSquareWidget s : SQUARE_WIDGETS.values()) {
            if (s.isActivated()) {
                previouslyActiveSquare = s;
            }
            if (s.isHighlighted()) {
                // deactivate to remove 'marked', in case it's both highlighted and marked
                s.deactivate();
                s.highlight();
                highlightedSquares.add(s);
                continue;
            }
            s.deactivate();
        }
        // simply deactivate if already activated
        if (targetSquareWidget == previouslyActiveSquare) {
            return;
        }
        // in the case where a valid square is clicked for the current user's turn
        // activate and return, irrespective of if another square was active
        if (targetSquare.hasPiece() && piece.getSide() == board.getSideToMove()) {
            targetSquareWidget.activate();
            // add the possible moves highlight, if enabled
            if (jCheckBoxHighlightMoves.isSelected() && !isComputerMove) {
                for (MovePlyProposal mp : board.getPossibleMovesByPiece(targetSquare)) {
                    BoardSquareWidget bsw = SQUARE_WIDGETS.get(mp.getTo());
                    bsw.mark();
                }
            }
            return;
        }
        if (previouslyActiveSquare == null) { // no square was previously active
            return;
        }
        String from = previouslyActiveSquare.toString(),
                to = targetSquareWidget.toString();

        String time = "";
        if (isTimedGame()) {
            time = board.getSideToMove() == ChessPiece.Side.WHITE
                   ? jLabelWhiteTimer.getText() : jLabelBlackTimer.getText();
        }
        boolean result = board.movePiece(new MovePlyProposal(from, to), time);
        if (!result) {
            logOutput(board.getMessage());
            displayTimedMessage(board.getMessage());
        }
        else {
            // check if timed game
            if (gameTimer != null && !gameTimer.isRunning()) {
                // start the timer after white moves
                gameTimer.start();
            }
            // highlight for the current move
            for (BoardSquareWidget bs : highlightedSquares) {
                bs.deactivate();
            }
            // show the necessary UI stuff
            SQUARE_WIDGETS.get(from).highlight();
            SQUARE_WIDGETS.get(to).highlight();
            updateInterface();
            
            // check for game over
            if (board.gameOver != null) {
                endGame(board.gameOver);
                return;
            }
            else if (board.isKingCheck()) {
                logOutput(board.getKingCheckInformation());
                if (board.getSideToMove() == ChessPiece.Side.BLACK) {
                    jLabelBlackNotice.setText(board.getWarning());
                }
                else {
                    jLabelWhiteNotice.setText(board.getWarning());
                }
            }
        }
        // in any case, check if to make computer make the next move
        // this is necessary to be here instead of within chessMoveProposed()'s timer
        // since a human player needs to trigger the computer
        if (isComputerTurn()) {
            requestMoveFromComputer();
        }
    }

    private void requestMoveFromComputer() {
        assert isComputerTurn() : "ERROR! Computer move requested when it's not computer's turn.";
        String[] text = new String[4];
        text[0] = "Thinking...";
        text[1] = "Please wait...";
        text[2] = "Calculating...";
        text[3] = "Calm down bro...";
        String t = text[new Random().nextInt(text.length)];
        if (board.getSideToMove() == ChessPiece.Side.WHITE) {
            jLabelWhiteThinking.setText(t);
        }
        else {
            jLabelBlackThinking.setText(t);
        }
        computerPlayer.initiate(board);
//        jTreeMoveAnalysis.setModel(new DefaultTreeModel(p.getMoveTree().getRoot()));
    }

    /**
     * Allows the computer's generated moves to play out like human moves on the
     * board. The computer calls this method when it's ready with a move,
     * basically implementing the observer pattern.
     *
     * @param move
     */
    @Override
    synchronized public void chessMoveProposed(MovePlyProposal move) {
        deactivateBoardSquares();
        jLabelBlackThinking.setText("");
        jLabelWhiteThinking.setText("");
        if (move == null) {
            if (board.gameOver == null) {
                System.out.println("Stalemate.");
                board.gameOver = ChessBoard.GameEnds.DRAW_STALEMATE;
                endGame(ChessBoard.GameEnds.DRAW_STALEMATE);
                return;
            }
            System.err.println("Move should only be NULL if it's stalemate, that is if the computer had"
                    + " to find out by trying to generate an new move and failing. In the REPETITION and"
                    + " CHECKMATE types, the computer already knows by the end of the last move, so it"
                    + " never is asked to propose a new move.");
            // end of game 
            if (board.isKingCheck()) {
                // checkmate
                endGame(ChessBoard.GameEnds.CHECKMATE);
            }
            else {
                endGame(ChessBoard.GameEnds.DRAW_STALEMATE);
            }
            return;
        }

        moveInProgress = true;
        // select the square for the move
        boardSquareClicked(SQUARE_WIDGETS.get(move.getFrom()), true);
        // add a slight delay before the actual move is made
        Timer t = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boardSquareClicked(SQUARE_WIDGETS.get(move.getTo()), true);
                moveInProgress = false;
            }
        });
        t.setRepeats(false);
        t.start();
    }

    private void undoMove() {
        // delegate the actual undoing of movement to the chessboard
        board.undoMove();
        // show the highlights 
        // repaint current last move on board
        if (!board.getMovePlyHistory().isEmpty()) {
            MovePly m = (MovePly) board.getMovePlyHistory().peek();
            if (m != null) {
                deactivateBoardSquares();
                SQUARE_WIDGETS.get(m.getTo()).highlight();
                SQUARE_WIDGETS.get(m.getFrom()).highlight();
            }
        }
        updateInterface();
    }

    private void redoMove() {
        board.setRedoMoveInProgress(true);
        MovePly m = board.redoMove();
        if (m == null) {
            ChessBoardGUI.displayTimedMessage("No moves to redo.");
        }
        else {
            // use boardsquareclicked to include highlighting
            deactivateBoardSquares();
            boardSquareClicked(SQUARE_WIDGETS.get(m.getFrom().toString()), false);
            boardSquareClicked(SQUARE_WIDGETS.get(m.getTo().toString()), false);
        }
        jButtonRedoMove.setEnabled(!board.getRedoStack().isEmpty());
        board.setRedoMoveInProgress(false);
    }

    public boolean isComputerTurn() {
        return (board.getSideToMove() == ChessPiece.Side.WHITE && !gameSetup.whiteIsHuman())
               || (board.getSideToMove() == ChessPiece.Side.BLACK && !gameSetup.blackIsHuman());
    }

    @Override
    public ChessPiece getPromotionPiece(ChessPiece.Side side) {
        if (isComputerTurn()) {
            // this is the computer's move to make
            return computerPlayer.getPromotionPiece(side);
        }
        // instantiate all the pieces except king
        return new JDialogPromotion(this, side).launch();
    }

    private void initializeGame(ChessGameSetup setup) {
        initializeGame(setup, new ChessBoard(this));
    }

    private void initializeGame(ChessGameSetup setup, ChessBoard board) {
        this.gameSetup = setup;
        this.board = board;
        deactivateBoardSquares();

        // reset the history paraphernalia
        resetHistory();
        updateInterface();
        jTextAreaOutput.setText("");
        jLabelBlackThinking.setText("");
        jLabelWhiteThinking.setText("");

        computerPlayer = null;

        if (!gameSetup.whiteIsHuman() || !gameSetup.blackIsHuman()) {
            computerPlayer = new ComputerPlayer(this);
        }
        // reset labels for timers and stop timer if running
        resetTimer();

        // create the tree for the move analyser
        DefaultMutableTreeNode node = new DefaultMutableTreeNode("root");
//        DefaultTreeModel treeModel = new DefaultTreeModel(node);
//        jTreeMoveAnalysis.setModel(treeModel);
        if (isComputerTurn()) {
            requestMoveFromComputer();
        }
    }

    private void updateInterface() {
        boolean isWhiteTurn = board.getSideToMove() == ChessPiece.Side.WHITE;
        if (isWhiteTurn) {
            jLabelWhiteTurnIndicator.setIcon(green);
            jLabelBlackTurnIndicator.setIcon(red);
        }
        else {
            jLabelWhiteTurnIndicator.setIcon(red);
            jLabelBlackTurnIndicator.setIcon(green);
        }
        jButtonBlackResign.setEnabled(!isWhiteTurn && gameSetup != null);
        jButtonWhiteResign.setEnabled(isWhiteTurn && gameSetup != null);
        // print the board
        printBoard();
        displayCapturedPieces();
        clearNotifications();
        jTextAreaMovesHistory.setText(board.getMovesAnnotation());
        logOutput(board.getMessage());
        // activate undo
        jButtonRedoMove.setEnabled(!board.getRedoStack().isEmpty());
        jButtonUndoMove.setEnabled(!board.getMovePlyHistory().isEmpty());
        /**
         * Repaints the board squares to reflect new piece positions, e.g. after
         * undoMove()
         */
        for (BoardSquareWidget w : SQUARE_WIDGETS.values()) {
            w.updateInterface();
        }
    }

    private void displayCapturedPieces() {
        // make a copy of the original capture list since we wanna preserve
        // the capture order, and use the copy for display
        List<ChessPiece> copy = new ArrayList<>(board.capturedPieces);
        // sort the list pawns first, irrespective of colour
        Collections.sort(copy, new Comparator<ChessPiece>() {

            @Override
            public int compare(ChessPiece p1, ChessPiece p2) {
                return p1.sortValue(true) == p2.sortValue(true) ? 0 : (int) Math.copySign(1, p1.sortValue(true) - p2.sortValue(true));
            }

        });
        jPanelCapturedPieces.removeAll();
        for (ChessPiece p : copy) {
            JLabel label;
            try {
                ImageIcon icon = new ImageIcon(
                        ChessBoardGUI.class.getResource("/images/chess_pieces/x32/" + ChessPieceImageGetter.getImageFilename(p)));
                label = new JLabel(icon);
            }
            catch (Exception e) {
                System.err.println(e.toString());
                label = new JLabel(p.toShortString());
            }
            label.setPreferredSize(new Dimension(SQUARE_WIDTH_SMALL, SQUARE_WIDTH_SMALL));
            label.setOpaque(false);
            jPanelCapturedPieces.add(label);
        }
        jPanelCapturedPieces.updateUI();
    }

    private void clearNotifications() {
        jLabelBlackNotice.setText("");
        jLabelWhiteNotice.setText("");
    }

    private boolean markTime() {
        // check who to "debit"
        JLabel currentSideTimerLabel = board.getSideToMove() == ChessPiece.Side.WHITE
                                       ? jLabelWhiteTimer : jLabelBlackTimer;
        String[] currentTimeTokens = currentSideTimerLabel.getText().split(":");
        int hours = Integer.parseInt(currentTimeTokens[0]);
        int minutes = Integer.parseInt(currentTimeTokens[1]);
        int seconds = Integer.parseInt(currentTimeTokens[2]);
        if (seconds == 0) {
            if (minutes == 0) {
                if (hours == 0) {
                    // time up
                    gameTimer.stop();
                    JOptionPane.showMessageDialog(this, board.getSideToMove().name() + "'s timer has expired.",
                            "Timer Expired", JOptionPane.INFORMATION_MESSAGE);
                    endGame(ChessBoard.GameEnds.TIMEEXPIRY);

                    return false;
                }
                hours--;
            }
            minutes--;
            if (minutes < 0) {
                minutes = 59;
            }
        }
        seconds--;
        if (seconds < 0) {
            seconds = 59;
        }
        String timeString = String.format("%1$02d:%2$02d:%3$02d", hours, minutes, seconds);
        currentSideTimerLabel.setText(timeString);
        return true;
    }

    private void endGame(ChessBoard.GameEnds reason) {
        String message;
        switch (reason) {
            case CHECKMATE:
                message = "Checkmate! " + board.getSideToMove().otherSide().name() + " wins! ";
                break;
            case RESIGNATION:
                message = board.getSideToMove().name() + " resigns! " + board.getSideToMove().otherSide().name() + " wins!";
                break;
            case DRAW_STALEMATE:
                message = " Draw by Stalemate! " + board.getSideToMove().name() + " has no more moves.";
                break;
            case DRAW_REPETITION:
                message = " Draw by 3-fold repetition!";
                break;
            case TIMEEXPIRY:
                message = board.getSideToMove().name() + "'s timer has expired. "
                          + board.getSideToMove().otherSide().name() + " wins";
                break;
            default:
                // timer expired
                message = board.getSideToMove().name() + "'s timer has expired. " + board.getSideToMove().otherSide().name() + " wins";
                break;
        }
        jLabelWhiteNotice.setText(message);
        jLabelBlackNotice.setText(message);
        stopTimer();
        // set all cursors to be pointer
        for (BoardSquareWidget b : SQUARE_WIDGETS.values()) {
            b.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        jButtonBlackResign.setEnabled(false);
        jButtonWhiteResign.setEnabled(false);
        logOutput("===== " + message + " =====\n");
        JOptionPane.showMessageDialog(this, message, "Game Over!",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Deselect all active squares in preparation for using them again
     */
    private void deactivateBoardSquares() {
        for (BoardSquareWidget s : SQUARE_WIDGETS.values()) {
            s.setChessBoard(board);
            s.deactivate();
        }
    }

    private void resetHistory() {
        board.resetHistory();
        jButtonRedoMove.setEnabled(false);
        jButtonUndoMove.setEnabled(false);
        displayCapturedPieces();
        // reset the notices
        clearNotifications();
        // clear the moves history
        jTextAreaMovesHistory.setText("");

    }

    private void startTimedGame(int hours, int minutes, int seconds) {
        board.initializePieces();
        String timeLabel = String.format("%1$02d:%2$02d:%3$02d", hours, minutes, seconds);
        jLabelBlackTimer.setText(timeLabel);
        jLabelWhiteTimer.setText(timeLabel);
        gameTimer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                markTime();
            }
        });
    }

    public boolean isTimedGame() {
        return gameTimer != null;
    }

    public void resetTimer() {
        stopTimer();
        jLabelBlackTimer.setText("00:00:00");
        jLabelWhiteTimer.setText("00:00:00");
    }

    public void stopTimer() {
        if (gameTimer != null) {
            gameTimer.stop();
        }
        gameTimer = null;
    }

    public static void displayTimedMessage(String message) {
        displayTimedMessage(message, false, 5000);
    }

    public static void displayTimedMessage(String message, boolean isError, int timeOut) {
        if (isError) {
            jLabelStatusInfo.setForeground(Color.red);
        }
        jLabelStatusInfo.setText(message);
        if (message.isEmpty()) {
            // this is a means to clear the error message, so no timeout is needed
            return;
        }
        statusBarMessageTimer.restart();
    }

    public void logOutput(String message) {
        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        jTextAreaOutput.setText(jTextAreaOutput.getText() + "\n[" + date + "]\t" + message);
    }

    /**
     * Prints the board content in a "graphical" way. Lol this part is kinda
     * interesting, but simple.
     */
    public void printBoard() {
        System.out.println(board.print());
    }

    public ChessBoard getBoard() {
        return board;
    }

    public void saveBoard() {
        String playerInfo = gameSetup.whiteIsHuman() ? "H" : "C";
        playerInfo += gameSetup.blackIsHuman() ? "H" : "C";
        Util.writeToFile("logs/board_" + Instant.now().toEpochMilli() + ".txt",
                playerInfo + "\n" + board.serialiseBoard(), false);
    }

    /**
     * This is the component used to set the location of the pawn promotion
     * dialog box
     *
     * @return
     */
    public JPanel getChessBoardPanel() {
        return this.jPanelChessBoard;
    }

    /**
     * This method is called from within the constructor to initializePieces the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jToolBarMain = new javax.swing.JToolBar();
        jButtonUndoMove = new javax.swing.JButton();
        jButtonRedoMove = new javax.swing.JButton();
        jToolBarHighlight = new javax.swing.JToolBar();
        jCheckBoxHighlightMoves = new javax.swing.JCheckBox();
        jButtonSaveBoard = new javax.swing.JButton();
        jScrollPaneBody = new javax.swing.JScrollPane();
        jPanelBody = new javax.swing.JPanel();
        jPanelCapturedPiecesContainer = new javax.swing.JPanel();
        jLabelHeading = new javax.swing.JLabel();
        jScrollPaneCapturedPieces = new javax.swing.JScrollPane();
        jPanelCapturedPieces = new javax.swing.JPanel();
        jPanelChessBoardContainer = new javax.swing.JPanel();
        jPanelChessBoard = new javax.swing.JPanel();
        jPanelRankLabel = new javax.swing.JPanel();
        jLabelRank8 = new javax.swing.JLabel();
        jLabelRank7 = new javax.swing.JLabel();
        jLabelRank6 = new javax.swing.JLabel();
        jLabelRank5 = new javax.swing.JLabel();
        jLabelRank4 = new javax.swing.JLabel();
        jLabelRank3 = new javax.swing.JLabel();
        jLabelRank2 = new javax.swing.JLabel();
        jLabelRank1 = new javax.swing.JLabel();
        jPanelFileLabelTop = new javax.swing.JPanel();
        jLabelFileA = new javax.swing.JLabel();
        jLabelFileB = new javax.swing.JLabel();
        jLabelFileC = new javax.swing.JLabel();
        jLabelFileD = new javax.swing.JLabel();
        jLabelFileE = new javax.swing.JLabel();
        jLabelFileF = new javax.swing.JLabel();
        jLabelFileG = new javax.swing.JLabel();
        jLabelFileH = new javax.swing.JLabel();
        jPanelTurnIndicator = new javax.swing.JPanel();
        jLabelBlackTurnIndicator = new javax.swing.JLabel();
        jLabelWhiteTurnIndicator = new javax.swing.JLabel();
        filler4 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        jLabelWhiteThinking = new javax.swing.JLabel();
        jLabelBlackThinking = new javax.swing.JLabel();
        jLabelWhiteTimer = new javax.swing.JLabel();
        jLabelBlackTimer = new javax.swing.JLabel();
        jButtonBlackResign = new javax.swing.JButton();
        jButtonWhiteResign = new javax.swing.JButton();
        jLabelWhiteNotice = new javax.swing.JLabel();
        jLabelBlackNotice = new javax.swing.JLabel();
        jPanelFileLabelBottom = new javax.swing.JPanel();
        jLabelFileA1 = new javax.swing.JLabel();
        jLabelFileB1 = new javax.swing.JLabel();
        jLabelFileC1 = new javax.swing.JLabel();
        jLabelFileD1 = new javax.swing.JLabel();
        jLabelFileE1 = new javax.swing.JLabel();
        jLabelFileF1 = new javax.swing.JLabel();
        jLabelFileG1 = new javax.swing.JLabel();
        jLabelFileH1 = new javax.swing.JLabel();
        filler3 = new javax.swing.Box.Filler(new java.awt.Dimension(180, 0), new java.awt.Dimension(180, 0), new java.awt.Dimension(100, 32767));
        jPanelDetailedAnnotation = new javax.swing.JPanel();
        jScrollPaneOutput = new javax.swing.JScrollPane();
        jTextAreaOutput = new javax.swing.JTextArea();
        jPanelRightButtons = new javax.swing.JPanel();
        jButtonClearOutput = new javax.swing.JButton();
        jLabelOutputHeading = new javax.swing.JLabel();
        filler5 = new javax.swing.Box.Filler(new java.awt.Dimension(5, 0), new java.awt.Dimension(5, 0), new java.awt.Dimension(5, 32767));
        jPanelAnalysis = new javax.swing.JPanel();
        jLabelAnalysisHeading = new javax.swing.JLabel();
        jScrollPaneMoveAnalysis = new javax.swing.JScrollPane();
        jTreeMoveAnalysis = new javax.swing.JTree();
        jPanelMovesAnnotation = new javax.swing.JPanel();
        jScrollPaneMoves = new javax.swing.JScrollPane();
        jTextAreaMovesHistory = new javax.swing.JTextArea();
        jLabelMovesHeading = new javax.swing.JLabel();
        jButtonSaveMoves = new javax.swing.JButton();
        jPanelBottom = new javax.swing.JPanel();
        jPanelInfo = new javax.swing.JPanel();
        jLabelStatusInfo = new javax.swing.JLabel();
        jLabelStatusDeveloperInfo = new javax.swing.JLabel();
        jMenuBarMainMenu = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemNewGame = new javax.swing.JMenuItem();
        jMenuItemClearBoard = new javax.swing.JMenuItem();
        jMenuActions = new javax.swing.JMenu();
        jMenuItemUndo = new javax.swing.JMenuItem();
        jMenuItemRedo = new javax.swing.JMenuItem();
        jMenuItemSaveBoard = new javax.swing.JMenuItem();
        jMenuItemSaveMoves = new javax.swing.JMenuItem();
        jMenuItemLoadBoard = new javax.swing.JMenuItem();
        jMenuOptions = new javax.swing.JMenu();
        jCheckBoxMenuItemHighlightMoves = new javax.swing.JCheckBoxMenuItem();
        jMenuItemOptions = new javax.swing.JMenuItem();
        jMenuAbout = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Zee Chess Game");
        setMinimumSize(new java.awt.Dimension(800, 650));

        jToolBarMain.setRollover(true);

        jButtonUndoMove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/undo.png"))); // NOI18N
        jButtonUndoMove.setToolTipText("Undo Move");
        jButtonUndoMove.setEnabled(false);
        jButtonUndoMove.setFocusable(false);
        jButtonUndoMove.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonUndoMove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUndoMoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonUndoMove);

        jButtonRedoMove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/redo.png"))); // NOI18N
        jButtonRedoMove.setToolTipText("Redo Move");
        jButtonRedoMove.setEnabled(false);
        jButtonRedoMove.setFocusable(false);
        jButtonRedoMove.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonRedoMove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRedoMoveActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonRedoMove);

        jToolBarHighlight.setRollover(true);

        jCheckBoxHighlightMoves.setSelected(true);
        jCheckBoxHighlightMoves.setText("Highlight moves");
        jCheckBoxHighlightMoves.setToolTipText("Show possible moves");
        jCheckBoxHighlightMoves.setFocusable(false);
        jCheckBoxHighlightMoves.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jToolBarHighlight.add(jCheckBoxHighlightMoves);

        jToolBarMain.add(jToolBarHighlight);

        jButtonSaveBoard.setText("Save board...");
        jButtonSaveBoard.setFocusable(false);
        jButtonSaveBoard.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonSaveBoard.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButtonSaveBoard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveBoardActionPerformed(evt);
            }
        });
        jToolBarMain.add(jButtonSaveBoard);

        getContentPane().add(jToolBarMain, java.awt.BorderLayout.PAGE_START);

        jPanelBody.setLayout(new java.awt.GridBagLayout());

        jPanelCapturedPiecesContainer.setBackground(new java.awt.Color(255, 255, 255));
        jPanelCapturedPiecesContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        jPanelCapturedPiecesContainer.setPreferredSize(new java.awt.Dimension(12, 80));
        jPanelCapturedPiecesContainer.setLayout(new java.awt.GridBagLayout());

        jLabelHeading.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelHeading.setForeground(new java.awt.Color(51, 51, 51));
        jLabelHeading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pixel.png"))); // NOI18N
        jLabelHeading.setText("Captured Pieces");
        jLabelHeading.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(153, 153, 153)));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        gridBagConstraints.weightx = 1.0;
        jPanelCapturedPiecesContainer.add(jLabelHeading, gridBagConstraints);

        jScrollPaneCapturedPieces.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPaneCapturedPieces.setBorder(null);
        jScrollPaneCapturedPieces.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPaneCapturedPieces.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        jScrollPaneCapturedPieces.setFocusable(false);

        jPanelCapturedPieces.setBackground(new java.awt.Color(255, 255, 255));
        jPanelCapturedPieces.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));
        jScrollPaneCapturedPieces.setViewportView(jPanelCapturedPieces);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelCapturedPiecesContainer.add(jScrollPaneCapturedPieces, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelBody.add(jPanelCapturedPiecesContainer, gridBagConstraints);

        jPanelChessBoardContainer.setBackground(new java.awt.Color(64, 37, 37));
        jPanelChessBoardContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jPanelChessBoardContainer.setLayout(new java.awt.GridBagLayout());

        jPanelChessBoard.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jPanelChessBoard.setMinimumSize(new java.awt.Dimension(300, 300));
        jPanelChessBoard.setPreferredSize(new java.awt.Dimension(400, 400));
        jPanelChessBoard.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        jPanelChessBoardContainer.add(jPanelChessBoard, gridBagConstraints);

        jPanelRankLabel.setOpaque(false);
        jPanelRankLabel.setLayout(new java.awt.GridLayout(0, 1));

        jLabelRank8.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank8.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank8.setText("8");
        jLabelRank8.setToolTipText("Rank 8");
        jLabelRank8.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank8.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank8.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank8);

        jLabelRank7.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank7.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank7.setText("7");
        jLabelRank7.setToolTipText("Rank 7");
        jLabelRank7.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank7.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank7.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank7);

        jLabelRank6.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank6.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank6.setText("6");
        jLabelRank6.setToolTipText("Rank 6");
        jLabelRank6.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank6.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank6.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank6);

        jLabelRank5.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank5.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank5.setText("5");
        jLabelRank5.setToolTipText("Rank 5");
        jLabelRank5.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank5.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank5.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank5);

        jLabelRank4.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank4.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank4.setText("4");
        jLabelRank4.setToolTipText("Rank 4");
        jLabelRank4.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank4.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank4.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank4);

        jLabelRank3.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank3.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank3.setText("3");
        jLabelRank3.setToolTipText("Rank 3");
        jLabelRank3.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank3.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank3.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank3);

        jLabelRank2.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank2.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank2.setText("2");
        jLabelRank2.setToolTipText("Rank 2");
        jLabelRank2.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank2.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank2.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank2);

        jLabelRank1.setBackground(new java.awt.Color(51, 51, 51));
        jLabelRank1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelRank1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelRank1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank1.setText("1");
        jLabelRank1.setToolTipText("Rank 1");
        jLabelRank1.setMaximumSize(new java.awt.Dimension(30, 64));
        jLabelRank1.setMinimumSize(new java.awt.Dimension(30, 64));
        jLabelRank1.setPreferredSize(new Dimension(30, ChessBoardGUI.SQUARE_WIDTH));
        jPanelRankLabel.add(jLabelRank1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 7;
        jPanelChessBoardContainer.add(jPanelRankLabel, gridBagConstraints);

        jPanelFileLabelTop.setOpaque(false);
        jPanelFileLabelTop.setLayout(new java.awt.GridLayout(1, 0));

        jLabelFileA.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileA.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileA.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileA.setText("A");
        jLabelFileA.setToolTipText("File A");
        jLabelFileA.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileA.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileA.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileA);

        jLabelFileB.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileB.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileB.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileB.setText("B");
        jLabelFileB.setToolTipText("File B");
        jLabelFileB.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileB.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileB.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileB);

        jLabelFileC.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileC.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileC.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileC.setText("C");
        jLabelFileC.setToolTipText("File C");
        jLabelFileC.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileC.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileC.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileC);

        jLabelFileD.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileD.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileD.setText("D");
        jLabelFileD.setToolTipText("File D");
        jLabelFileD.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileD.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileD.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileD);

        jLabelFileE.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileE.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileE.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileE.setText("E");
        jLabelFileE.setToolTipText("File E");
        jLabelFileE.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileE.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileE.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileE);

        jLabelFileF.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileF.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileF.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileF.setText("F");
        jLabelFileF.setToolTipText("File F");
        jLabelFileF.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileF.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileF.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileF);

        jLabelFileG.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileG.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileG.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileG.setText("G");
        jLabelFileG.setToolTipText("File G");
        jLabelFileG.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileG.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileG.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileG);

        jLabelFileH.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileH.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileH.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileH.setText("H");
        jLabelFileH.setToolTipText("File H");
        jLabelFileH.setMaximumSize(new java.awt.Dimension(64, 30));
        jLabelFileH.setMinimumSize(new java.awt.Dimension(64, 30));
        jLabelFileH.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelTop.add(jLabelFileH);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanelChessBoardContainer.add(jPanelFileLabelTop, gridBagConstraints);

        jPanelTurnIndicator.setOpaque(false);
        jPanelTurnIndicator.setPreferredSize(new java.awt.Dimension(75, 75));
        jPanelTurnIndicator.setLayout(new java.awt.GridBagLayout());

        jLabelBlackTurnIndicator.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelBlackTurnIndicator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/play_turn_stop.png"))); // NOI18N
        jLabelBlackTurnIndicator.setMinimumSize(new java.awt.Dimension(40, 40));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelTurnIndicator.add(jLabelBlackTurnIndicator, gridBagConstraints);

        jLabelWhiteTurnIndicator.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelWhiteTurnIndicator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/play_turn_go.png"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelTurnIndicator.add(jLabelWhiteTurnIndicator, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelTurnIndicator.add(filler4, gridBagConstraints);

        jLabelWhiteThinking.setForeground(new java.awt.Color(224, 224, 224));
        jLabelWhiteThinking.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanelTurnIndicator.add(jLabelWhiteThinking, gridBagConstraints);

        jLabelBlackThinking.setForeground(new java.awt.Color(224, 224, 224));
        jLabelBlackThinking.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 10);
        jPanelTurnIndicator.add(jLabelBlackThinking, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelChessBoardContainer.add(jPanelTurnIndicator, gridBagConstraints);

        jLabelWhiteTimer.setBackground(new java.awt.Color(0, 0, 0));
        jLabelWhiteTimer.setFont(new java.awt.Font("Digital Readout Upright", 1, 24)); // NOI18N
        jLabelWhiteTimer.setForeground(new java.awt.Color(255, 255, 255));
        jLabelWhiteTimer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelWhiteTimer.setText("00:00:00");
        jLabelWhiteTimer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jLabelWhiteTimer.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelChessBoardContainer.add(jLabelWhiteTimer, gridBagConstraints);

        jLabelBlackTimer.setBackground(new java.awt.Color(0, 0, 0));
        jLabelBlackTimer.setFont(new java.awt.Font("Digital Readout Upright", 1, 24)); // NOI18N
        jLabelBlackTimer.setForeground(new java.awt.Color(255, 255, 255));
        jLabelBlackTimer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelBlackTimer.setText("00:00:00");
        jLabelBlackTimer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));
        jLabelBlackTimer.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelChessBoardContainer.add(jLabelBlackTimer, gridBagConstraints);

        jButtonBlackResign.setBackground(new java.awt.Color(64, 37, 37));
        jButtonBlackResign.setText("Resign");
        jButtonBlackResign.setEnabled(false);
        jButtonBlackResign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resignActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelChessBoardContainer.add(jButtonBlackResign, gridBagConstraints);

        jButtonWhiteResign.setBackground(new java.awt.Color(64, 37, 37));
        jButtonWhiteResign.setText("Resign");
        jButtonWhiteResign.setEnabled(false);
        jButtonWhiteResign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resignActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelChessBoardContainer.add(jButtonWhiteResign, gridBagConstraints);

        jLabelWhiteNotice.setForeground(new java.awt.Color(255, 255, 153));
        jLabelWhiteNotice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelChessBoardContainer.add(jLabelWhiteNotice, gridBagConstraints);

        jLabelBlackNotice.setForeground(new java.awt.Color(255, 255, 153));
        jLabelBlackNotice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanelChessBoardContainer.add(jLabelBlackNotice, gridBagConstraints);

        jPanelFileLabelBottom.setOpaque(false);
        jPanelFileLabelBottom.setLayout(new java.awt.GridLayout(1, 0));

        jLabelFileA1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileA1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileA1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileA1.setText("A");
        jLabelFileA1.setToolTipText("File A");
        jLabelFileA1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileA1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileA1);

        jLabelFileB1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileB1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileB1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileB1.setText("B");
        jLabelFileB1.setToolTipText("File B");
        jLabelFileB1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileB1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileB1);

        jLabelFileC1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileC1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileC1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileC1.setText("C");
        jLabelFileC1.setToolTipText("File C");
        jLabelFileC1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileC1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileC1);

        jLabelFileD1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileD1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileD1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileD1.setText("D");
        jLabelFileD1.setToolTipText("File D");
        jLabelFileD1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileD1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileD1);

        jLabelFileE1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileE1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileE1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileE1.setText("E");
        jLabelFileE1.setToolTipText("File E");
        jLabelFileE1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileE1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileE1);

        jLabelFileF1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileF1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileF1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileF1.setText("F");
        jLabelFileF1.setToolTipText("File F");
        jLabelFileF1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileF1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileF1);

        jLabelFileG1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileG1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileG1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileG1.setText("G");
        jLabelFileG1.setToolTipText("File G");
        jLabelFileG1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileG1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileG1);

        jLabelFileH1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabelFileH1.setForeground(new java.awt.Color(255, 150, 23));
        jLabelFileH1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelFileH1.setText("H");
        jLabelFileH1.setToolTipText("File H");
        jLabelFileH1.setMinimumSize(new java.awt.Dimension(50, 30));
        jLabelFileH1.setPreferredSize(new Dimension(ChessBoardGUI.SQUARE_WIDTH, 30));
        jPanelFileLabelBottom.add(jLabelFileH1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        jPanelChessBoardContainer.add(jPanelFileLabelBottom, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelChessBoardContainer.add(filler3, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        jPanelBody.add(jPanelChessBoardContainer, gridBagConstraints);

        jPanelDetailedAnnotation.setLayout(new java.awt.GridBagLayout());

        jTextAreaOutput.setEditable(false);
        jTextAreaOutput.setColumns(20);
        jTextAreaOutput.setLineWrap(true);
        jTextAreaOutput.setRows(5);
        jTextAreaOutput.setWrapStyleWord(true);
        jScrollPaneOutput.setViewportView(jTextAreaOutput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelDetailedAnnotation.add(jScrollPaneOutput, gridBagConstraints);

        jButtonClearOutput.setText("Clear Output");
        jButtonClearOutput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearOutputActionPerformed(evt);
            }
        });
        jPanelRightButtons.add(jButtonClearOutput);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelDetailedAnnotation.add(jPanelRightButtons, gridBagConstraints);

        jLabelOutputHeading.setBackground(new java.awt.Color(0, 0, 0));
        jLabelOutputHeading.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelOutputHeading.setForeground(new java.awt.Color(235, 235, 235));
        jLabelOutputHeading.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelOutputHeading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pixel.png"))); // NOI18N
        jLabelOutputHeading.setText("In-Game Information");
        jLabelOutputHeading.setIconTextGap(6);
        jLabelOutputHeading.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 15;
        gridBagConstraints.weightx = 1.0;
        jPanelDetailedAnnotation.add(jLabelOutputHeading, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanelDetailedAnnotation.add(filler5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 10);
        jPanelBody.add(jPanelDetailedAnnotation, gridBagConstraints);

        jPanelAnalysis.setLayout(new java.awt.GridBagLayout());

        jLabelAnalysisHeading.setBackground(new java.awt.Color(0, 0, 0));
        jLabelAnalysisHeading.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelAnalysisHeading.setForeground(new java.awt.Color(235, 235, 235));
        jLabelAnalysisHeading.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelAnalysisHeading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pixel.png"))); // NOI18N
        jLabelAnalysisHeading.setText("Move Analysis Window");
        jLabelAnalysisHeading.setIconTextGap(6);
        jLabelAnalysisHeading.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 15;
        gridBagConstraints.weightx = 1.0;
        jPanelAnalysis.add(jLabelAnalysisHeading, gridBagConstraints);

        jScrollPaneMoveAnalysis.setViewportView(jTreeMoveAnalysis);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelAnalysis.add(jScrollPaneMoveAnalysis, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 10, 10);
        jPanelBody.add(jPanelAnalysis, gridBagConstraints);

        jPanelMovesAnnotation.setLayout(new java.awt.GridBagLayout());

        jTextAreaMovesHistory.setEditable(false);
        jTextAreaMovesHistory.setBackground(java.awt.SystemColor.controlHighlight);
        jTextAreaMovesHistory.setColumns(20);
        jTextAreaMovesHistory.setRows(10);
        jScrollPaneMoves.setViewportView(jTextAreaMovesHistory);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 1.0;
        jPanelMovesAnnotation.add(jScrollPaneMoves, gridBagConstraints);

        jLabelMovesHeading.setBackground(new java.awt.Color(0, 0, 0));
        jLabelMovesHeading.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabelMovesHeading.setForeground(new java.awt.Color(235, 235, 235));
        jLabelMovesHeading.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabelMovesHeading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pixel.png"))); // NOI18N
        jLabelMovesHeading.setText("Moves History");
        jLabelMovesHeading.setIconTextGap(6);
        jLabelMovesHeading.setOpaque(true);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 15;
        jPanelMovesAnnotation.add(jLabelMovesHeading, gridBagConstraints);

        jButtonSaveMoves.setText("Save...");
        jButtonSaveMoves.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveMovesActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        jPanelMovesAnnotation.add(jButtonSaveMoves, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 0);
        jPanelBody.add(jPanelMovesAnnotation, gridBagConstraints);

        jScrollPaneBody.setViewportView(jPanelBody);

        getContentPane().add(jScrollPaneBody, java.awt.BorderLayout.CENTER);

        jPanelBottom.setPreferredSize(new java.awt.Dimension(10, 30));
        jPanelBottom.setLayout(new java.awt.BorderLayout());

        jLabelStatusInfo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanelInfo.add(jLabelStatusInfo);

        jPanelBottom.add(jPanelInfo, java.awt.BorderLayout.WEST);

        jLabelStatusDeveloperInfo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelStatusDeveloperInfo.setText("Developed by Eziama Ubachukwu. 2017.");
        jPanelBottom.add(jLabelStatusDeveloperInfo, java.awt.BorderLayout.EAST);

        getContentPane().add(jPanelBottom, java.awt.BorderLayout.PAGE_END);

        jMenuFile.setText("File");

        jMenuItemNewGame.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemNewGame.setText("New Game");
        jMenuItemNewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNewGameActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemNewGame);

        jMenuItemClearBoard.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemClearBoard.setText("Clear Board");
        jMenuItemClearBoard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemClearBoardActionPerformed(evt);
            }
        });
        jMenuFile.add(jMenuItemClearBoard);

        jMenuBarMainMenu.add(jMenuFile);

        jMenuActions.setText("Actions");

        jMenuItemUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemUndo.setText("Undo Move");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, jButtonUndoMove, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), jMenuItemUndo, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jMenuItemUndo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemUndoActionPerformed(evt);
            }
        });
        jMenuActions.add(jMenuItemUndo);

        jMenuItemRedo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemRedo.setText("Redo Move");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, jButtonRedoMove, org.jdesktop.beansbinding.ELProperty.create("${enabled}"), jMenuItemRedo, org.jdesktop.beansbinding.BeanProperty.create("enabled"));
        bindingGroup.addBinding(binding);

        jMenuItemRedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRedoActionPerformed(evt);
            }
        });
        jMenuActions.add(jMenuItemRedo);

        jMenuItemSaveBoard.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSaveBoard.setText("Save board");
        jMenuItemSaveBoard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSaveBoardActionPerformed(evt);
            }
        });
        jMenuActions.add(jMenuItemSaveBoard);

        jMenuItemSaveMoves.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemSaveMoves.setText("Save moves");
        jMenuActions.add(jMenuItemSaveMoves);

        jMenuItemLoadBoard.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemLoadBoard.setText("Load board from file...");
        jMenuItemLoadBoard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLoadBoardActionPerformed(evt);
            }
        });
        jMenuActions.add(jMenuItemLoadBoard);

        jMenuBarMainMenu.add(jMenuActions);

        jMenuOptions.setText("Options");

        jCheckBoxMenuItemHighlightMoves.setSelected(true);
        jCheckBoxMenuItemHighlightMoves.setText("Highlight valid moves");
        jCheckBoxMenuItemHighlightMoves.setEnabled(false);
        jCheckBoxMenuItemHighlightMoves.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItemHighlightMovesActionPerformed(evt);
            }
        });
        jMenuOptions.add(jCheckBoxMenuItemHighlightMoves);

        jMenuItemOptions.setText("Options...");
        jMenuItemOptions.setEnabled(false);
        jMenuItemOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemOptionsActionPerformed(evt);
            }
        });
        jMenuOptions.add(jMenuItemOptions);

        jMenuBarMainMenu.add(jMenuOptions);

        jMenuAbout.setText("About");
        jMenuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuAboutActionPerformed(evt);
            }
        });
        jMenuBarMainMenu.add(jMenuAbout);

        setJMenuBar(jMenuBarMainMenu);

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemNewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNewGameActionPerformed
        NewGameDialog dialog = new NewGameDialog(this, true);
        initializeGame(dialog.launch());
    }//GEN-LAST:event_jMenuItemNewGameActionPerformed

    private void jButtonClearOutputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearOutputActionPerformed
        jTextAreaOutput.setText("");
    }//GEN-LAST:event_jButtonClearOutputActionPerformed

    private void jMenuItemOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemOptionsActionPerformed
        Options options = new Options(this, true);
        options.setVisible(true);
    }//GEN-LAST:event_jMenuItemOptionsActionPerformed

    private void jCheckBoxMenuItemHighlightMovesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItemHighlightMovesActionPerformed
        // highlight all valid squares for the current player

    }//GEN-LAST:event_jCheckBoxMenuItemHighlightMovesActionPerformed

    private void resignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resignActionPerformed
        int res = JOptionPane.showConfirmDialog(this, "Do you really want to resign?",
                board.getSideToMove().name(), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                new ImageIcon(getClass().getResource("/images/chess_pieces/x48/"
                                                     + board.getSideToMove().name() + "_king.png")));
        if (res == JOptionPane.YES_OPTION) {
            endGame(ChessBoard.GameEnds.RESIGNATION);
        }
    }//GEN-LAST:event_resignActionPerformed

    private void jMenuItemRedoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRedoActionPerformed
        redoMove();
    }//GEN-LAST:event_jMenuItemRedoActionPerformed

    private void jButtonUndoMoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUndoMoveActionPerformed
        undoMove();
    }//GEN-LAST:event_jButtonUndoMoveActionPerformed

    private void jButtonRedoMoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRedoMoveActionPerformed
        redoMove();
    }//GEN-LAST:event_jButtonRedoMoveActionPerformed

    private void jMenuItemUndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemUndoActionPerformed
        undoMove();
    }//GEN-LAST:event_jMenuItemUndoActionPerformed

    private void jMenuItemClearBoardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemClearBoardActionPerformed
        for (BoardSquare bs : board.SQUARES.values()) {
            bs.placePiece(null);
        }
        updateInterface();
    }//GEN-LAST:event_jMenuItemClearBoardActionPerformed

    private void jMenuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuAboutActionPerformed
        JOptionPane.showMessageDialog(this, "Developed from scratch by @prof_zeezy\n"
                                            + "Copyright Eziama Ubachukwu 2017 ", "About Z-Chess", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuAboutActionPerformed

    private void jButtonSaveBoardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveBoardActionPerformed
        saveBoard();
    }//GEN-LAST:event_jButtonSaveBoardActionPerformed

    private void jButtonSaveMovesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveMovesActionPerformed
        Util.writeToFile("logs/annotation_" + Instant.now().toEpochMilli() + ".txt", board.getMovesAnnotation(), false);
    }//GEN-LAST:event_jButtonSaveMovesActionPerformed

    private void jMenuItemLoadBoardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLoadBoardActionPerformed
        List<String[]> filters = new ArrayList<>();
        filters.add(new String[]{"Text files", "txt"});

        // launch filechooser
        File f = Util.useFileChooserDialog(this, filters, "Select Seerialized Board File", true);
        if (f != null) {
            try {
                String contents = Util.readFileContents(f);
                ChessBoard b = new ChessBoard(contents, this);
                board = b;
                String playerInfo = contents.substring(0, 2);
                gameSetup = new ChessGameSetup(playerInfo.charAt(0) == 'H',
                        playerInfo.charAt(1) == 'H', null, null);
                initializeGame(gameSetup, board);
            }
            catch (Exception ex) {
                Logger.getLogger(ChessBoardGUI.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, "Invalid serialisation format. Please crosscheck.", "Failure!!",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_jMenuItemLoadBoardActionPerformed

    private void jMenuItemSaveBoardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSaveBoardActionPerformed
        saveBoard();
    }//GEN-LAST:event_jMenuItemSaveBoardActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch (ClassNotFoundException ex) {

        }
        catch (InstantiationException ex) {
        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChessBoardGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ChessBoardGUI chessBoardGUI = new ChessBoardGUI();
                chessBoardGUI.setVisible(true);
                NewGameDialog dialog = new NewGameDialog(chessBoardGUI, true);
                chessBoardGUI.initializeGame(dialog.launch());
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.Box.Filler filler3;
    private javax.swing.Box.Filler filler4;
    private javax.swing.Box.Filler filler5;
    private javax.swing.JButton jButtonBlackResign;
    private javax.swing.JButton jButtonClearOutput;
    private javax.swing.JButton jButtonRedoMove;
    private javax.swing.JButton jButtonSaveBoard;
    private javax.swing.JButton jButtonSaveMoves;
    private javax.swing.JButton jButtonUndoMove;
    private javax.swing.JButton jButtonWhiteResign;
    private javax.swing.JCheckBox jCheckBoxHighlightMoves;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItemHighlightMoves;
    private javax.swing.JLabel jLabelAnalysisHeading;
    private javax.swing.JLabel jLabelBlackNotice;
    private javax.swing.JLabel jLabelBlackThinking;
    private javax.swing.JLabel jLabelBlackTimer;
    private javax.swing.JLabel jLabelBlackTurnIndicator;
    private javax.swing.JLabel jLabelFileA;
    private javax.swing.JLabel jLabelFileA1;
    private javax.swing.JLabel jLabelFileB;
    private javax.swing.JLabel jLabelFileB1;
    private javax.swing.JLabel jLabelFileC;
    private javax.swing.JLabel jLabelFileC1;
    private javax.swing.JLabel jLabelFileD;
    private javax.swing.JLabel jLabelFileD1;
    private javax.swing.JLabel jLabelFileE;
    private javax.swing.JLabel jLabelFileE1;
    private javax.swing.JLabel jLabelFileF;
    private javax.swing.JLabel jLabelFileF1;
    private javax.swing.JLabel jLabelFileG;
    private javax.swing.JLabel jLabelFileG1;
    private javax.swing.JLabel jLabelFileH;
    private javax.swing.JLabel jLabelFileH1;
    private javax.swing.JLabel jLabelHeading;
    private javax.swing.JLabel jLabelMovesHeading;
    private javax.swing.JLabel jLabelOutputHeading;
    private javax.swing.JLabel jLabelRank1;
    private javax.swing.JLabel jLabelRank2;
    private javax.swing.JLabel jLabelRank3;
    private javax.swing.JLabel jLabelRank4;
    private javax.swing.JLabel jLabelRank5;
    private javax.swing.JLabel jLabelRank6;
    private javax.swing.JLabel jLabelRank7;
    private javax.swing.JLabel jLabelRank8;
    private static javax.swing.JLabel jLabelStatusDeveloperInfo;
    private static javax.swing.JLabel jLabelStatusInfo;
    private javax.swing.JLabel jLabelWhiteNotice;
    private javax.swing.JLabel jLabelWhiteThinking;
    private javax.swing.JLabel jLabelWhiteTimer;
    private javax.swing.JLabel jLabelWhiteTurnIndicator;
    private javax.swing.JMenu jMenuAbout;
    private javax.swing.JMenu jMenuActions;
    private javax.swing.JMenuBar jMenuBarMainMenu;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenuItem jMenuItemClearBoard;
    private javax.swing.JMenuItem jMenuItemLoadBoard;
    private javax.swing.JMenuItem jMenuItemNewGame;
    private javax.swing.JMenuItem jMenuItemOptions;
    private javax.swing.JMenuItem jMenuItemRedo;
    private javax.swing.JMenuItem jMenuItemSaveBoard;
    private javax.swing.JMenuItem jMenuItemSaveMoves;
    private javax.swing.JMenuItem jMenuItemUndo;
    private javax.swing.JMenu jMenuOptions;
    private javax.swing.JPanel jPanelAnalysis;
    private javax.swing.JPanel jPanelBody;
    private javax.swing.JPanel jPanelBottom;
    private javax.swing.JPanel jPanelCapturedPieces;
    private javax.swing.JPanel jPanelCapturedPiecesContainer;
    private javax.swing.JPanel jPanelChessBoard;
    private javax.swing.JPanel jPanelChessBoardContainer;
    private javax.swing.JPanel jPanelDetailedAnnotation;
    private javax.swing.JPanel jPanelFileLabelBottom;
    private javax.swing.JPanel jPanelFileLabelTop;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelMovesAnnotation;
    private javax.swing.JPanel jPanelRankLabel;
    private javax.swing.JPanel jPanelRightButtons;
    private javax.swing.JPanel jPanelTurnIndicator;
    private javax.swing.JScrollPane jScrollPaneBody;
    private javax.swing.JScrollPane jScrollPaneCapturedPieces;
    private javax.swing.JScrollPane jScrollPaneMoveAnalysis;
    private javax.swing.JScrollPane jScrollPaneMoves;
    private javax.swing.JScrollPane jScrollPaneOutput;
    private javax.swing.JTextArea jTextAreaMovesHistory;
    private javax.swing.JTextArea jTextAreaOutput;
    private javax.swing.JToolBar jToolBarHighlight;
    private javax.swing.JToolBar jToolBarMain;
    private javax.swing.JTree jTreeMoveAnalysis;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

}
