/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.helpers;

import model.chesspiece.ChessPiece;

/**
 *
 * @author e.ubachukwu
 */
public class ChessPieceImageGetter {
    
    public static final String getImageFilename(ChessPiece piece) {
        String filename;        
        String color = piece.getSide().toString();
        
        switch(piece.toShortString().charAt(0)){
            case 'P':
                filename = "pawn.png";
                break;
            case 'N':
                filename ="knight_left.png";
                break;
            case 'B':
                filename ="bishop.png";
                break;
            case 'R':
                filename = "rook.png";
                break;
            case 'Q':
                filename = "queen.png";
                break;
            case 'K':
                filename = "king.png";
                break;
            default:
                filename = null;
        }
        
        return color + "_" + filename;
    }
}
