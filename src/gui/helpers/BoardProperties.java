package gui.helpers;

import java.awt.Color;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * Some general settings for the board appearance
 * @author eziama
 */
public class BoardProperties {

    // colors
    public static Color squareLightColor = Color.WHITE;
    public static Color squareDarkColor = new Color(150, 150, 150);
    public static Color squareMarkedColor = new Color(255, 162, 0, 100);
    public static Color squareTransparentColor = new Color(0, 0, 0, 0);
    public static Color squareHighlightColor = new Color(188, 191, 223);

    // borders
    public static Border squareSelectedBorder = new LineBorder(Color.blue);
    public static Border squareHighlightBorder = new LineBorder(new Color(200, 100, 100), 2);

}
