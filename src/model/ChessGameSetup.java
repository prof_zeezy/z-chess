/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author e.ubachukwu
 */
public class ChessGameSetup {

    boolean whiteIsHuman;
    boolean blackIsHuman;
    String whiteTimer;
    String blackTimer;
    int whiteSeconds;
    int blackSeconds;
    boolean whiteHasTimer;
    boolean blackHasTimer;

    public ChessGameSetup(boolean whiteIsHuman, boolean blackIsHuman, String whiteTimer, String blackTimer) {
        this.whiteIsHuman = whiteIsHuman;
        this.blackIsHuman = blackIsHuman;
        this.whiteTimer = whiteTimer;
        this.blackTimer = blackTimer;
        if (whiteTimer == null || whiteTimer.isEmpty()) {
            this.whiteTimer = "0:0:0";
        }
        if (blackTimer == null || blackTimer.isEmpty()) {
            this.blackTimer = "0:0:0";
        }
        Pattern p = Pattern.compile("(?<wH>\\d+):(?<wM>\\d+):(?<wS>\\d+)--(?<bH>\\d+):(?<bM>\\d+):(?<bS>\\d+)");
        Matcher m = p.matcher(whiteTimer + "--" + blackTimer);
        if (m.find()) {
            whiteSeconds = Integer.parseInt(m.group("wH")) * 60 * 60 + Integer.parseInt(m.group("wM")) * 60 + Integer.parseInt(m.group("wS")) * 60;
            blackSeconds = Integer.parseInt(m.group("bH")) * 60 * 60 + Integer.parseInt(m.group("bM")) * 60 + Integer.parseInt(m.group("bS")) * 60;
        }
        whiteHasTimer = whiteSeconds > 0;
        blackHasTimer = blackSeconds > 0;
    }

    public boolean whiteIsHuman() {
        return whiteIsHuman;
    }

    public boolean blackIsHuman() {
        return blackIsHuman;
    }

    public void setWhiteTimer(String whiteTimer) {
        this.whiteTimer = whiteTimer;
    }

    public void setBlackTimer(String blackTimer) {
        this.blackTimer = blackTimer;
    }

    public void setWhiteSeconds(int whiteSeconds) {
        this.whiteSeconds = whiteSeconds;
    }

    public void setBlackSeconds(int blackSeconds) {
        this.blackSeconds = blackSeconds;
    }

    public void setWhiteHasTimer(boolean whiteHasTimer) {
        this.whiteHasTimer = whiteHasTimer;
    }

    public void setBlackHasTimer(boolean blackHasTimer) {
        this.blackHasTimer = blackHasTimer;
    }

}
