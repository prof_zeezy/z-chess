/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Collection;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.chesspiece.ChessPiece;
import model.interfaces.IUserInteractionEndpoint;
import util.Util;

/**
 *
 * @author e.ubachukwu
 */
public class MovePlayer implements IUserInteractionEndpoint {

    private Stack<MovePly> moves;
    private Stack<MovePly> movesPlayed;
    private ChessBoard board;

    public MovePlayer(Stack<MovePly> moves) {
        this.moves = moves;
    }

    public MovePlayer(String movesText) {
        this.board = new ChessBoard(this);
        this.moves = new Stack<>();
        this.movesPlayed = new Stack<>();
        // parse the text
        // the moves are recorded as letter followed by other characters,
        // two moves per line separated by whitespace
        // 1. d5 d6 2. e5 cxb5 e.p.
        Pattern p = Pattern.compile("(?:(?<number>\\d+)\\.?\\s+)?(?<move>\\S+(\\d|O)(=[PNBRQ])?)(?<check>\\+|#)?(?:\\s*(?<ep>e\\.?p\\.?))?\\s*+");
        Matcher m = p.matcher(movesText);
        while (m.find()) {
            ChessPiece.Side side = m.group("number") != null
                                   ? ChessPiece.Side.WHITE : ChessPiece.Side.BLACK;
            boolean isCheck = false, isCheckmate = false;
            if (m.group("check") != null) {
                isCheckmate = m.group("check").equals("#");
                isCheck = !isCheckmate;
            }
            parseStringIntoMove(m.group("move"), side, isCheck, m.group("ep") != null, isCheckmate);
        }
    }

    private void parseStringIntoMove(String moveString, ChessPiece.Side side, boolean isCheck, boolean isEnpassant,
                                     boolean isCheckmate) {
        Pattern p = Pattern.compile("(?<castle>O-O(-O)?)|((?<piece>[KQRBN])?(?<source>[a-h1-8]{0,2})(?<times>x)?(?<dest>[a-h][1-8])(=(?<promo>[A-Z]))?)");
        Matcher m = p.matcher(moveString.trim());
        if (m.find()) {
            System.out.println(m.group(0));
            // get the first character
            ChessPiece piece;
            String to, from;
            if (m.group("castle") != null) {
                boolean kingSide = m.group("castle").length() == 3;
                boolean isWhite = side == ChessPiece.Side.WHITE;
                String rank = isWhite ? "1" : "8";
                from = "e" + rank;
                to = kingSide ? "g" + rank : "c" + rank;
                piece = ChessPieceFactory.createPiece("K", side);
            }
            else {
                to = m.group("dest");
                from = m.group("source");
                piece = ChessPieceFactory.createPiece(m.group("piece"), side);
            }
            BoardSquare toSquare = board.getSquareForPosition(to);
            // List of possible move formats, for convenience:
            // Pawn: d5, exd5
            // others: N(x)g6, Nd(x)g6, N3(x)g6, Nd3(x)g6,
            // King: O-O, O-O-O
            // reverse-engineer the move by:
            // 1. Where could the piece have come from?
            Collection<BoardSquare> squares;
            // if that move wasn't a capture, then I should check for "moveable pieces"
            // and not "attacking pieces" 
            ChessPiece captured = null;
            if (m.group("times") != null) {
                squares = board.getAttackingPiecePositions(toSquare, side);
                // get the piece that was captured by looking at history
                // search the stack as a list, backwards (basically in pop() order)
                for (int i = moves.size() - 1; i >= 0; i--) {
                    MovePly ply = moves.get(i);
                    if (ply.to.equals(to)) {
                        captured = ply.getPiece();
                    }
                }
            }
            else {
                squares = board.getMoveablePieces(toSquare, side).values();
            }
            // check for promotion
            ChessPiece promoPiece = null;
            if (m.group("promo") != null) {
                promoPiece = ChessPieceFactory.createPiece(m.group("promo"), side);
            }
            boolean found = false;
            // filter the results based on the piece we need
            for (BoardSquare sq : squares) {
                ChessPiece cp = sq.getPiece();
                if (cp.getClass() == piece.getClass()) {
                    // likely candidate, but could be multiple pieces of the same type
                    if (from != null && !from.isEmpty()) {
                        if (from.length() == 1 && to.contains(from.toUpperCase())) {
                            // we only have one other contender
                            found = true;
                        }
                        else if (from.toUpperCase().contentEquals(to)) {
                            // we had more than two contenders, and this is an exact location
                            // match
                            found = true;
                        }
                    }
                    else {
                        found = true;
                    }
                }
                if (found) {
                    from = sq.toString();
                    break;
                }
            }
            assert found == true : "A piece must be found that moved to the square";
            MovePly move = new MovePly(from, to, piece, captured, isCheck, isEnpassant, isCheckmate);
            move.setPromotionPiece(promoPiece);
            move.setDistinguisher(m.group("source"));
            moves.add(move);
            // make the move on the board
            //        board.move()
        }
    }

    public void stepForward() {

    }

    public void stepBackward() {

    }

    public void jumpTo(int moveNumber, boolean isWhiteMove) {

    }

    public void goToBeginning() {

    }

    public void goToEnd() {

    }

    public static void main(String[] args) {
        String regex = "1. e4+ e.p. e6 2. d4 d5 3. Nc3 Bb4e.p. 4. Qxd5 h8=Q 5. Qe2# Be5+ 6. O-O O-O-O";
        MovePlayer pl = new MovePlayer(regex);
        Pattern p = Pattern.compile("(?<castle>O-O(-O)?)|((?<piece>[KQRBN])?(?<source>[a-h1-8]{0,2})(?<times>x)?(?<dest>[a-h][1-8])(=(?<promo>[A-Z]))?)");
        Matcher m = p.matcher(regex);
        System.out.println(Util.printList(pl.moves));
    }

    @Override
    public ChessPiece getPromotionPiece(ChessPiece.Side side) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
