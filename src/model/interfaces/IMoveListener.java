/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

import model.MovePlyProposal;

/**
 *
 * @author e.ubachukwu
 */
public interface IMoveListener {

    public void chessMoveProposed(MovePlyProposal move);
}
