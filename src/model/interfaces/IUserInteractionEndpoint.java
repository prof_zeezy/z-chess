/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.interfaces;

import model.chesspiece.ChessPiece;

/**
 * Handles user interaction that lets the player select a piece for promotion
 * @author e.ubachukwu
 */
public interface IUserInteractionEndpoint {
    public abstract ChessPiece getPromotionPiece(ChessPiece.Side side);
}
