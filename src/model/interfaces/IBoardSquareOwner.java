package model.interfaces;

import gui.components.BoardSquareWidget;

/**
 * An interface that unites all objects that contain board squares, notably the 
 * JDialogPromotion and ChessBoard objects. This interface enforces the contract
 * that all such objects should handle the click event for their board squares.
 * @author eziama
 */
public interface IBoardSquareOwner {

    public void boardSquareClicked(BoardSquareWidget target, boolean isComputerPlayer);
}
