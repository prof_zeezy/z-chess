package model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import model.chesspiece.ChessPiece;
import model.chesspiece.King;
import model.chesspiece.Pawn;

/**
 *
 * @author eziama
 */
public class MovePly extends MovePlyProposal {

    private final ChessPiece piece;
    private final boolean isCapture;
    private final boolean isKingCheck;
    private final boolean isCheckmate;
    private final boolean isEnpassant;
    private boolean isQueensideCastling;
    private boolean isKingsideCastling;
    private final ChessPiece capturedPiece;
    // for when two identical pieces of the same side can make the same move,
    // use either the file or rank to distinguish them. It applies to only
    // rooks and knights.
    private String distinguisher;
    // for recording promotion
    private ChessPiece promotionPiece;
    private String timeOfMove;

    public MovePly(String fromSquare, String toSquare, ChessPiece piece,
                     ChessPiece capturedPiece, boolean isKingCheck, boolean isEnpassant, boolean isCheckmate) {
        super(fromSquare, toSquare);
        this.piece = piece;
        this.isCapture = capturedPiece != null;
        this.capturedPiece = capturedPiece;
        this.isKingCheck = isKingCheck;
        this.isCheckmate = isCheckmate;
        this.isEnpassant = isEnpassant;
        this.distinguisher = "";
        this.promotionPiece = null;
        this.timeOfMove = (LocalDateTime.now()).format(DateTimeFormatter.ISO_DATE_TIME);
        this.isQueensideCastling = false;
        this.isKingsideCastling = false;
//        this.isCastlingBitMask = 0;
        if (piece instanceof King) {
            int fileDiff = fromSquare.charAt(0) - toSquare.charAt(0);
            boolean isCastlingMove = Math.abs(fileDiff) == 2
                                     && (fromSquare.charAt(1) == toSquare.charAt(1));
            if (isCastlingMove) {
                if (fileDiff == 2) {
                    // this is queenside castling
                    isQueensideCastling = true;
                }
                else if (fileDiff == -2) {
                    // this is queenside castling
                    isKingsideCastling = true;
                }
            }
        }
    }

    public boolean isCastling() {
        return isQueensideCastling || isKingsideCastling;
    }

    public boolean isQueensideCastling() {
        return isQueensideCastling;
    }

    public boolean isKingsideCastling() {
        return isKingsideCastling;
    }

    public boolean isEnpassant() {
        return isEnpassant;
    }

    public ChessPiece getPiece() {
        return piece;
    }

    public ChessPiece getCapturedPiece() {
        return capturedPiece;
    }

    public ChessPiece.Side getSide() {
        return piece.getSide();
    }

    public boolean isKingCheck() {
        return isKingCheck;
    }

    public boolean isCapture() {
        return isCapture;
    }

    public boolean isCheckMate() {
        return isCheckmate;
    }

    public boolean isPromotion() {
        return promotionPiece != null;
    }

    public void setDistinguisher(String distinguisher) {
        this.distinguisher = distinguisher;
    }

    public void setPromotionPiece(ChessPiece piece) {
        promotionPiece = piece;
    }

    public void setTimeOfMove(String timeOfMove) {
        this.timeOfMove = timeOfMove;
    }

    public String getTimeOfMove() {
        return timeOfMove;
    }

    @Override
    public String toString() {
        String kc = isKingCheck ? isCheckmate ? "#" : "+" : "";
        if (isKingsideCastling()) {
            return "O-O" + kc;
        }
        else if (isQueensideCastling()) {
            return "O-O-O" + kc;
        }
        String c = isCapture ? "x" : "";
        String ep = isEnpassant ? " e.p." : "";
        String _to = this.to.toLowerCase();
        String _from = this.from.toLowerCase();
        String p = piece.toShortString().substring(0, 1);
        StringBuilder s = new StringBuilder();
        if (piece instanceof Pawn) {
            if (isCapture) {
                // we record only the file of the pawn
                s.append(_from.substring(0, 1)).append(c);
            }
            s.append(_to);
            if (isPromotion()) {
                s.append("=").append(promotionPiece.toShortString().substring(0, 1));
            }
        }
        else {
            s.append(p).append(distinguisher).append(c).append(_to);
        }
        return s.append(kc).append(ep).toString();
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.from);
        hash = 37 * hash + Objects.hashCode(this.to);
        hash = 37 * hash + Objects.hashCode(this.piece);
        return hash;
    }

}
