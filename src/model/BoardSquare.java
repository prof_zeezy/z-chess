/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.chesspiece.ChessPiece;

/**
 *
 * @author e.ubachukwu
 */
public class BoardSquare {

    private char file;
    private byte rank;
    private ChessPiece piece;

    public BoardSquare(char file, byte rank, ChessPiece piece) {
        this.file = file;
        this.rank = rank;
        this.piece = piece;
    }

    public BoardSquare(char file, byte rank) {
        this.file = file;
        this.rank = rank;
    }

    public BoardSquare(ChessPiece piece) {
        this.piece = piece;
    }

    public BoardSquare(BoardSquare clonedSquare) {
        this.file = clonedSquare.file;
        this.rank = clonedSquare.rank;
        this.piece = clonedSquare.getPiece();
    }

    public char getFile() {
        return file;
    }

    public byte getRank() {
        return rank;
    }

    public ChessPiece getPiece() {
        return piece;
    }

    public ChessPiece removePiece() {
        ChessPiece former = this.piece;
        this.piece = null;
        return former;
    }

    public ChessPiece placePiece(ChessPiece piece) {
        ChessPiece former = this.piece;
        this.piece = piece;
        return former;
    }

    public boolean hasPiece() {
        return piece != null;
    }

    @Override
    public final String toString() {
        return file + "" + rank;
    }

    public final String toStringWithPiece() {
        return toString() + (this.piece != null? " > " + this.piece.toShortString(): "");
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof BoardSquare)){
            return false;
        }
        return obj.hashCode() == this.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.file;
        hash = 89 * hash + this.rank;
        return hash;
    }
    
}
