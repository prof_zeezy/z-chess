/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;
import model.chesspiece.Bishop;
import model.chesspiece.ChessPiece;
import model.chesspiece.King;
import model.chesspiece.Knight;
import model.chesspiece.Pawn;
import model.chesspiece.Queen;
import model.chesspiece.Rook;
import model.interfaces.IUserInteractionEndpoint;
import util.Util;

/**
 *
 * @author e.ubachukwu
 */
public class ChessBoard {

    public Map<String, BoardSquare> SQUARES = new LinkedHashMap<>(64, 1);
    private final List<BoardSquare>[] positionsForSides = new ArrayList[2];
    public Stack<ChessPiece> capturedPieces = new Stack<>();
    private Stack<MovePly> redoStack = new Stack<>();
    private final List<MovePlyProposal>[] checkResolutionMoves = new ArrayList[3];
    private Stack<MovePlyProposal> moveHistory = new Stack<>();

    private StringBuilder movesAnnotation = new StringBuilder();

    public static final int MIN_RANK = 1;
    public static final int MAX_FILE = 'H';
    public static final int MIN_FILE = 'A';
    public static final int MAX_RANK = 8;
    private ChessPiece.Side sideToMove;
    private boolean redoMoveInProgress;
    private String message;
    private String warning;
    private String kingCheckInformation;
    public GameEnds gameOver = null;
    private IUserInteractionEndpoint userEndpoint;
    // for checking the board's state, so that we can cache previous calculations
    private long boardPositionHash;

    public static enum GameEnds {

        CHECKMATE, RESIGNATION, TIMEEXPIRY, DRAW_STALEMATE, DRAW_REPETITION
    }

    public ChessBoard(IUserInteractionEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
        initializeSquares();
        initializePieces();
    }

    public ChessBoard(String serializedBoard, IUserInteractionEndpoint userEndpoint) throws Exception {
        // first create a normal board without stuff on
        this.userEndpoint = userEndpoint;
        initializeSquares();
        deserializeBoard(serializedBoard);
    }

    public ChessBoard(ChessBoard cloned, IUserInteractionEndpoint userEndpoint) {
        for (String k : cloned.SQUARES.keySet()) {
            this.SQUARES.put(k, new BoardSquare(cloned.SQUARES.get(k)));
        }
        this.moveHistory = new Stack<>();
        for (MovePlyProposal move : cloned.moveHistory) {
            this.moveHistory.add(move);
        }
        for (int i = 0; i < this.checkResolutionMoves.length; i++) {
            this.checkResolutionMoves[i] = new ArrayList<>(cloned.checkResolutionMoves[i]);
        }
        this.redoStack = new Stack<>();
        for (MovePly move : cloned.redoStack) {
            this.redoStack.add(move);
        }
        this.capturedPieces = new Stack<>();
        for (ChessPiece p : cloned.capturedPieces) {
            this.capturedPieces.add(p);
        }
        this.movesAnnotation = new StringBuilder(cloned.movesAnnotation.toString());
        // enums probably are by value
        this.sideToMove = cloned.sideToMove;
        this.redoMoveInProgress = cloned.redoMoveInProgress;
        this.message = cloned.message;
        this.warning = cloned.warning;
        this.kingCheckInformation = cloned.kingCheckInformation;
        this.gameOver = cloned.gameOver;
        this.positionsForSides[0] = new ArrayList<>();
        this.positionsForSides[1] = new ArrayList<>();

        int wLen = cloned.positionsForSides[0].size(),
                bLen = cloned.positionsForSides[1].size();
        for (int i = 0; i < Math.max(wLen, bLen); i++) {
            if (i < wLen) {
                positionsForSides[0].add(new BoardSquare(cloned.positionsForSides[0].get(i)));
            }
            if (i < bLen) {
                positionsForSides[1].add(new BoardSquare(cloned.positionsForSides[1].get(i)));
            }
        }
        if (userEndpoint != null) {
            this.userEndpoint = userEndpoint;
        }
        else {
            this.userEndpoint = cloned.userEndpoint;
        }
    }

    private void initializeSquares() {
        // ========= initialize all the squares and pieces ============
        for (int i = 0; i < 64; i++) {
            // the squares are filled from top to bottom, from rank 8 to 1, file A to H
            BoardSquare square = new BoardSquare((char) ('A' + (i % 8)), (byte) (8 - (i / 8)));
            SQUARES.put(square.toString(), square);
        }
        for (int i = 0; i < checkResolutionMoves.length; i++) {
            checkResolutionMoves[i] = new ArrayList<>();
        }
    }

    public final void initializePieces() {

        // white: officials
        SQUARES.get("A1").placePiece(new Rook(ChessPiece.Side.WHITE));
        SQUARES.get("B1").placePiece(new Knight(ChessPiece.Side.WHITE));
        SQUARES.get("C1").placePiece(new Bishop(ChessPiece.Side.WHITE));
        SQUARES.get("D1").placePiece(new Queen(ChessPiece.Side.WHITE));
        SQUARES.get("E1").placePiece(new King(ChessPiece.Side.WHITE));
        SQUARES.get("F1").placePiece(new Bishop(ChessPiece.Side.WHITE));
        SQUARES.get("G1").placePiece(new Knight(ChessPiece.Side.WHITE));
        SQUARES.get("H1").placePiece(new Rook(ChessPiece.Side.WHITE));
        // white: pawns
        // a loop over rank 2 would have done this too
        SQUARES.get("A2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("B2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("C2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("D2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("E2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("F2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("G2").placePiece(new Pawn(ChessPiece.Side.WHITE));
        SQUARES.get("H2").placePiece(new Pawn(ChessPiece.Side.WHITE));

        // black: officials
        SQUARES.get("A8").placePiece(new Rook(ChessPiece.Side.BLACK));
        SQUARES.get("B8").placePiece(new Knight(ChessPiece.Side.BLACK));
        SQUARES.get("C8").placePiece(new Bishop(ChessPiece.Side.BLACK));
        SQUARES.get("D8").placePiece(new Queen(ChessPiece.Side.BLACK));
        SQUARES.get("E8").placePiece(new King(ChessPiece.Side.BLACK));
        SQUARES.get("F8").placePiece(new Bishop(ChessPiece.Side.BLACK));
        SQUARES.get("G8").placePiece(new Knight(ChessPiece.Side.BLACK));
        SQUARES.get("H8").placePiece(new Rook(ChessPiece.Side.BLACK));
        // black: pawns
        SQUARES.get("A7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("B7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("C7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("D7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("E7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("F7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("G7").placePiece(new Pawn(ChessPiece.Side.BLACK));
        SQUARES.get("H7").placePiece(new Pawn(ChessPiece.Side.BLACK));

        // initializePieces the remaining empty squares to null, i.e. from rank 3 to rank 6
        // this is done explicitly in case it's a restart and pieces were previously
        // on these squares. If it's the game following a fresh launch, it's a
        // redundant operation
        for (int i = 3; i <= 6; i++) {
            SQUARES.get("A" + i).placePiece(null);
            SQUARES.get("B" + i).placePiece(null);
            SQUARES.get("C" + i).placePiece(null);
            SQUARES.get("D" + i).placePiece(null);
            SQUARES.get("E" + i).placePiece(null);
            SQUARES.get("F" + i).placePiece(null);
            SQUARES.get("G" + i).placePiece(null);
            SQUARES.get("H" + i).placePiece(null);
        }

        // create the first view of side pieces, so we can use them for loops, since it's
        // cheaper than looping through all 64 squares each time.
        refreshPositionsForSides();

        gameOver = null;
        sideToMove = ChessPiece.Side.WHITE;
        clearMessages();
    }

    public void setUpPosition(Map<String, ChessPiece> positions) {
        clearBoard();
        for (String k : positions.keySet()) {
            this.SQUARES.get(k).placePiece(positions.get(k));
        }
    }

    /**
     * Returns the BoardSquare object at location the given file and rank.
     *
     * @param file The file of the square, one of A-H
     * @param rank The rank of the square, from 1-8
     * @return
     */
    public BoardSquare getSquareForPosition(char file, int rank) {
        return SQUARES.get(file + "" + rank);
    }

    /**
     * Returns the BoardSquare object representing the given location
     *
     * @param location The address of the square, e.g. A1 or D7
     * @return
     */
    public BoardSquare getSquareForPosition(String location) {
        return SQUARES.get(location.toUpperCase());
    }

    /**
     * Yields the squares that have pieces of the specified
     * <tt>attackingSide</tt> that are attacking the given
     * <tt>square</tt>. It uses <tt>canAttackOrDefend(b, square)</tt>, which
     * gets the squares that have a valid attack move to the given square,
     * irrespective of who is on the square.
     *
     * @param attackedSquare
     * @param attackingSide
     * @return
     */
    public List<BoardSquare> getAttackingPiecePositions(BoardSquare attackedSquare, ChessPiece.Side attackingSide) {
        List<BoardSquare> attackingPieces = new ArrayList<>();
        // go through all the pieces and check if they have a valid move to that square
        // the pawn will have a valid move to that square if it's diagonally ahead and is
        // occupied by an opposing piece, but implement that later
        for (BoardSquare b : positionsForSides[getSideAsIndex(attackingSide)]) {
            ChessPiece piece = b.getPiece();
            assert (piece.getSide() == attackingSide) : "Attacking pieces SHOULD be from attackingSide";
            // check if the attacking piece can jump to the square in one move
            if (piece.canAttackOrDefend(b, attackedSquare, this)) {
                attackingPieces.add(b);
            }
        }
        return attackingPieces;
    }

    /**
     * Gets all the pieces and their locations, who can make a move to the given
     * <tt>toSquare</tt>, irrespective of if the move is an attack move or a
     * normal move. It only considers squares that are either empty or has a
     * piece belonging to the other side (a capturable square).
     *
     * @param toSquare
     * @param side
     * @return
     */
    public Map<ChessPiece, BoardSquare> getMoveablePieces(BoardSquare toSquare, ChessPiece.Side side) {
        Map<ChessPiece, BoardSquare> moveablePieces = new HashMap<>();
        // go through all the pieces and check if they have a valid move to that square
        // the pawn will have a valid move to that square if it's diagonally ahead
        for (BoardSquare b : positionsForSides[getSideAsIndex(side)]) {
            ChessPiece piece = b.getPiece();
            assert (piece.getSide() == side) : "Moveable pieces must be from <side>";
            // check if the attacking piece can jump to the square in one move
            if (piece.canMakeMove(b, toSquare, this)) {
                moveablePieces.put(piece, b);
            }
        }
        return moveablePieces;
    }

    public List<MovePlyProposal> getKingEvadingMoves(BoardSquare kingSquare, List<BoardSquare> attackingSquares) {
        List<MovePlyProposal> evadeMoves = new ArrayList<>();
        King king = (King) kingSquare.getPiece();
        // check all squares around the king that the king can move to on an empty board
        for (BoardSquare bs : SQUARES.values()) {
            if (!king.isAttackMove(kingSquare, bs)) {
                // eliminates all squares the king can't jump to if board is empty
                //skip
                continue;
            }
            // also we need to check if the king is the blocking piece from the evade square,
            // in which case it's not a valid move. E.g. K@c7 being attacked by Q@f4, and b8 is free
            boolean isValid = true;
            for (BoardSquare as : attackingSquares) {
                // get the pieces btw the attacker and the evade square if the attacker can
                // go there
                List<ChessPiece> blockingPieces = as.getPiece().getBlockingPieces(as, bs, this);
                // check if it's only the king that's blocking, meaning that if the king
                // moves, then the block is removed and the attacker can access the square
                if (blockingPieces.size() == 1 && blockingPieces.get(0) instanceof King
                    && blockingPieces.get(0).getSide() == king.getSide()) {
                    isValid = false;
                }
            }
            // check if the king can make the move and if it's free from attack and empty
            // the final condition eliminates the case where the evade move involves the
            // king capturing a piece. This is included rather in getKingAttackerCapturingMoves().
            if (isValid && king.canMakeMove(kingSquare, bs, this)) {
                evadeMoves.add(new MovePlyProposal(kingSquare.toString(), bs.toString()));
            }
        }
        return evadeMoves;
    }

    public List<BoardSquare> getPieceLocation(Class<? extends ChessPiece> type, ChessPiece.Side side) {
        List<BoardSquare> pieces = new ArrayList<>();
        for (BoardSquare square : SQUARES.values()) {
            ChessPiece piece = square.getPiece();
            if (piece != null && piece.getClass() == type && piece.getSide() == side) {
                pieces.add(square);
            }
        }
        return pieces;
    }

    public List<BoardSquare>[] getPieceLocation(Class<? extends ChessPiece> type) {
        List<BoardSquare>[] pieceSquares = new ArrayList[2];
        List<BoardSquare> whites = new ArrayList<>();
        List<BoardSquare> blacks = new ArrayList<>();
        for (BoardSquare square : SQUARES.values()) {
            ChessPiece piece = square.getPiece();
            if (piece != null && piece.getClass() == type) {
                if (piece.getSide() == ChessPiece.Side.WHITE) {
                    whites.add(square);
                }
                else {
                    blacks.add(square);
                }
            }
        }
        pieceSquares[0] = whites;
        pieceSquares[1] = blacks;
        return pieceSquares;
    }

    public Map<Class<? extends ChessPiece>, List<BoardSquare>[]> getAllPieceLocations() {

        // lists for each type and side
        // king
        List<BoardSquare> kWhites = new ArrayList<>();
        List<BoardSquare> kBlacks = new ArrayList<>();
        // queen
        List<BoardSquare> qWhites = new ArrayList<>();
        List<BoardSquare> qBlacks = new ArrayList<>();
        // rook
        List<BoardSquare> rWhites = new ArrayList<>();
        List<BoardSquare> rBlacks = new ArrayList<>();
        // bishop
        List<BoardSquare> bWhites = new ArrayList<>();
        List<BoardSquare> bBlacks = new ArrayList<>();
        // knights
        List<BoardSquare> nWhites = new ArrayList<>();
        List<BoardSquare> nBlacks = new ArrayList<>();
        // pawns
        List<BoardSquare> pWhites = new ArrayList<>();
        List<BoardSquare> pBlacks = new ArrayList<>();

        for (BoardSquare square : SQUARES.values()) {
            ChessPiece piece = square.getPiece();
            if (piece != null) {
                if (piece instanceof King) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        kWhites.add(square);
                    }
                    else {
                        kBlacks.add(square);
                    }
                }
                else if (piece instanceof Queen) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        qWhites.add(square);
                    }
                    else {
                        qBlacks.add(square);
                    }
                }
                else if (piece instanceof Rook) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        rWhites.add(square);
                    }
                    else {
                        rBlacks.add(square);
                    }
                }
                else if (piece instanceof Bishop) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        bWhites.add(square);
                    }
                    else {
                        bBlacks.add(square);
                    }
                }
                else if (piece instanceof Knight) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        nWhites.add(square);
                    }
                    else {
                        nBlacks.add(square);
                    }
                }
                else if (piece instanceof Pawn) {
                    if (piece.getSide() == ChessPiece.Side.WHITE) {
                        pWhites.add(square);
                    }
                    else {
                        pBlacks.add(square);
                    }
                }
            }
        }

        List<BoardSquare>[] kingSquares = new ArrayList[2];
        kingSquares[0] = kWhites;
        kingSquares[1] = kBlacks;

        List<BoardSquare>[] queenSquares = new ArrayList[2];
        queenSquares[0] = qWhites;
        queenSquares[1] = qBlacks;

        List<BoardSquare>[] rookSquares = new ArrayList[2];
        rookSquares[0] = rWhites;
        rookSquares[1] = rBlacks;

        List<BoardSquare>[] bishopSquares = new ArrayList[2];
        bishopSquares[0] = bWhites;
        bishopSquares[1] = bBlacks;

        List<BoardSquare>[] knightSquares = new ArrayList[2];
        knightSquares[0] = nWhites;
        knightSquares[1] = nBlacks;

        List<BoardSquare>[] pawnSquares = new ArrayList[2];
        pawnSquares[0] = pWhites;
        pawnSquares[1] = pBlacks;

        Map<Class<? extends ChessPiece>, List<BoardSquare>[]> map = new HashMap<>();
        map.put(King.class, kingSquares);
        map.put(Queen.class, queenSquares);
        map.put(Rook.class, rookSquares);
        map.put(Bishop.class, bishopSquares);
        map.put(Knight.class, knightSquares);
        map.put(Pawn.class, pawnSquares);

        return map;
    }

    public List<MovePlyProposal> getKingShieldingMoves(BoardSquare kingSquare, List<BoardSquare> attackingSquares) {
        ChessPiece king = kingSquare.getPiece();
        List<MovePlyProposal> shieldingMoves = new ArrayList<>();
        int index = getSideAsIndex(king.getSide()),
                altIndex = (index + 1) % 2;
        // king can only be shielded if there's just one attacker
        if (attackingSquares.size() == 1) {
            BoardSquare s = attackingSquares.get(0);
            // check if there are intervening squares
            ChessPiece p = s.getPiece();
            List<BoardSquare> interveningSquares = p.getBlockingSquares(s, kingSquare, this);
            if (interveningSquares.isEmpty()){
                // no need; no blocking space
                return shieldingMoves;
            }
            List<BoardSquare> fixedPieces = new ArrayList<>();
            // get all the opponent pieces that have a remote attack line to the king
            for (BoardSquare sq : positionsForSides[altIndex]) {
                ChessPiece bp = sq.getPiece();
                List<BoardSquare> blockingSquares = bp.getBlockingSquares(sq, kingSquare, this);
                List<BoardSquare> pinned = new ArrayList<>();
                for (BoardSquare bs : blockingSquares) {
                    if (bs.hasPiece() && bs.getPiece().getSide() == sideToMove) {
                        pinned.add(bs);
                    }
                }
                if (pinned.size() == 1) {
                    fixedPieces.addAll(pinned);
                }
            }
            for (BoardSquare interveningSquare : interveningSquares) {
                // Loop through all pieces and check who can occupy the squares.
                // Get the pinned pieces
                for (BoardSquare sq : positionsForSides[index]) {
                    ChessPiece pb = sq.getPiece();
                    if (pb == null) {
                        System.err.println("Wait here;");
                    }
                    assert (pb != null) : "in getKingShieldingMoves(). pb should not be null";
                    if (!sq.equals(kingSquare) && pb.canMakeMove(sq, interveningSquare, this)
                        && !fixedPieces.contains(sq)) {
                        shieldingMoves.add(new MovePlyProposal(sq.toString(), interveningSquare.toString()));
                    }
                }
            }
        }
        return shieldingMoves;
    }

    public List<MovePlyProposal> getKingAttackerCapturingMoves(ChessPiece.Side kingSide,
                                                               List<BoardSquare> attackingSquares) {

        List<MovePlyProposal> capturingMoves = new ArrayList<>();
        BoardSquare ks = getPieceLocation(King.class, kingSide).get(0);
        King king = (King) ks.getPiece();
        // capturing an attacker to cancel check works only if it's one attacker
        if (attackingSquares.size() == 1) {
            BoardSquare s = attackingSquares.get(0);
            for (BoardSquare bs : SQUARES.values()) {
                ChessPiece p = bs.getPiece();
                if (p != null && p.getSide() == king.getSide()) {
                    BoardSquare captureSquare = p.canCapturePiece(bs, s, this);
                    if (captureSquare != null &&
                        // make sure the piece isn't pinned
                        getPinningSquares(bs, ks).isEmpty()) {
                        capturingMoves.add(new MovePlyProposal(bs.toString(), captureSquare.toString()));
                    }
                }
            }
        }
        return capturingMoves;
    }

    public List<BoardSquare> getKingAttackerPositions(ChessPiece.Side kingSide) {
        // first get the square of the king for the side under attack
        ChessPiece.Side attackingSide = kingSide.otherSide();
        try {
            BoardSquare s = getPieceLocation(King.class, kingSide).get(0);
            if (s == null) {
                System.err.println("INVALID GAME STATE: " + kingSide.toString() + " is missing from the board.");
                throw new Exception();
            }
            return getAttackingPiecePositions(s, attackingSide);
        }
        catch (Exception ex) {
            System.err.println("The KING has been captured??????");
        }
        return null;
    }

    private void recordMove(BoardSquare from, BoardSquare to, ChessPiece piece, ChessPiece capturedPiece, boolean isKingCheck, boolean isEnpassant, String timeOfMove) {
        // record this move
        MovePly lastMove = new MovePly(from.toString(), to.toString(), piece,
                capturedPiece, isKingCheck, isEnpassant, gameOver == GameEnds.CHECKMATE);
        lastMove.setTimeOfMove(timeOfMove);
        // check if there's a possible ambuiguity in knowing which piece moved,
        // based on standard chess notation
        if (!(piece instanceof King) && !(piece instanceof Pawn)) {
            // find the other piece
            List<BoardSquare> squares = getPieceLocation(piece.getClass(), piece.getSide());
            List<BoardSquare> others = new ArrayList<>();
            for (BoardSquare b : squares) {
                if (b != from) {
                    ChessPiece p = b.getPiece();
                    if (p.canAttackOrDefend(b, to, this)) {
                        others.add(b);
                    }
                }
            }
            // now check the number of other pieces of same type attacking same square
            if (!others.isEmpty()) {
                if (others.size() == 1) {
                    // this is the another piece
                    BoardSquare b = others.get(0);
                    // disambiguate using file
                    if (b.getFile() != from.getFile()) {
                        lastMove.setDistinguisher(from.getFile() + "");
                    }
                    else {
                        // or rank
                        lastMove.setDistinguisher(from.getRank() + "");
                    }

                }
                else {
                    // disambiguate using both rank AND file
                    lastMove.setDistinguisher(from.toString());
                }
            }
        } // check for promotion
        else if (piece instanceof Pawn && ((piece.getSide() == ChessPiece.Side.WHITE && to.getRank() == 8) || (piece.getSide() == ChessPiece.Side.BLACK && to.getRank() == 1))) {
            lastMove.setPromotionPiece(to.getPiece());
        }
        moveHistory.push(lastMove);
        // record the moves as 1. White move Black move
        int size = moveHistory.size();
        // check if we're bringing a new line or writing on same line
        if (size % 2 == 1) {
            // last move was white
            int n = (size / 2) + 1;
            movesAnnotation.append(n).append(".  ").append(lastMove.toString());
        }
        else {
            movesAnnotation.append("  ").append(lastMove.toString()).append("\n");
        }
    }

    public MovePly redoMove() {
        clearMessages();
        if (!redoStack.isEmpty()) {
            MovePly lastUndo = redoStack.pop();
            // use boardsquareclicked to include highlighting
            return lastUndo;
        }
        return null;
    }

    public void undoMove() {
        clearMessages();
        // remove any check warnings
        clearCheckResolutionMoves();
        MovePly lastMove = (MovePly) moveHistory.pop();
        if (lastMove == null) {
            return;
        }
        int index = getSideAsIndex(lastMove.getSide()),
                altIndex = (index + 1) % 2;
        BoardSquare from = SQUARES.get(lastMove.getFrom());
        BoardSquare to = SQUARES.get(lastMove.getTo());
        BoardSquare capturedPiecePosition = to;
        ChessPiece captured = null;
        if (lastMove.isEnpassant()) {
            // we place the pawn in a different place than normal
            capturedPiecePosition = SQUARES.get(to.getFile() + "" + from.getRank());
        }
        if (lastMove.isCapture()) {
            captured = capturedPieces.pop();
        }
        if (lastMove.isCastling()) {
            // we also must move the rook back
            King k = (King) lastMove.getPiece();
            // to make the move, get the start and end squares for the castling rook
            BoardSquare[] castlingRookSquares = k.getCastlingRookSquares(
                    from, to, this);
            // move the rook
            SQUARES.get(castlingRookSquares[0].toString())
                    .placePiece(castlingRookSquares[1].getPiece());
            SQUARES.get(castlingRookSquares[1].toString())
                    .removePiece();
            // adjust the positions array
            positionsForSides[index].add(castlingRookSquares[0]);
            positionsForSides[index].remove(castlingRookSquares[1]);
        }

        from.placePiece(lastMove.getPiece());
        to.placePiece(null);
        capturedPiecePosition.placePiece(captured);
        positionsForSides[index].add(from);
        positionsForSides[index].remove(to);
        positionsForSides[altIndex].add(capturedPiecePosition);
        // the alternative to all the positionsForSides declarations above
        refreshPositionsForSides();
        /**
         * Edge cases: 1. Pawn promotion 2. King castling 3. En passant
         */
        redoStack.push(lastMove);
        switchPlayTurn();
        // check status of new last move
        if (!moveHistory.isEmpty() && ((MovePly) moveHistory.peek()).isKingCheck()) {
            resolveKingCheck(getKingAttackerPositions(sideToMove));
        }
        // deal with the history text
        String history = movesAnnotation.toString();
        if (lastMove.getPiece().getSide() == ChessPiece.Side.WHITE) {
            // remove the number and move
            history = history.substring(0, Math.max(0, history.lastIndexOf("\n") + 1));
        }
        else {
            history = history.substring(0, history.lastIndexOf("  "));
        }
        movesAnnotation = new StringBuilder(history);
        // undo possible game end
        gameOver = null;
    }

    /**
     * Needed after undo and redo, which come relatively rarely
     */
    private void refreshPositionsForSides() {
        // create the first view of side pieces, so we can use them for loops, since it's
        // cheaper than looping through all 64 squares each time.
        List<BoardSquare> whites = new ArrayList<>();
        List<BoardSquare> blacks = new ArrayList<>();
        for (BoardSquare bs : SQUARES.values()) {
            ChessPiece piece = bs.getPiece();
            if (piece != null) {
                if (piece.getSide() == ChessPiece.Side.WHITE) {
                    whites.add(bs);
                }
                else {
                    blacks.add(bs);
                }
            }
        }
        positionsForSides[0] = whites;
        positionsForSides[1] = blacks;
    }

    /**
     * Check for draw by n-fold repetition; we need the 3-fold version for
     * ending the game
     *
     * @param numTimes
     * @return
     */
    public boolean isRepetition(int numTimes) {
        // numMoves will be + 1 of numTimes, because the first move only forms the basis
        // and is not counted as a repeat
        int numMoves = (numTimes + 1);
        if (moveHistory.size() < numMoves * 2) {
            return false;
        }
        boolean sideRepeats = true;
        int lastIndex = moveHistory.size() - 1;
        MovePlyProposal lastSide1 = null;
        MovePlyProposal lastSide2 = null;
        // compare moves independently for white and black
        for (int i = 0; i < numMoves * 2; i++) {
            MovePlyProposal mp = new MovePlyProposal(moveHistory.get(lastIndex - i));
            if (i % 2 == 0) {
                // last side that moved
                if (lastSide1 == null) {
                    lastSide1 = mp;
                    continue;
                }
                // if we get any move for this side that isn't the reverse of its previous
                // counterpart before we run out of loops, then it's definitely not a repeat
                if (!lastSide1.equals(mp.reverse())) {
                    sideRepeats = false;
                    break;
                }
                lastSide1 = mp;
            }
            else {
                // last side that moved
                if (lastSide2 == null) {
                    lastSide2 = mp;
                    continue;
                }
                if (!lastSide2.equals(mp.reverse())) {
                    sideRepeats = false;
                    break;
                }
                lastSide2 = mp;
            }
        }
        return sideRepeats;
    }

    public boolean moveIsPossible(MovePlyProposal move) {

        if (gameOver != null) {
            // we shouldn't be here... ever
            System.err.println("ERROR: Trying to make move but game has ended.");
            message = "ERROR: Trying to make move but game has ended.";
            return false;
        }
        BoardSquare fromSquare = SQUARES.get(move.getFrom()),
                toSquare = SQUARES.get(move.getTo());

        ChessPiece p = fromSquare.getPiece();

        if (p == null) {
            return false;
        }

        // check if it's the chosen piece's turn to move
        if (p.getSide() != getSideToMove()) {
            message = "INVALID MOVE: The piece you're trying to move doesn't belong to your side.";
            return false;
        }

        // check if it's a valid moveIsPossible
        if (p.canMakeMove(fromSquare, toSquare, this)) {
            // check for pins
            if (!(p instanceof King)) {
                BoardSquare ks = getPieceLocation(King.class, getSideToMove()).get(0);
                List<BoardSquare> pinningSquares = getPinningSquares(fromSquare, ks);
                // check if pinned to the king and if the move it's making is taking away the 
                // pinning piece or moving the pinned piece away from a pinning position
                if (!pinningSquares.isEmpty() && !pinningSquares.contains(toSquare)) {
                    message += "INVALID MOVE: That piece is pinned to your King by "
                              + pinningSquares.get(0).getPiece() + " at " + pinningSquares.get(0);
                    return false;
                }
            }
            // first, check for king and its resolution
            if (isKingCheck()) {
                MovePlyProposal m = new MovePlyProposal(fromSquare.toString(), toSquare.toString());
                if (!getAllCheckResolutionMoves().contains(m)) {
                    // disallow the move
                    message += "INVALID MOVE: That move doesn't remove the check on your king!";
                    return false;
                }
            }
        }
        else {
            // or else tell the user it's an invalid moveIsPossible
            message = ("INVALID MOVE: The piece cannot move to that square.");
            return false;
        }
        return true;
    }

    /**
     * Moves a piece from the source square to the destination square, if the
     * moveIsPossible is valid. Prints the progress of the moveIsPossible. Takes
     * <tt>computerIsThinking</tt> to state that these functions are running in
     * the computer's head, so disable some unneeded "activities" in the method.
     *
     *
     * @param move
     * @param time
     * @return True if the piece is successfully moved; false otherwise.
     */
    public boolean movePiece(MovePlyProposal move, String time) {
        return movePiece(move, time, false);
    }

    /**
     * Moves a piece from the source square to the destination square, if the
     * moveIsPossible is valid. Prints the progress of the moveIsPossible. Takes
     * <tt>computerIsThinking</tt> to state that these functions are running in
     * the computer's head, so disable some unneeded "activities" in the method.
     *
     *
     * @param move
     * @param time
     * @return True if the piece is successfully moved; false otherwise.
     */
    public boolean movePiece(MovePlyProposal move, String time, boolean computerIsThinking) {
        clearMessages();

        int pIndex = sideToMove == ChessPiece.Side.WHITE ? 0 : 1;

        String from = move.getFrom();
        String to = move.getTo();
        BoardSquare fromSquare = SQUARES.get(from);
        BoardSquare toSquare = SQUARES.get(to);

        ChessPiece p = fromSquare.getPiece();

        // check if it's a valid moveIsPossible
        if (moveIsPossible(move)) {
            // from here we are sure the move can actually be made on the board
            clearCheckResolutionMoves();
            ChessPiece capturedPiece;
            boolean isEnpassant = false;
            BoardSquare enpassantSquare = null;
            boolean isCastling = false;
            int fileDiff = fromSquare.getFile() - toSquare.getFile();
            // first check for castling
            if (p instanceof King) {
                King k = ((King) p);
                isCastling = k.canCastle(fromSquare, toSquare, this);
                if (isCastling) {
                    // to make the move, get the start and end squares for the castling rook
                    BoardSquare[] castlingRookSquares = k.getCastlingRookSquares(fromSquare, toSquare, this);

                    // move the rook
                    SQUARES.get(castlingRookSquares[1].toString())
                            .placePiece(castlingRookSquares[0].getPiece());
                    SQUARES.get(castlingRookSquares[0].toString())
                            .removePiece();
                    // update the white/black position view
                    positionsForSides[pIndex].remove(castlingRookSquares[0]);
                    positionsForSides[pIndex].add(castlingRookSquares[1]);
                }
            } // check for promotion
            else if (p instanceof Pawn && ((p.getSide() == ChessPiece.Side.WHITE && toSquare.getRank() == 8)
                                           || (p.getSide() == ChessPiece.Side.BLACK && toSquare.getRank() == 1))) {
                // swap out the advancing pawn for the selected promoted piece
                fromSquare.placePiece(promotePawn());
                message = (p + " at " + from + " promoted to " + fromSquare.getPiece());
            }
            else if (p instanceof Pawn && ((Pawn) p).isEnPassant(fromSquare, toSquare, this)) {
                isEnpassant = true;
                enpassantSquare = SQUARES.get(toSquare.getFile() + "" + fromSquare.getRank());
                ChessPiece enpassantPiece = enpassantSquare.removePiece();
                // pretend that the piece being captured en passant is on the normal attacking pawns line of attack
                toSquare.placePiece(enpassantPiece);
            }
            // make the move by removing the piece from the source square,
            // and placing it in the destination square. Note that the replace()
            // function returns the former occupant of the slot you are replacing
            capturedPiece = toSquare.placePiece(fromSquare.removePiece());
            positionsForSides[pIndex].remove(fromSquare);
            positionsForSides[pIndex].add(toSquare);
            // mark these square as having a piece that has moved, for the sake of castling
            p.setHasMoved(true);

            // build the text for reporting this move
            StringBuilder log = new StringBuilder();
            // if there was a piece on the destination square, then the replace()
            // function automatically returns that piece, and we put it in the list
            // of captured pieces
            if (capturedPiece != null) {
                capturedPieces.push(capturedPiece);
                // update our array to reflect new realities
                BoardSquare toRemove = isEnpassant? enpassantSquare : toSquare;
                positionsForSides[(pIndex + 1) % 2].remove(toRemove);
                // then inform the user
                log.append(p.toString()).append(" at ").append(from).append(" captures ")
                        .append(capturedPiece.toString()).append(" at ").append(to);
            }
            else if (isCastling) {
                // report the movement
                String b = fileDiff > 0 ? "queenside" : "kingside";
                log.append(p.toString()).append(" castles ").append(b);
            }
            else {
                // report the movement
                log.append(p.toString()).append(" at ").append(from).append(" moves to ")
                        .append(to);
            }
            // redoMove() calls this function, so we need to differentiate a normal
            // user move from the former, so that we know when redoStack should be flushed
            if (!isRedoMoveInProgress() && !getRedoStack().isEmpty()) {
                redoStack.clear();
            }

            // =========================================================================
            // ========== From this point, we switch turns and do some checks ==========
            // reverse the turn
            switchPlayTurn();
            // notify of checks for the newly completed move, i.e. after reversing turns
            // check if the new side's king is checked
            List checkingPieces = getKingAttackerPositions(getSideToMove());
            boolean isNewCheck = !checkingPieces.isEmpty();
            if (isNewCheck) {
                log.append(" and checks ").append(getSideToMove().name()).append(" King.\n");
            }
            // ======= for annotation of moves ========
            message = (log.toString());
            // record the move here so we know when to record a mate
            recordMove(fromSquare, toSquare, p, capturedPiece, isNewCheck,
                    isEnpassant, time);

            Util.printList(positionsForSides[0]);
            Util.printList(positionsForSides[1]);
            
            // Resolve the king check
            if (isNewCheck) {
                warning = sideToMove.name() + " King checked!";
                // get the check resolution moves
                this.resolveKingCheck(checkingPieces);
                if (isCheckResolutionMovesEmpty()) {
                    gameOver = GameEnds.CHECKMATE;
                    return true;
                }
            }
            // check for existence of next moves for the new turn, for stalemates
            // only if it's a human playing, so you can tell when there's stalemate by looking 
            // head one move. For the computer, we can let it try to make a move, instead of
            // looking ahead, and come up empty. 
            if (!computerIsThinking && getPossibleMoves().isEmpty()) {
                gameOver = GameEnds.DRAW_STALEMATE;
            }
            // check for repetitions
            else if (isRepetition(3)) {
                gameOver = GameEnds.DRAW_REPETITION;
            }
            return true;
        }
        return false;
    }

    private void resolveKingCheck(List attackingSquares) {

        if (attackingSquares.isEmpty()) {
            return;
        }
        BoardSquare kingSquare = getPieceLocation(King.class, sideToMove).get(0);
        StringBuilder sb = new StringBuilder();
        sb.append("\n========== King Check information ========== \n");
        sb.append("Check on ").append(sideToMove.name()).append(" King at ").append(kingSquare).append("\n");
        sb.append("Attacking pieces: ")
                .append(Util.printPieceBoardSquareList(attackingSquares))
                .append("\n");
        List<MovePlyProposal> kingEvadingMoves = getKingEvadingMoves(kingSquare, attackingSquares);
        boolean kingCanEvade = !kingEvadingMoves.isEmpty();
        sb.append("King can evade: ").append(kingCanEvade).append("\n");
        if (kingCanEvade) {
            sb.append("Evade moves: ")
                    .append(Util.printMoveProposalList(kingEvadingMoves)).append("\n");
        }
        List<MovePlyProposal> kingShieldingMoves = getKingShieldingMoves(kingSquare, attackingSquares);
        boolean kingCanBeShielded = !kingShieldingMoves.isEmpty();
        sb.append("King can be shielded: ").append(kingCanBeShielded).append("\n");
        if (kingCanBeShielded) {
            sb.append("Shielding moves: ")
                    .append(Util.printMoveProposalList(kingShieldingMoves))
                    .append("\n");
        }
        List<MovePlyProposal> kingDefendingMoves = getKingAttackerCapturingMoves(sideToMove, attackingSquares);
        boolean kingCanBeDefended = !kingDefendingMoves.isEmpty();
        sb.append("King's attacker can be captured: ").append(kingCanBeDefended).append("\n");
        if (kingCanBeDefended) {
            sb.append("Attacker capturing moves: ")
                    .append(Util.printMoveProposalList(kingDefendingMoves))
                    .append("\n");
        }
        sb.append("=============================================\n");
        kingCheckInformation = sb.toString();

        // store the possible escape moves
        checkResolutionMoves[0].addAll(kingDefendingMoves);
        checkResolutionMoves[1].addAll(kingShieldingMoves);
        checkResolutionMoves[2].addAll(kingEvadingMoves);
    }

    public List<MovePlyProposal>[] getCheckResolutionMoves() {
        return checkResolutionMoves;
    }

    /**
     * Get the possible moves for the current side, considering king checks.
     * This method uses excluded lists (possibly supplied by the computer
     * thinker) as a fail-fast means to limit considered moves by the engine.
     *
     * @param excludedMoves MoveProposals to excluded
     * @param excludedFromSquares Squares to not consider moving from
     * @param excludedToSquares Squares to not consider going to
     * @return
     */
    public List<MovePlyProposal> getPossibleMoves() {
        // check for king check
        if (isKingCheck()) {
            // make a copy of this, since when the king is checked, this same
            // list is return, and is modified within itself, causing an exception
            // this method automatically copies out the list, although just a 
            // shallow copy; that's enough to not cause the concurrent modification
            // exception
            return getAllCheckResolutionMoves();
        }
        List<MovePlyProposal> moves = new ArrayList<>();
        for (BoardSquare from : SQUARES.values()) {
            ChessPiece p = from.getPiece();
            if (p != null && p.getSide() == getSideToMove()) {
                // list all the moves
                for (BoardSquare ps : getPossibleDestinationSquares(from)) {
                    MovePlyProposal move = new MovePlyProposal(from.toString(), ps.toString());
                    // check the actual move
                    if (moveIsPossible(move)) {
                        moves.add(move);
                    }
                }
            }
        }
        return moves;
    }

    public List<MovePlyProposal> getPossibleMovesByPiece(BoardSquare pieceSquare) {
        if (!pieceSquare.hasPiece()) {
            return null;
        }
        List<MovePlyProposal> moves = new ArrayList<>();
        for (BoardSquare sq : SQUARES.values()) {
            MovePlyProposal m = new MovePlyProposal(pieceSquare.toString(), sq.toString());
            if (moveIsPossible(m)) {
                moves.add(m);
            }
        }
        return moves;
    }

    /**
     * Gets all the squares that the piece on <tt>pieceSquare</tt> can make from
     * <tt>pieceSquare</tt>
     *
     * @param pieceSquare
     * @param piece
     * @return
     */
    public List<BoardSquare> getPossibleDestinationSquares(BoardSquare pieceSquare) {
        return getPossibleDestinationSquares(pieceSquare, pieceSquare.getPiece());
    }

    /**
     * Gets all the squares that this piece can go to from
     * <tt>pieceSquare</tt>
     *
     * @param pieceSquare
     * @param piece
     * @return
     */
    private List<BoardSquare> getPossibleDestinationSquares(BoardSquare pieceSquare, ChessPiece piece) {
        List<BoardSquare> squares = new ArrayList<>();
        if (piece != null) {
            for (BoardSquare sq : SQUARES.values()) {
                if (piece.canMakeMove(pieceSquare, sq, this)) {
                    squares.add(sq);
                }
            }
        }
        return squares;
    }

    /**
     * For the current side which has the move turn, which is always the case,
     * because we can't make any other move if the King is checked.
     *
     * This variable is reliably manage by the clearCheckResolutionMoves()
     *
     * @return
     */
    public boolean isKingCheck() {
        return !getAllCheckResolutionMoves().isEmpty() && gameOver == null;
    }

    public String getKingCheckInformation() {
        return kingCheckInformation;
    }

    /**
     * Returns the square that has a piece that pins another piece at
     * <tt>pinnedSquare</tt> to a third piece at <tt>pinnedTo</tt>
     * square, and then all the squares in-between the pinning piece and the
     * pinned-to piece, which of course would include the pinnedSquare if there
     * is a pin. Empty result if there is no pinning piece.
     * <p>
     * Note 1: By definition, a pin means that only the piece at the
     * <tt>pinnedSquare</tt> blocks an attacker from the <tt>pinnedTo</tt>
     * square. If there is more than one piece in the intervening squares
     * between attacker and destination square, then it's not a pin.
     * <p>
     * Note 2: There can only be one enemy piece pinning your piece to your
     * king, for example, because by the nature of the game, it's not possible
     * to have one piece that is pinned to another piece by more than one
     * opponent piece. If there's a battery like enemy bishop and queen on the
     * same diagonal to your king, with your pawn somewhere in-between, then
     * only the piece closer to the pinned pawn is an actual threat.
     *
     * @param pinnedSquare The square that is pinned to the
     * <tt>pinnedTo</tt> square
     * @param pinnedTo The square the piece is pinned to
     * @return The square of the piece that is pinning the
     * <tt>pinnedSquare</tt> to the <tt>pinnedTo</tt>, followed by all the
     * squares between the pinned-to and the attacker.
     */
    private List<BoardSquare> getPinningSquares(BoardSquare pinnedSquare, BoardSquare pinnedTo) {
        List<BoardSquare> squares = new ArrayList<>();
        if (!pinnedSquare.hasPiece()) {
            return squares;
        }
        for (BoardSquare sq : positionsForSides[getSideAsIndex(sideToMove.otherSide())]) {
            // get attackers to the current side, and check if they have a line
            // of attacked to the pinned to square, and this line is blocked only
            // by the pinned square
            ChessPiece bp = sq.getPiece();
            assert (bp != null) : "in getPinningSquares(). bp shouldn't be null.";
            // a pawn can't pin
            if (bp instanceof Pawn){
                continue;
            }
            List<BoardSquare> blockingSquares = bp.getBlockingSquares(sq, pinnedTo, this);
            int numOccupied = 0;
            for (BoardSquare bs : blockingSquares) {
                if (bs.hasPiece()) {
                    numOccupied++;
                }
                if (bs.equals(pinnedSquare)) {
                    // check if this piece is pinned to the the pinnedTo square
                    squares.add(sq);
                }
            }
            // check the count to be sure it's only one piece blocking, and that
            // the pinned square is in the mix (a lá !squares.isEmpty())
            if (numOccupied == 1 && !squares.isEmpty()) {
                // add the remainder, with the pinned block at index 0.
                squares.addAll(blockingSquares);
                return squares;
            }
        }
        return new ArrayList<>();
    }

    private ChessPiece promotePawn() {
        return userEndpoint.getPromotionPiece(sideToMove);
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append(this).append("\n");
        sb.append("      A    B    C    D    E    F    G    H\n");
        sb.append("  ---------------------------------------------\n");
        for (int i = 8; i >= 1; i--) {
            sb.append(i).append("   ");
            for (int j = 0; j < 8; j++) {
                sb.append("| ");
                // get the actual piece
                ChessPiece p = getSquareForPosition((char) ('A' + j), i).getPiece();
                if (p == null) {
                    // if nobody is on the square, just print three spaces
                    sb.append("   ");
                }
                else {
                    sb.append(p.toShortString()).append(" ");
                }
            }
            sb.append("|\n");
            sb.append("  ---------------------------------------------\n");
        }
        sb.append("      A    B    C    D    E    F    G    H\n");
        sb.append("=============================================\n\n");
        return sb.toString();
    }

    public boolean isCheckResolutionMovesEmpty() {
        return getAllCheckResolutionMoves().isEmpty();
    }

    public List<MovePlyProposal> getAllCheckResolutionMoves() {
        List<MovePlyProposal> list = new ArrayList<>();
        for (List<MovePlyProposal> moves : checkResolutionMoves) {
            list.addAll(moves);
        }
        return list;
    }

    private void deserializeBoard(String gamePosition) throws Exception {
        String[] lines = gamePosition.split("\n");
        // line 0 is for player information
        sideToMove = lines[1].equalsIgnoreCase("w")
                     ? ChessPiece.Side.WHITE : ChessPiece.Side.BLACK;
        for (int i = 2; i < lines.length; i++) {
            String[] line = lines[i].split("\\s");
            SQUARES.get(line[0]).placePiece(ChessPieceFactory.createPiece(line[1]));
        }
    }

    public String serialiseBoard() {
        StringBuilder sb = new StringBuilder();
        sb.append(sideToMove).append("\n");
        for (BoardSquare e : SQUARES.values()) {
            if (e.hasPiece()) {
                sb.append(e).append(" ").append(e.getPiece().toShortString())
                        .append("\n");
            }
        }
        return sb.toString();
    }

    public String getMessage() {
        return message;
    }

    public String getWarning() {
        return warning;
    }

    /**
     * @return the sideToMove
     */
    public ChessPiece.Side getSideToMove() {
        return sideToMove;
    }

    /**
     * @param sideToMove the sideToMove to set
     */
    public void setSideToMove(ChessPiece.Side sideToMove) {
        this.sideToMove = sideToMove;
    }

    /**
     * @return the moveHistory
     */
    public Stack<MovePlyProposal> getMovePlyHistory() {
        return moveHistory;
    }

    /**
     * @param moveHistory the moveHistory to set
     */
    public void setMoveHistory(Stack<MovePlyProposal> moveHistory) {
        this.moveHistory = moveHistory;
    }

    /**
     * @return the redoStack
     */
    public Stack<MovePly> getRedoStack() {
        return redoStack;
    }

    /**
     * @param redoStack the redoStack to set
     */
    public void setRedoStack(Stack<MovePly> redoStack) {
        this.redoStack = redoStack;
    }

    /**
     * @return the redoMoveInProgress
     */
    public boolean isRedoMoveInProgress() {
        return redoMoveInProgress;
    }

    /**
     * @param redoMoveInProgress the redoMoveInProgress to set
     */
    public void setRedoMoveInProgress(boolean redoMoveInProgress) {
        this.redoMoveInProgress = redoMoveInProgress;
    }

    public String getMovesAnnotation() {
        return movesAnnotation.toString();
    }

    public void switchPlayTurn() {
        sideToMove = sideToMove.otherSide();
    }

    private void clearMessages() {
        message = "";
        warning = "";
        kingCheckInformation = "";
    }

    private void clearCheckResolutionMoves() {
        for (List<MovePlyProposal> moves : checkResolutionMoves) {
            moves.clear();
        }
    }

    public final void clearBoard() {
        for (BoardSquare boardSquare : SQUARES.values()) {
            boardSquare.removePiece();
        }
    }

    public void resetTurns() {
        sideToMove = ChessPiece.Side.WHITE;
    }

    public void resetHistory() {
        // initializePieces the moves container
        moveHistory = new Stack<>();
        // and the redo buffer
        redoStack = new Stack<>();
        // clear captured pieces
        capturedPieces.clear();
    }

    public long getBoardPositionHash() {
        return boardPositionHash;
    }

    public List<BoardSquare>[] getPositionsForSides() {
        return positionsForSides;
    }

    public List<BoardSquare> getPositionsForCurrentSide() {
        return positionsForSides[getSideAsIndex(sideToMove)];
    }

    public static int getSideAsIndex(ChessPiece.Side side) {
        return side == ChessPiece.Side.WHITE ? 0 : 1;
    }

    /**
     * Called whenever the board changes, to be used for caching in the pieces.
     */
    private void hashBoardPosition() {
        long hash = 31;
        for (String string : SQUARES.keySet()) {
            hash = 7 * hash + string.hashCode();
            hash = 11 * hash + Objects.hashCode(SQUARES.get(string).getPiece());
        }
        boardPositionHash = hash;
    }

}
