/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Encapsulates the concept of an intended move, sans details of side effects
 * and actors<p> Note: always stores moves in uppercase. E.g. C2 to C4 or D1
 * to E3
 *
 * @author e.ubachukwu
 */
public class MovePlyProposal {

    protected final String from;
    protected final String to;

    public MovePlyProposal(String from, String to) {
        this.from = from;
        this.to = to;
    }
    
    public MovePlyProposal(MovePlyProposal move){
        this.from = move.getFrom();
        this.to = move.getTo();
    }
    
    public MovePlyProposal reverse(){
        return new MovePlyProposal(to, from);
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    @Override
    public String toString() {
        return from + "-" + to;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MovePlyProposal)) {
            return false;
        }
        MovePlyProposal m = (MovePlyProposal) obj;
        return this.hashCode() == m.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + from.hashCode();
        hash = 97 * hash + to.hashCode();
        return hash;
    }

}
