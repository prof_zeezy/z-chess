package model.intelligence;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import model.BoardSquare;
import model.ChessBoard;
import model.MovePly;
import model.MovePlyProposal;
import model.chesspiece.ChessPiece;
import model.chesspiece.King;
import model.chesspiece.Pawn;
import model.chesspiece.Queen;
import model.interfaces.IMoveListener;
import model.interfaces.IUserInteractionEndpoint;
import util.Util;

/**
 *
 * @author eziama
 */
public class ComputerPlayer implements IUserInteractionEndpoint {

    private ChessBoard board;
    private final List<IMoveListener> listeners = new ArrayList<>();

    private MovePlyTree moveTree;
    public static int MAX_ANALYSIS_DEPTH = 2;
    public static int MAX_WORKER_THREADS = 1;
    public StringBuilder output;
    private ExecutorService executorService;
    private List<Callable<Float>> tasks;
    private List<Future<Float>> taskFutures;
    private MovePlyProposal proposedMove;

    private int DEBUG_MAX_MOVES = 300;

    public ComputerPlayer(IMoveListener endpoint) {
        this.listeners.add(endpoint);
    }

    @Override
    public ChessPiece getPromotionPiece(ChessPiece.Side side) {
        // TODO: make this a real analysis thing
        return new Queen(side);
    }

    private MovePlyProposal beginAnalysis() {
        boolean moveGenerated = true;
        float value;
        MovePlyProposal moveProposal = null;
        int maxTries = 5;
        do {
            try {
                this.output = new StringBuilder();

                // for choosing at random
                Random rand = new Random();

                if (board.getMovePlyHistory().size() < 2) { // first move
                    List<MovePlyProposal> validMoves = getStandardOpeningMoves();
                    return validMoves.get(rand.nextInt(validMoves.size()));
                }

                // ======== THINKING PART =============
                // the last move forms the basis for the tree building
                MovePly lastRealMove = (MovePly) board.getMovePlyHistory().peek();
                // it was made by the opponent. we don't have a problem with
                // nulls since we already made at least one move before now
                MovePlyNode root = new MovePlyNode(lastRealMove);
                moveTree = new MovePlyTree(root);

                // the real work comes in
                ChessBoard boardClone = new ChessBoard(board, this);
                // create the runners
                executorService = Executors.newFixedThreadPool(MAX_WORKER_THREADS);
                tasks = new ArrayList<>();
                taskFutures = new ArrayList<>();
                // for each valid move, play out on the cloned board
                value = generateRatedMoveTree(boardClone, root, MAX_ANALYSIS_DEPTH,
                        board.getSideToMove() == ChessPiece.Side.WHITE);
                // find the node(s) with the given value
                List<MovePlyProposal> sampleSpace = new ArrayList<>();
                Enumeration children = root.children();
                while (children.hasMoreElements()) {
                    MovePlyNode node = (MovePlyNode) children.nextElement();
                    // Note that we use + for the difference, because the value returned to this
                    // point will be the additive inverse of the value stored in the child
                    // node, since the child node represents the opponents turn, and according
                    // to the negamax algo, each node stores its value w.r.t whose turn it
                    // is at that point (maximize for the current player), and not w.r.t 
                    // our global rule for scoring the board position, which says maximize
                    // for WHITE (= prefer +ve values) and minimize for BLACK (prefer -ve values)
                    // So the line below comes from: diff = value - (-nodeValue)
                    float diff = value + node.getValue();
                    if (Math.abs(diff) < 0.00000001 || Double.isNaN(diff) /* for game endings with INFINITY */) {
                        sampleSpace.add(node.getMovePlyProposal());
                    }
                }
                assert (sampleSpace.size() > 0) : "Before the computer tries to think, it must have moves to play.";
                Util.writeToFile("tree.txt", moveTree.print(), false);
                if (!sampleSpace.isEmpty()) {
                    moveProposal = sampleSpace.get(rand.nextInt(sampleSpace.size()));
                }
            }
            catch (Exception ex) {
                java.util.logging.Logger.getLogger(ComputerPlayer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                moveGenerated = false;
            }
        } while (!moveGenerated && --maxTries > 0);
        if (maxTries == 0) {
            System.err.println("Couldn't generate move.");
        }
        // delete the tree contents to save memory
        this.moveTree = null;
        this.tasks = null;
        this.taskFutures = null;
        return moveProposal;
    }

    /**
     * Get the possible moves for both sides, considering king checks. It uses
     * the negamax algorithm (a specialised minimax algorithm for combinatorial
     * games) to select the value for the best node, based on checking the given
     * number of moves ahead.
     *
     * @param boardClone
     * @param node Represents the current state of the game, and stores the move
     * that led to its creation.
     * @param analysisDepth
     * @param isWhiteTurn
     * @return
     */
    private float generateRatedMoveTree(ChessBoard boardClone, MovePlyNode node, int analysisDepth,
                                        boolean isWhiteTurn) throws InterruptedException, Exception {

        int numValidMoves = 0;
        /*
        At the beginning of the method, the board values could possibly get calculated if game over
        or we've reached max analysis depth. In that case, the side to play (as by isWhiteTurn) is NOT
        the side to calculate board values for. So if isWhiteTurn is true, then it means the last
        move was by black, so we need to calculate the best position w.r.t. black.
         */
        if (boardClone.gameOver != null) {
            float value = 0;
            switch (boardClone.gameOver) {
                case CHECKMATE:
                    value = isWhiteTurn
                            ? // black won : white won
                            Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
                    break;
                case RESIGNATION:
                    value = isWhiteTurn
                            ? Float.NEGATIVE_INFINITY : Float.POSITIVE_INFINITY;
                    break;
                case DRAW_REPETITION:
                case DRAW_STALEMATE:
                    value = 0;
            }
            node.setValue(value);
            return value;
        }
        if (analysisDepth == 0) {
            // logging
            float value = (float) (isWhiteTurn ? evaluatePosition(boardClone) : -1.0 * evaluatePosition(boardClone));
            // evaluate board position for leaf states only
            node.setValue(value);
            return value;
        }
        // we might not need to look at all squares if it's king check, so we
        // create a variable that gets populated instead if we're limited
        List<BoardSquare> relevantSquares;
        // check for king check
        if (boardClone.isKingCheck()) {
            // this is just a silly and fancy use of complex stuff :P
            relevantSquares = (List) boardClone.getAllCheckResolutionMoves().stream()
                    .map(e -> boardClone.getSquareForPosition(e.getFrom()))
                    .collect(Collectors.toList());
        }
        else {
            relevantSquares = boardClone.getPositionsForCurrentSide();
        }
        ArrayList<BoardSquare> arrayList = new ArrayList(relevantSquares);
        float value = Float.NEGATIVE_INFINITY;
        for (BoardSquare from : arrayList) {
            ChessPiece p = from.getPiece();
            assert (p != null) : "p should not be null here";
            // list all the moves
            for (BoardSquare ps : boardClone.SQUARES.values()) {
                proposedMove = new MovePlyProposal(from.toString(), ps.toString());
                // DEBUG:
                if (numValidMoves == DEBUG_MAX_MOVES) {
                    break;
                }
                // In the first round just add the tasks to be performed and then move to the end section
                // which then calls the executor threads. No need to undo at this point, since we're using a new
                // board clone for each submitted move.
                if (analysisDepth == MAX_ANALYSIS_DEPTH) {
                    // try the move at this point
                    ChessBoard board_ = new ChessBoard(boardClone, ComputerPlayer.this);
                    if (board_.movePiece(proposedMove, null, true)) {
                        MovePly last = (MovePly) board_.getMovePlyHistory().peek();
                        // filter the additions
                        MovePlyNode child = new MovePlyNode(last);
                        node.add(child);
                        // the recursive exploration of the available moves for the current player
                        // is submitted to independent threads here
                        tasks.add(new Callable<Float>() {
                            @Override
                            public Float call() throws Exception {
                                return (float) -1.0 * generateRatedMoveTree(board_, child, analysisDepth - 1, !isWhiteTurn);
                            }
                        });
                        numValidMoves++;
                    }
                    continue;
                }
                // check the actual move
                if (boardClone.movePiece(proposedMove, null, true)) {
                    // grab the move
                    MovePly last = (MovePly) boardClone.getMovePlyHistory().peek();
                    // filter the additions
                    MovePlyNode child = new MovePlyNode(last);
                    node.add(child);
                    // recurse
                    value = (float) Math.max(value, -1.0 * generateRatedMoveTree(boardClone, child, analysisDepth - 1, !isWhiteTurn));
                    // revert for others to use the board for the same ply
                    boardClone.undoMove();
                    numValidMoves++;
                }
            }
            // DEBUG:
            if (numValidMoves == DEBUG_MAX_MOVES) {
                break;
            }
        }

        if (analysisDepth == MAX_ANALYSIS_DEPTH) {
            taskFutures = executorService.invokeAll(tasks);
            List<Float> values = new ArrayList<>();
            float v = 0;
            for (Future<Float> taskFuture : taskFutures) {
                v = taskFuture.get();
                values.add(v);
                value = Math.max(value, v);
            }
            executorService.shutdown();
            executorService.awaitTermination(5, TimeUnit.MINUTES);
        }
        node.setValue(value);
        return value;
    }

    public void writeLog(ChessBoard boardClone, float evaluatedValues, BufferedWriter bw) throws IOException {

        bw.append((new StringBuilder().append(boardClone.getMovePlyHistory().peek()).append("\n")
                   .append(boardClone.getMovesAnnotation()).append("\n")
                   //                .append("Material : ").append(evaluatedValues[1]).append("\n")
                   //                .append("Coverage : ").append(evaluatedValues[2]).append("\n")
                   //                .append("Pawn Str.: ").append(evaluatedValues[3]).append("\n")
                   .append("Composite: ").append(evaluatedValues).append("\n")
                   .append(boardClone.print())).toString());

    }

    /**
     * More positive values favour white. More negative values favour black. So
     * white is maximising and black is minimising. The first index stores the
     * composite, and the next 3 breaks down the components:
     * <p>
     * 1: piece advantage 2. board coverage 3. pawn advantage
     *
     * @param boardClone
     * @return
     */
    private float evaluatePosition(ChessBoard boardClone) {
        Map<Class<? extends ChessPiece>, List<BoardSquare>[]> allPieceLocations
                = boardClone.getAllPieceLocations();
        // perform some calculations
        float pawnAdvantage = evaluatePawnStructure(allPieceLocations.get(Pawn.class));
        float[] coverage = evaluateBoardCoverage(boardClone);
        float materialAdvantage = coverage[3];
//        float[] coverage2 = evaluateBoardCoverage2(boardClone);
        for (byte i = 0; i < coverage.length; i++) {
            // compare the two results
//            assert (coverage[i] == coverage2[i]);
        }
        // if white is maximising player
        // we're using weighted coverage here
        float coverageAdvantage = coverage[2];
//        return pawnAdvantage;
        return materialAdvantage;//+ pawnAdvantage + coverageAdvantage;
    }

    /**
     * Returns two arrays of 3 values per side, consisting of the following (for
     * white):
     *
     * <p>
     * index 0: total squares attacked by white's pieces, regardless of how many
     * pieces attack the same square.
     * <p>
     * index 1: total attack potential of white, which means total of white
     * pieces attacking each square.
     * <p>
     * index 2: the weighted attack potential, which rewards attacking squares
     * with a smaller valued piece. The simple formula used is the inverse of
     * the piece value, with the value for the king being 0.
     *
     *
     * @param boardClone
     * @return
     */
    private float[] evaluateBoardCoverage(ChessBoard boardClone) {
        float[] result = new float[4];
        // just determines how many squares are covered per side, regardless of degree of battery
        float totalWhiteSquares = 0;
        float totalBlackSquares = 0;
        // takes into consideration "batteries" - multiple same side pieces attacking one square
        float totalWhiteAttack = 0;
        float totalBlackAttack = 0;
        // check the value of the battery, where lower pieces covering more squares is
        // better. E.g. it's better for two pawns to attack d5, than two knights
        float totalWeightedWhiteAttack = 0;
        float totalWeightedBlackAttack = 0;
        // material advantage
        float materialAdvantage = 0;
        for (BoardSquare sq : boardClone.SQUARES.values()) {
            // get the material advantage
            if (sq.hasPiece()) {
                ChessPiece p = sq.getPiece();
                if (p.getSide() == ChessPiece.Side.WHITE) {
                    materialAdvantage += p.pieceValue();
                }
                else {
                    materialAdvantage -= p.pieceValue();
                }
            }
            // white stuff
            List<BoardSquare> whitePieces = boardClone.getAttackingPiecePositions(sq,
                    ChessPiece.Side.WHITE);
            totalWhiteSquares += whitePieces.isEmpty() ? 0 : 1;
            totalWhiteAttack += whitePieces.size();
            // we get the weighted value of the attack, with lower valued pieces
            // ranked higher, and king 0
            for (BoardSquare ws : whitePieces) {
                ChessPiece pc = ws.getPiece();
                if (pc.getClass() == King.class) {
                    continue;
                }
                totalWeightedWhiteAttack += 1.0 / pc.pieceValue();
            }
            List<BoardSquare> blackPieces = boardClone.getAttackingPiecePositions(sq,
                    ChessPiece.Side.BLACK);
            totalBlackSquares += blackPieces.isEmpty() ? 0 : 1;
            totalBlackAttack += blackPieces.size();
            for (BoardSquare bs : blackPieces) {
                ChessPiece pc = bs.getPiece();
                if (pc.getClass() == King.class) {
                    continue;
                }
                totalWeightedBlackAttack += 1.0 / pc.pieceValue();
            }
        }
        result[0] = totalWhiteSquares - totalBlackSquares;
        result[1] = totalWhiteAttack - totalBlackAttack;
        result[2] = totalWeightedWhiteAttack - totalWeightedBlackAttack;
        result[3] = materialAdvantage;
        return result;
    }

    private float[] evaluateBoardCoverage2(ChessBoard boardClone) {
        float[] result = new float[3];
        // just determines how many squares are covered per side, regardless of degree of battery
        float totalWhiteSquares = 0;
        float totalBlackSquares = 0;
        // takes into consideration "batteries" - multiple same side pieces attacking one square
        float totalWhiteAttack = 0;
        float totalBlackAttack = 0;
        // check the value of the battery, where lower pieces covering more squares is
        // better. E.g. it's better for two pawns to attack d5, than two knights
        float totalWeightedWhiteAttack = 0;
        float totalWeightedBlackAttack = 0;
        for (BoardSquare sq : boardClone.SQUARES.values()) {
            // white stuff
            List<BoardSquare> whitePieces = boardClone.getAttackingPiecePositions(sq,
                    ChessPiece.Side.WHITE);
            totalWhiteSquares += whitePieces.isEmpty() ? 0 : 1;
            totalWhiteAttack += whitePieces.size();
            // we get the weighted value of the attack, with lower valued pieces
            // ranked higher, and king 0
            for (BoardSquare ws : whitePieces) {
                ChessPiece pc = ws.getPiece();
                if (pc.getClass() == King.class) {
                    continue;
                }
                totalWeightedWhiteAttack += 1.0 / pc.pieceValue();
            }
            List<BoardSquare> blackPieces = boardClone.getAttackingPiecePositions(sq,
                    ChessPiece.Side.BLACK);
            totalBlackSquares += blackPieces.isEmpty() ? 0 : 1;
            totalBlackAttack += blackPieces.size();
            for (BoardSquare bs : blackPieces) {
                ChessPiece pc = bs.getPiece();
                if (pc.getClass() == King.class) {
                    continue;
                }
                totalWeightedBlackAttack += 1.0 / pc.pieceValue();
            }
        }
        result[0] = totalWhiteSquares - totalBlackSquares;
        result[1] = totalWhiteAttack - totalBlackAttack;
        result[2] = totalWeightedWhiteAttack - totalWeightedBlackAttack;
        return result;
    }

    /**
     * Tries to put a value to the structure of the pawns using a number of
     * criteria, easily seen from the methods it calls and the way it weights
     * their return values.
     *
     * @param pawnLocations
     * @return
     */
    protected float evaluatePawnStructure(List<BoardSquare>[] pawnLocations) {
        // transform the view for ease of analysis
        byte[][] fileSummary = new byte[2][8];
        byte[][] rankSummary = new byte[2][8];
        byte maxSize = (byte) Math.max(pawnLocations[0].size(), pawnLocations[1].size());
        for (byte i = 0; i < maxSize; i++) {
            if (i < pawnLocations[0].size()) {
                byte fileIndex = (byte) (pawnLocations[0].get(i).getFile() - 'A');
                byte rankIndex = (byte) (pawnLocations[0].get(i).getRank() - 1);
                fileSummary[0][fileIndex] = (byte) (fileSummary[0][fileIndex] + 1);
                rankSummary[0][rankIndex] = (byte) (rankSummary[0][rankIndex] + 1);
            }
            if (i < pawnLocations[1].size()) {
                byte fileIndex = (byte) (pawnLocations[1].get(i).getFile() - 'A');
                byte rankIndex = (byte) (pawnLocations[1].get(i).getRank() - 1);
                fileSummary[1][fileIndex] = (byte) (fileSummary[1][fileIndex] + 1);
                rankSummary[1][rankIndex] = (byte) (rankSummary[1][rankIndex] + 1);
            }
        }
        // pawn chain length
//        float[] chainLengthAverage = {evaluatePawnChainLengthAverage(fileSummary[0]),
//            evaluatePawnChainLengthAverage(fileSummary[1])};
//        float[] fileOccupancy = {evaluatePawnFileOccupancy(fileSummary[0]),
//            evaluatePawnFileOccupancy(fileSummary[0])};
//        float[] chainStrength = {evaluatePawnChainStrength(pawnLocations[0]),
//            evaluatePawnChainStrength(pawnLocations[1])};
        // TODO: Determine the relative contributions of these factors, at least
        // For now I'll just use some intelligent guess.
        float a = (float) (1.0 / 8),
                b = 1f, c = 1f, d = 1f;

        float white = 0;
        if (!pawnLocations[0].isEmpty()) {
            white = a * evaluatePawnChainLengthAverage(fileSummary[0])
                    + b * evaluatePawnFileOccupancy(fileSummary[0])
                    + c * evaluatePawnChainStrength(pawnLocations[0])
                    + d * evaluatePawnAdvancement(rankSummary[0], (byte) 2);
        }

        float black = 0;
        if (!pawnLocations[1].isEmpty()) {
            black = a * evaluatePawnChainLengthAverage(fileSummary[1])
                    + b * evaluatePawnFileOccupancy(fileSummary[1])
                    + c * evaluatePawnChainStrength(pawnLocations[1])
                    + d * evaluatePawnAdvancement(rankSummary[1], (byte) 7);
        }
        return white - black;
    }

    /**
     * Calculates the # files occupied to # pawns ratio for the pawn file
     * distribution given. The larger the ratio the better. Max is 1, where
     * there are no pawn stacks on a file.
     *
     * @param fileSummary
     * @return
     */
    protected float evaluatePawnFileOccupancy(byte[] fileSummary) {
        // num files occupied by pawns
        float numOccupied = 0;
        // highest stacking height
        float highestStack = 0;
        // difference between highest stack and lowest, for occupied files
        float stackingRange = 0;
        // for that we need:
        float lowestStack = Float.POSITIVE_INFINITY;
        float numPawns = 0;
        for (byte n : fileSummary) {
            numOccupied += n > 0 ? 1 : 0;
            if (n > highestStack) {
                highestStack = n;
            }
            if (n > 0 && n < lowestStack) {
                lowestStack = n;
            }
            numPawns += n;
        }

        // return if numPawns is zero
        if (numPawns == 0) {
            return 0;
        }
        if (lowestStack < Double.POSITIVE_INFINITY) {
            stackingRange = highestStack - lowestStack;
        }
        // testing\
        // used this to run the tests
//        return numOccupied + highestStack + stackingRange + numPawns
//               + (numOccupied * highestStack * stackingRange * numPawns);
        /*
        What we'd need:
        1. numOccupied/numPawns; max=1; higher is better
        2. stackingRange; max=highestStack; min=0; lower is better
        3. highestStack; max=numPawns; min=0; lower is better
         */
        return (numOccupied / numPawns) + (1 / (stackingRange + 1)) + (1 / (highestStack + 1));

    }

    /**
     * Calculates the average length of the pawn chains, in terms of file
     * adjacency and not necessarily linking suitable for defence. The latter is
     * handled by <tt>evaluatePawnChainStrength()</tt>. Since the max is 8, we
     * normalise this by dividing by 8.
     *
     * @param fileSummary
     * @return The average length of the pawn chains. Maximum = 1.
     */
    protected float evaluatePawnChainLengthAverage(byte[] fileSummary) {
        assert (fileSummary.length == 8) : "File length must be 8.";

        byte lenUnbroken = 0;
        List<Byte> unbrokenRuns = new ArrayList<>();
        for (byte n : fileSummary) {
            // check for at least one pawn on this file
            if (n > 0) {
                lenUnbroken++;
            }
            else if (lenUnbroken > 0) {
                // save out the run length we got so far
                unbrokenRuns.add(lenUnbroken);
                lenUnbroken = 0;
            }
        }
        // add the last run that we might have missed
        if (lenUnbroken > 0) {
            unbrokenRuns.add(lenUnbroken);
        }
        if (unbrokenRuns.isEmpty()) {
            // no pawns on the board; we could catch this in an upstream method
            return 0;
        }
        // use functional operations to average out
        return (float) unbrokenRuns.stream().mapToInt(e -> e).average().getAsDouble() / 8 /* the max value*/;
    }

    /**
     * Evaluates the strength of the pawn chain by looking at how many and which
     * pawns are protected by other pawns out of all the occupied files
     *
     * @param positions
     * @return
     */
    protected float evaluatePawnChainStrength(List<BoardSquare> pawnLocations) {
        if (pawnLocations.isEmpty()) {
            return 0;
        }
        Map<BoardSquare, Byte> defence = new HashMap<>();
        // run a nested loop over this collection - at most 64 iterations in all
        // and get some other data too
        // TODO: analyse the structure of the chain, to know if the weak pawns
        // are at the middle of a chain or end, and if closer to board centre
        // or flank. Normally, prefer ^ shaped chains to v shaped, ich denke.
        for (BoardSquare loc : pawnLocations) {
            // look around at others
            for (BoardSquare loc2 : pawnLocations) {
                if (loc2.getPiece().isAttackMove(loc2, loc)) {
                    if (defence.containsKey(loc)) {
                        defence.put(loc, (byte) (defence.get(loc) + 1));
                    }
                    else {
                        defence.put(loc, (byte) 1);
                    }
                }
            }
        }
        // ratio of protected to total; bigger is better. Max = 1.
//        float defended = defence.values().stream().filter(e -> e > 0).count();
        float defended = defence.size();
        return defended / pawnLocations.size();
    }

    /**
     * Evaluates the degree of advancement of the pawns into the enemy
     * territory, one side at a time. It is best if all white pawns are at rank
     * 8, or all black pawns at rank 1 for black. In these cases, the average
     * pawn advancement above the starting rank is 7 (since no pawns ever get to
     * its home rank). We normalise by dividing the result by 7, the maximum.
     *
     * @param rankSummary
     * @param startingRank The rank that the pawns start on, 2 for white and 7
     * for black
     * @return
     */
    protected float evaluatePawnAdvancement(byte[] rankSummary, byte startingRank) {
        float num = 0;
        byte count = 0;
        for (byte i = 0; i < rankSummary.length; i++) {
            num += rankSummary[i] /* rank 1 to 8 are in indexes 0 - 7 resp.*/
                   * Math.abs(i - (startingRank - 1))/* how far advanced; +1 corrects for index vs. rank */;
            count += rankSummary[i];
        }
        if (count == 0) {  // no pawns
            return 0;
        }
        return (float) (num / count) /*average advancement*/ / 6/*  the max avg. advancement */;
    }

    public void initiate(ChessBoard board) {
        this.board = board;
        this.moveTree = new MovePlyTree();
        this.output = new StringBuilder();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                MovePlyProposal m = beginAnalysis();
                for (IMoveListener listener : listeners) {
                    listener.chessMoveProposed(m);
                }
            }
        }, 500);

    }

    public void addListener(IMoveListener listener) {
        listeners.add(listener);
    }

    public MovePlyTree getMoveTree() {
        return moveTree;
    }

    private List<MovePlyProposal> getStandardOpeningMoves() {
        List<MovePlyProposal> moves = new ArrayList<>();
        ChessPiece.Side sideToMove = board.getSideToMove();
        int rankDiff = sideToMove == ChessPiece.Side.WHITE ? -1 : 1;
        int startingRank = sideToMove == ChessPiece.Side.WHITE ? 2 : 7;
        int rank = startingRank - rankDiff * 2;
        String files = "CDE";
        for (Character c : files.toCharArray()) {
            moves.add(new MovePlyProposal(c.toString() + startingRank, c.toString() + rank));
        }
        startingRank = startingRank + rankDiff;
        rank = startingRank - rankDiff * 2;
        // add knight moves
        moves.add(new MovePlyProposal("B" + (startingRank), "C" + rank));
        moves.add(new MovePlyProposal("G" + (startingRank), "F" + rank));

        return moves;
    }

}
