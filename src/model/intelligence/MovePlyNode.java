/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.intelligence;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import model.MovePly;
import model.MovePlyProposal;

/**
 *
 * @author e.ubachukwu
 */
public class MovePlyNode extends DefaultMutableTreeNode {

    private float value;
    // to save memory, since we have literally 100,000s of these
    private final char[] squares;

    public MovePlyNode(MovePly move) {
        this.userObject = move.toString();
        String from = move.getFrom();
        String to = move.getTo();
        this.squares = new char[]{from.charAt(0), from.charAt(1), to.charAt(0), to.charAt(1)};
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public String getFrom() {
        return String.valueOf(squares, 0, 2);
    }

    public String getTo() {
        return String.valueOf(squares, 2, 2);
    }

    public MovePlyProposal getMovePlyProposal() {
        return new MovePlyProposal(getFrom(), getTo());
    }

    @Override
    public void add(MutableTreeNode newChild) {
//        // store the cumulatives for the ancestors of the added node, so 
//        // we don't have to walk up the tree recursively each time we need the
//        // branch value. This is simply optimisation by caching.
//        MovePlyNode child = ((MovePlyNode) newChild);
//        child.cumulativeValue = getMoveBranchValue();
//        if (getLevel() == (ComputerPlayer.MAX_ANALYSIS_DEPTH - 1)) {
//            // we are adding to the last level, so be picky
//            // check to see if to allow this move to stay, based on it's cumulative
//            // value considering its ancestry
//            float v = child.getMoveBranchValue();
//            MovePlyNode root = (MovePlyNode) getRoot();
//            if (v < root.maxValueSeen) {
//                return;
//            }
//            root.maxValueSeen = v;
//        }
        super.add(newChild);
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }

    public boolean hasParent() {
        return parent != null;
    }

    @Override
    public String toString() {
        return String.format("%1$s (%2$.4f)", userObject, value);
    }

}
