/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.intelligence;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import model.MovePlyProposal;

/**
 *
 * @author e.ubachukwu
 */
public class MovePlyTree {

    private MovePlyNode root;

    public MovePlyTree() {
    }

    public MovePlyTree(MovePlyNode root) {
        this.root = root;
    }

    public void setRoot(MovePlyNode root) {
        this.root = root;
    }

    public MovePlyNode getRoot() {
        return root;
    }

    public int evaluateBranch() {
        return 0;
    }

    public List<MovePlyNode> getLeaves() {
        List<MovePlyNode> leaves = new ArrayList<>();
        Enumeration e = root.postorderEnumeration();
        while (e.hasMoreElements()) {
            MovePlyNode n = (MovePlyNode) e.nextElement();
            if (n.isLeaf()) {
                leaves.add(n);
            }
        }
        return leaves;
    }

    public List<MovePlyNode> getNonLeaves() {
        List<MovePlyNode> leaves = new ArrayList<>();
        Enumeration e = root.postorderEnumeration();
        while (e.hasMoreElements()) {
            MovePlyNode n = (MovePlyNode) e.nextElement();
            if (!n.isLeaf()) {
                leaves.add(n);
            }
        }
        return leaves;
    }

    /**
     * Take away all the branches that are shorter or have lower values at the
     * leaf
     */
    public MovePlyProposal prune() {
        boolean done = false;
        // remove unproductive leaf nodes first
        // check for short branches and remove them recursively, because
        // their children didn't qualify to be added
//        while (!done) {
//            done = true;
//            for (MovePlyNode leaf : getLeaves()) {
//                if (leaf.getLevel() < ComputerPlayer.MAX_ANALYSIS_DEPTH
//                        || leaf.getMoveBranchValue() < root.getMaxValueSeen()) {
//                    leaf.removeFromParent();
//                    // if you removed at least one, then go again to check if the remaining
//                    // leaves meet the criteria for inclusion in the tree
//                    done = false;
//                }
//            }
//        }
        return null;
    }

    public String print() {
        return printNode(root, 0);
    }

    private String printNode(MovePlyNode node, int level) {
        if (node == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("\n");
        for (int i = 0; i < level; i++) {
            sb.append("\t");
        }
        sb.append(node.toString());
        // print children recursively
        Enumeration children = node.children();
        while (children.hasMoreElements()) {
            sb.append(printNode((MovePlyNode) children.nextElement(), level + 1));
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return root + " [root]";
    }

}
