package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class Knight extends ChessPiece {

    public static final String SYMBOL = "N";

    public Knight(Side side) {
        super(side);
    }

    public Knight(Knight clonedPiece) {
        super(clonedPiece);
    }

    /**
     * Returns the name (symbol) of this chess piece.
     *
     * @return
     */
    @Override
    public String toString() {
        return getSide().name() + " Knight";
    }

    @Override
    public boolean canMakeMove(BoardSquare from, BoardSquare to, ChessBoard board) {
        boolean isNormalMove = isNormalMove(from, to); // the move is valid AND
        boolean isValid
                = // the destination is empty OR opponent on destination
                (!to.hasPiece() || to.getPiece().getSide() != getSide());
        return isNormalMove && isValid;
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        char fromFile = from.getFile();
        int fromRank = from.getRank();
        char toFile = to.getFile();
        int toRank = to.getRank();
        int absRankDiff =Math.abs(fromRank - toRank);
        int absFileDiff =Math.abs(fromFile - toFile);

        // first approach
        return (absFileDiff + absRankDiff) == 3
               && absFileDiff != 0 && absRankDiff != 0;
        // second approach
//        return (Math.abs(fromFile - toFile) == 2 && Math.abs(fromRank - toRank) == 1)
//                || (Math.abs(fromFile - toFile) == 2 && Math.abs(fromRank - toRank) == 1);

        // Explanation of first approach
        /**
         * The Knight moves in an L shape, meaning it either moves TWO spaces to
         * the right/left and ONE space up/down, or else it moves TWO spaces
         * up/down and ONE space to the right/left. That means it either jumps
         * TWO files and ONE rank (e.g. from A1 to C1 (two files) and then C1 to
         * C2 (one rank)), or ONE file and TWO ranks (e.g. from A1 to B1 and
         * then B1 to B3). This means that the sum of the ranks jumped and the
         * files jumped must always give 3. Now we don't care if it moves to the
         * left and up, or to the right and down, or... The important thing is
         * that the difference between the current rank (fromRank) and the
         * destination rank (toRank) must be one or two (with the difference
         * between the current file (fromFile) and the destination file (toFile)
         * being two or one respectively. That's why we use Math.abs(x), which
         * yields the absolute value of x, |x| NOTE: 1+2 = 2+1 = 0+3 = 3+0 = 3.
         * This means that we must be sure we're not dealing with 0+3 or 3+0,
         * i.e. the fromRank must be different from the toRank and the fromFile
         * must be different from the toFile. If not, if we move the Knight from
         * A1 to D1, change in rank = 0, and change in file = 3, therefore our
         * approach would have yielded TRUE, which is incorrect. Hence there is
         * a condition that (Math.abs(fromFile - toFile)!=0 && Math.abs(fromRank
         * - toRank)!=0)
         */
        // Explanation of the second approach
        /**
         * This approach is straight forward: either change in rank = 2 and
         * change in file = 1 for the move, or change in rank = 1 and change in
         * file = 2. Shikena!!
         */
    }

    @Override
    public String toShortString() {
        return "N" + getSide().toString();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        return new ArrayList<>();
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 3 : 4;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 3;
    }

}
