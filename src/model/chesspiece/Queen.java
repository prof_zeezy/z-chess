package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class Queen extends ChessPiece {

    public static final String SYMBOL = "Q";

    public Queen(Side side) {
        super(side);
    }

    public Queen(Queen clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public String toString() {
        return getSide().name() + " Queen";
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
//        boolean isRookMove = new Rook(side, null).isNormalMove(from, to);
//        boolean isBishopMove = new Bishop(side, null).isNormalMove(from, to);
//        return isRookMove || isBishopMove;
        // the above code was slower, and we need efficiency
        // get rank difference
        int fromRank = from.getRank();
        int fromFile = from.getFile();
        int toRank = to.getRank();
        int toFile = to.getFile();
        int rankDiff = toRank - fromRank;
        int fileDiff = toFile - fromFile;

        return // bishop move
                (Math.abs(rankDiff) == Math.abs(fileDiff)
                 && (from.getFile() - to.getFile() != 0))
                || // or rook move
                (fileDiff == 0 && Math.abs(rankDiff) > 0
                 || rankDiff == 0 && Math.abs(fileDiff) > 0);
    }

    @Override
    public String toShortString() {
        return "Q" + getSide().toString();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {

        List<BoardSquare> blockingSquares = new ArrayList<>();
        if (isNormalMove(from, to)) {
            // get rank difference
            int rankDiff = to.getRank() - from.getRank();
            int fileDiff = to.getFile() - from.getFile();
            int fromRank = from.getRank();
            int fromFile = from.getFile();
            int maxDiff = Math.max(Math.abs(fileDiff), Math.abs(rankDiff));
            int rankFactor = rankDiff == 0 ? 0 : 1;
            int fileFactor = fileDiff == 0 ? 0 : 1;
            // walk through the intervening squares
            for (int i = 1; i < maxDiff; i++) {
                int rankStep = (int) Math.copySign(i * rankFactor, rankDiff);
                int fileStep = (int) Math.copySign(i * fileFactor, fileDiff);
                int newRank = fromRank + rankStep;
                int newFile = fromFile + fileStep;
                BoardSquare s = board.getSquareForPosition((char) newFile, newRank);
                blockingSquares.add(s);
            }
        }
        return blockingSquares;
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 9 : 10;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 8;
    }
}
