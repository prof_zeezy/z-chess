package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public abstract class ChessPiece {

    protected Side side;
    protected boolean hasMoved;

    public enum Side {

        BLACK, WHITE;

        @Override
        public String toString() {
            // this gives "w" or "b" depending on if WHITE or BLACK
            return this == WHITE ? "w" : "b";
        }

        /**
         * Switch the side: if white, return black, and vice versa.
         *
         * @return
         */
        public Side otherSide() {
            // if the current side is 
            return this == Side.WHITE ? Side.BLACK : Side.WHITE;
        }
    };

    /**
     *
     * @param side the value of colour
     */
    public ChessPiece(Side side) {
        this.side = side;
    }

    public ChessPiece(ChessPiece clonedPiece) {
        this.side = clonedPiece.side;
    }

    public final boolean hasMoved() {
        return hasMoved;
    }

    public final void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    /**
     * Returns true if the given piece can jump from the "from"
     * BoardSquareWidget to the "to" square, according to the rules of chess.
     *
     * @param from The starting square to jump from.
     * @param to The destination square.
     * @return True if canMakeMove is valid.
     */
    public abstract boolean isNormalMove(BoardSquare from, BoardSquare to);

    /**
     * Gets the squares that lie in the line of attack of a piece between the
     * starting square and a destination square, excluding the start and
     * destination squares. That is, if a piece is on any of these squares, it
     * can block the movement between start and destination. E.g. if the piece
     * is a bishop on C1, then between its position and F4, there are two
     * squares, namely D2 and E3. Again, for a Knight there are no intervening
     * squares because a Knight can always jump. For a rook on C1, between C1
     * and C8 there are 6 blocking squares.
     *
     * @param from The starting BoardSquare
     * @param to The destination square
     * @return The List of intervening squares between the start and the stop,
     * excluding the starting and stopping squares.
     */
    public final List<ChessPiece> getBlockingPieces(BoardSquare from, BoardSquare to, ChessBoard board) {
        // Initialise the list
        List<ChessPiece> blockingPieces = new ArrayList<>();
        List<BoardSquare> blockingSquares = getBlockingSquares(from, to, board);
        for (BoardSquare s : blockingSquares) {
            ChessPiece p = s.getPiece();
            if (p != null) {
                blockingPieces.add(p);
            }
        }
        return blockingPieces;
    }

    /**
     * States whether the piece on the board can make a move from <tt>from</tt>
     * to <tt>to</tt> squares, which involves either capture (where
     * <tt>to</tt> has an opposing piece) or just movement (in the case where
     * <tt>to</tt> is empty.
     *
     * @param from
     * @param to
     * @return the boolean
     */
    public boolean canMakeMove(BoardSquare from, BoardSquare to, ChessBoard board) {
        ChessPiece p = to.getPiece();
        boolean replacementAllowed = p == null || p.getSide() != getSide();
        return canAttackOrDefend(from, to, board) && replacementAllowed;
    }

    /**
     * Says whether the chess piece on the <tt>from</tt> square can capture the
     * piece on the <tt>to</tt> square. For pawns, this is overridden and we
     * check first if we can make an en passant move to the square directly
     * 'behind' the opponent (w.r.t. the opponent's direction of move). This
     * method returns the square which the capturing opponent lands on, which is
     * the same as the <tt>to</tt> square for all except the pawn during en
     * passant, where the latter is different. It's this behavior of the pawn
     * that inspired the non-boolean return type.
     *
     * @param from The square of the attacking piece
     * @param to The square to be captured by the attacking piece
     * @return The square on which the capturing piece lands during capture,
     * which is different from the <tt>to</tt> square for the pawn capturing en
     * passant.
     */
    public BoardSquare canCapturePiece(BoardSquare from, BoardSquare to, ChessBoard board) {
        ChessPiece p = to.getPiece();
        boolean canAttack = canAttackOrDefend(from, to, board);
        boolean isEnemyPiece = p != null && p.getSide() != getSide();
        return canAttack && isEnemyPiece ? to : null;
    }

    public abstract List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board);

    /**
     * States if this move is a valid attack move for the piece, assuming an
     * empty board.
     *
     * @param from
     * @param to
     * @return
     */
    public boolean isAttackMove(BoardSquare from, BoardSquare to) {
        return isNormalMove(from, to);
    }

    /**
     * States if this piece on <tt>from</tt> is currently attacking/defending
     * the <tt>to</tt> square. It basically ignores what piece is there, and
     * rather focuses on if it's possible for the piece to attack or defend
     * anything there. This move is a combination of isAttackMove(from, to) and
     * no-blocking.
     *
     * @param from
     * @param to
     * @return
     */
    public boolean canAttackOrDefend(BoardSquare from, BoardSquare to, ChessBoard board) {
        return isAttackMove(from, to) && getBlockingPieces(from, to, board).isEmpty();
    }

    /**
     * The getter method for side
     *
     * @return
     */
    public final Side getSide() {
        return side;
    }

    public final int sortValue(boolean isSortedBySide) {
        if (isSortedBySide && side == Side.BLACK) {
            return sortValue() + 100;
        }
        return sortValue();
    }

    public abstract int sortValue();

    /**
     *
     * @return the double
     */
    public abstract double pieceValue();

    @Override
    public abstract String toString();

    public abstract String toShortString();

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ChessPiece)) {
            return false;
        }
        return hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + toShortString().hashCode();
        return hash;
    }

}
