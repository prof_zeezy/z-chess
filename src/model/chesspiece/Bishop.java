package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class Bishop extends ChessPiece {

    public static final String SYMBOL = "B";

    public Bishop(Side side) {
        super(side);
    }

    public Bishop(Bishop clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public String toString() {
        return getSide().name() + " Bishop";
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        // for the bishop, the rank difference = the file difference
        return Math.abs(from.getFile() - to.getFile()) == Math.abs(from.getRank() - to.getRank())
               && (from.getFile() - to.getFile() != 0);
    }

    @Override
    public String toShortString() {
        return "B" + getSide().toString();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        List<BoardSquare> blockingSquares = new ArrayList<>();
        if (isNormalMove(from, to)) {
            // get rank difference
            int rankDiff = to.getRank() - from.getRank();
            int fileDiff = to.getFile() - from.getFile();
            int fromRank = from.getRank();
            int fromFile = from.getFile();
            // walk through the intervening squares
            for (int i = 1; i < Math.abs(rankDiff); i++) {
                int rankStep = (int) Math.copySign(i, rankDiff);
                int fileStep = (int) Math.copySign(i, fileDiff);
                int newRank = fromRank + rankStep;
                int newFile = fromFile + fileStep;
                BoardSquare s = board.getSquareForPosition((char) newFile, newRank);
                blockingSquares.add(s);
            }
        }
        return blockingSquares;
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 5 : 6;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 3;
    }

}
