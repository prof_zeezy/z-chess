package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class Rook extends ChessPiece {

    public static final String SYMBOL = "R";

    public Rook(Side side) {
        super(side);
    }

    public Rook(Rook clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public String toString() {
        return getSide().name() + " Rook";
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        return from.getFile() == to.getFile() && Math.abs(from.getRank() - to.getRank()) > 0
               || from.getRank() == to.getRank() && Math.abs(from.getFile() - to.getFile()) > 0;
    }

    @Override
    public String toShortString() {
        return "R" + getSide().toString();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {        
        List<BoardSquare> blockingSquares = new ArrayList<>();
        if (isNormalMove(from, to)) {
            // get rank difference
            int rankDiff = to.getRank() - from.getRank();
            int fileDiff = to.getFile() - from.getFile();
            int fromRank = from.getRank();
            int fromFile = from.getFile();
            int maxDiff = Math.max(Math.abs(fileDiff), Math.abs(rankDiff));
            // get the bigger of the two differences: the other one must be zero
            boolean isRankMove = Math.abs(rankDiff) > Math.abs(fileDiff);
            // walk through the intervening squares
            for (int i = 1; i < maxDiff; i++) {
                int rankStep = 0;
                int fileStep = 0;
                if (isRankMove) {
                    rankStep = (int) Math.copySign(i, rankDiff);
                }
                else {
                    fileStep = (int) Math.copySign(i, fileDiff);
                }
                int newRank = fromRank + rankStep;
                int newFile = fromFile + fileStep;
                BoardSquare s = board.getSquareForPosition((char) newFile, newRank);
                blockingSquares.add(s);
            }
        }
        return blockingSquares;
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 7 : 8;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 5;
    }

}
