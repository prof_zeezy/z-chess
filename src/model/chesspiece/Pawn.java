package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;
import model.MovePly;

/**
 *
 * @author eziama
 */
public class Pawn extends ChessPiece {

    public static final String SYMBOL = "P";

    public Pawn(Side side) {
        super(side);
    }

    public Pawn(Pawn clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public String toString() {
        return getSide().name() + " Pawn";
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        int rankDifference = side == Side.WHITE ? 1 : -1;
        int homeRank = side == Side.WHITE ? 2 : 7;
        boolean isCorrectRank = (to.getRank() == from.getRank() + rankDifference)
                                // if first moveIsPossible, i.e. starting from the start rank, then rank difference can be two
                                || (from.getRank() == homeRank && to.getRank() == from.getRank() + (2 * rankDifference));
        boolean isCorrectFile = from.getFile() == to.getFile();
        return (isCorrectFile && isCorrectRank);
    }

    @Override
    public boolean isAttackMove(BoardSquare from, BoardSquare to) {
        // check if the square to be attacked is directly diagonally ahead of the pawn position
        // This is true if for white, the piece moves one file right/left and one rank up (increasing)
        // For black, same thing but one rank down (decreasing)
        int rankDifference = this.side == Side.WHITE ? 1 : -1; // 1=going up; -1=going down
        return Math.abs(from.getFile() - to.getFile()) == 1 // difference of one file left/right
               && to.getRank() == from.getRank() + rankDifference; // difference of one file up/down
    }

    /**
     * Overridden here because of the pawn's special en passant move.
     *
     * @param from
     * @param to
     * @return
     */
    @Override
    public BoardSquare canCapturePiece(BoardSquare from, BoardSquare to, ChessBoard board) {
        boolean isNormalCapture = super.canCapturePiece(from, to, board) != null;
        // for checking en passant capture, we get the square on which the 
        // capturing piece would've landed, i.e. the square behind the opponent
        char file = to.getFile();
        int rank = this.getSide() == Side.WHITE ? from.getRank() + 1 : from.getRank() - 1;
        BoardSquare s = board.getSquareForPosition(file, rank);
        boolean isEnpassantCapture = isEnPassant(from, s, board);
        if (isNormalCapture) {
            return to;
        }
        else if (isEnpassantCapture) {
            return s;
        }
        return null;
    }

    @Override
    public boolean canMakeMove(BoardSquare from, BoardSquare to, ChessBoard board) {
        ChessPiece p = to.getPiece();
        // if it's a defending move, then the piece on the "to" square should be 
        // same color and non-null
        // progressively check the conditions
        boolean isNormalMove = isNormalMove(from, to),
                isAttackMove = isAttackMove(from, to);
        if (!isNormalMove && !isAttackMove) {
            return false;
        }
        if (isNormalMove) {
            return !to.hasPiece() && getBlockingPieces(from, to, board).isEmpty();
        }
        boolean replacementAllowed = p != null && p.getSide() != getSide();
        return replacementAllowed || isEnPassant(from, to, board);
    }

    /**
     * Checks if this move is the en passant move.
     *
     * @param from
     * @param to
     * @return
     */
    public boolean isEnPassant(BoardSquare from, BoardSquare to, ChessBoard board) {
        // this is the last move and the first to be retrieved since it's on the
        // top of the stack; we're using it as a stack and adding elements
        // via push()
        boolean isEmpty = board.getMovePlyHistory().empty();
        if (isEmpty) {
            return false;
        }
        MovePly lastMove = (MovePly) board.getMovePlyHistory().peek();
        if (lastMove == null) {
            return false;
        }
        ChessPiece lastPiece = lastMove.getPiece();
        if (!(lastPiece instanceof Pawn)) {
            return false;
        }
        BoardSquare lastFrom = board.SQUARES.get(lastMove.getFrom());
        BoardSquare lastTo = board.SQUARES.get(lastMove.getTo());
        // white can only move en passant if it's already in rank 5, two ranks away
        // from blacks home pawn rank, and vice versa (i.e. rank 4 for black's en passant)
        boolean correctRank = from.getRank() == (this.getSide() == Side.WHITE ? 5 : 4);
        if (!correctRank) {
            return false;
        }
        // the opponent must have moved two steps
        boolean correctDistance = Math.abs(lastFrom.getRank() - lastTo.getRank()) == 2;
        if (!correctDistance) {
            return false;
        }
        // and on the file next to this one's rank
        boolean correctFile = Math.abs(lastFrom.getFile() - from.getFile()) == 1;
        if (!correctFile) {
            return false;
        }
        // and this move must be the correct en passant move, i.e. an attack move on the 
        // side of the offending pawn
        // redo this move manually so as to avoid deadlocks, since isAttackMove calls
        // enpassant
        int rankDifference = this.side == Side.WHITE ? 1 : -1; // 1=going up; -1=going down
        boolean isNormalAttack = Math.abs(from.getFile() - to.getFile()) == 1 // difference of one file left/right
                                 && to.getRank() == from.getRank() + rankDifference; // difference of one file up/down
        return isNormalAttack && to.getFile() == lastTo.getFile();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        // Initialise the list
        List<BoardSquare> blockingSquares = new ArrayList<>();
        int step = getSide() == Side.WHITE ? 1 : -1;
        // 1. The rank difference would be two for there to be a blocking square
        if (Math.abs(from.getRank() - to.getRank()) == 2 && isNormalMove(from, to)) {
            blockingSquares.add(board.getSquareForPosition(from.getFile(), from.getRank() + step));
        }
        return blockingSquares;
    }

    @Override
    public String toShortString() {
        return "P" + getSide().toString();
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 1 : 2;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 1;
    }
}
