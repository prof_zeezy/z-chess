package model.chesspiece;

import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class DummyPiece extends ChessPiece {

    public static final String SYMBOL = "X";

    public DummyPiece(Side side) {
        super(side);
    }

    public DummyPiece(DummyPiece clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        return false;
    }

    @Override
    public String toString() {
        return "DUMMY PIECE";
    }

    @Override
    public String toShortString() {
        return "X";
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        return null;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 0;
    }

    @Override
    public boolean canMakeMove(BoardSquare from, BoardSquare to, ChessBoard board) {
        return false;
    }

    @Override
    public int sortValue() {
        return -1;
    }

}
