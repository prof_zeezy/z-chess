package model.chesspiece;

import java.util.ArrayList;
import java.util.List;
import model.BoardSquare;
import model.ChessBoard;

/**
 *
 * @author eziama
 */
public class King extends ChessPiece {

    public static final String SYMBOL = "K";

    public King(Side side) {
        super(side);
    }


    public King(King clonedPiece) {
        super(clonedPiece);
    }

    @Override
    public String toString() {
        return getSide().name() + " King";
    }

    @Override
    public boolean isNormalMove(BoardSquare from, BoardSquare to) {
        return isAttackMove(from, to);
    }

    @Override
    public boolean isAttackMove(BoardSquare from, BoardSquare to) {
        int rankDiff = Math.abs(from.getRank() - to.getRank());
        int fileDiff = Math.abs(from.getFile() - to.getFile());
        return (rankDiff <= 1 && fileDiff <= 1 && (rankDiff + fileDiff) > 0);
    }

    @Override
    public BoardSquare canCapturePiece(BoardSquare from, BoardSquare to, ChessBoard board) {

        ChessPiece.Side attackingSide = side.otherSide();
        return super.canCapturePiece(from, to, board) != null // is normal capturing 
               // and to square isn't being defended
               && board.getAttackingPiecePositions(to, attackingSide).isEmpty()
               ? to : null;
    }

    /**
     * Ensure that the king, in addition to being able to make the move, does
     * not move to an attacked square. This method MUST NOT call
     * canAttackOrDefend(), since the latter is called by
     * getAttackingPiecePositions(), which in turn is called from this method.
     * Otherwise, a circular reference is created and ozo i ga-anu bukwa Stack
     * Overflow!!!
     *
     * @param from
     * @param to
     * @return
     */
    @Override
    public boolean canMakeMove(BoardSquare from, BoardSquare to, ChessBoard board) {
        ChessPiece p = to.getPiece();
        return // is a normal move to an unattacked square
                (isNormalMove(from, to)
                 && board.getAttackingPiecePositions(to, side.otherSide()).isEmpty()
                 && (p == null || p.getSide() != side))
                || // is castling and valid
                (canCastle(from, to, board));
    }

    /**
     * Gets the squares relevant to the castling side rook: the initial square
     * of the rook and the final square of the rook after successful castling,
     * respectively.
     *
     * @param from
     * @param to
     * @return
     */
    public BoardSquare[] getCastlingRookSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        int rank = side == ChessPiece.Side.WHITE ? 1 : 8;
        boolean isKingSide = to.getFile() > from.getFile();
        BoardSquare rookSquare = isKingSide ? board.SQUARES.get("H" + rank) : board.SQUARES.get("A" + rank);
        BoardSquare castledRookSquare = isKingSide ? board.SQUARES.get("F" + rank) : board.SQUARES.get("D" + rank);
        return new BoardSquare[]{rookSquare, castledRookSquare};
    }

    public boolean canCastle(BoardSquare from, BoardSquare to, ChessBoard board) {
        // check if the king is moving two steps left/right
        if (!isCastlingMove(from, to)) {
            return false;
        }
        BoardSquare rookSquare = getCastlingRookSquares(from, to, board)[0];
        ChessPiece p = rookSquare.getPiece();
        boolean hasUnmovedRook = p != null && p instanceof Rook && !p.hasMoved;
        if (!hasUnmovedRook || hasMoved) {
            return false;
        }
        boolean noAttackers = true;
        boolean noBlockers = true;
        // all the pieces on the relevant squares must not be under attack, i.e.
        // the king, castling-side rook, and intervening squares
        List<BoardSquare> relevantSquares = p.getBlockingSquares(rookSquare, from, board);
        relevantSquares.add(from);
        relevantSquares.add(rookSquare);
        ChessPiece.Side attackingSide = side.otherSide();
        for (BoardSquare sq : relevantSquares) {
            if (!board.getAttackingPiecePositions(sq, attackingSide).isEmpty()) {
                noAttackers = false;
                break;
            }
            // ensure no piece is in the intervening squares
            if (!sq.equals(rookSquare) && !sq.equals(from)) {
                if (sq.hasPiece()) {
                    noBlockers = false;
                    break;
                }
            }
        }
        return noAttackers && noBlockers;
    }

    @Override
    public String toShortString() {
        return "K" + getSide().toString();
    }

    @Override
    public List<BoardSquare> getBlockingSquares(BoardSquare from, BoardSquare to, ChessBoard board) {
        return new ArrayList<>();
    }

    @Override
    public int sortValue() {
        return this.side == Side.WHITE ? 11 : 12;
    }

    /**
     *
     * @return the double
     */
    @Override
    public double pieceValue() {
        return 0;
    }

    private boolean isCastlingMove(BoardSquare from, BoardSquare to) {
        boolean isCastlingMove = Math.abs(from.getFile() - to.getFile()) == 2
                                 && (from.getRank() == to.getRank());
        int rank = side == Side.WHITE ? 1 : 8;
        isCastlingMove = isCastlingMove && from.getRank() == rank;
        if (!isCastlingMove) {
            return false;
        }
        if (hasMoved) {
            return false;
        }
        return true;
    }

}
