/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.chesspiece.Bishop;
import model.chesspiece.ChessPiece;
import model.chesspiece.DummyPiece;
import model.chesspiece.King;
import model.chesspiece.Knight;
import model.chesspiece.Pawn;
import model.chesspiece.Queen;
import model.chesspiece.Rook;

/**
 *
 * @author e.ubachukwu
 */
public class ChessPieceFactory {

    public static ChessPiece createPiece(String pieceAndSide) {
        ChessPiece.Side side = pieceAndSide.charAt(1) == 'w'
                               ? ChessPiece.Side.WHITE : ChessPiece.Side.BLACK;
        pieceAndSide = pieceAndSide.substring(0, 1);
        return createPiece(pieceAndSide, side);
    }

    public static ChessPiece createPiece(String piece, ChessPiece.Side side) {
        if (piece == null) {
            return new Pawn(side);
        }
        piece = piece.toUpperCase();
        piece = piece.substring(0, 1);
        if (piece.equals(King.SYMBOL)) {
            return new King(side);
        }
        if (piece.equals(Pawn.SYMBOL)) {
            return new Pawn(side);
        }
        if (piece.equals(Queen.SYMBOL)) {
            return new Queen(side);
        }
        if (piece.equals(Knight.SYMBOL)) {
            return new Knight(side);
        }
        if (piece.equals(Bishop.SYMBOL)) {
            return new Bishop(side);
        }
        if (piece.equals(Rook.SYMBOL)) {
            return new Rook(side);
        }
        return new DummyPiece(side);
    }
}
