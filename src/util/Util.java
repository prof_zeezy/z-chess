package util;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.BoardSquare;
import model.MovePly;
import model.MovePlyProposal;
import model.chesspiece.ChessPiece;
import model.intelligence.ComputerPlayer;

/**
 *
 * @author eziama
 */
public class Util {

    public static String printList(List list) {
        StringBuilder sb = new StringBuilder();
        for (Object o : list) {
            if (o.getClass().isArray()) {
                sb.append(printArray((Object[]) o));
            }
            else {
                sb.append(o.toString());
            }
            sb.append("; ");
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf("; "));
        }
        return sb.toString();
    }
    
    public static byte b(int n){
        return (byte) n;
    }

    public static String printMoveList(List<MovePly> moves) {
        StringBuilder sb = new StringBuilder();
        for (MovePly m : moves) {
            sb.append(m.toString());
            sb.append("; ");
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf("; "));
        }
        return sb.toString();
    }

    public static String printMoveProposalList(List<MovePlyProposal> moves) {
        StringBuilder sb = new StringBuilder();
        for (MovePlyProposal m : moves) {
            sb.append(m.toString());
            sb.append("; ");
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf("; "));
        }
        return sb.toString();
    }

    public static String printMap(Map<? extends Object, ? extends Object> map) {
        return printMap(map, ":", "\n");
    }

    public static String printMap(Map<? extends Object, ? extends Object> map, String keyValSeparator, String lineEnd) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry m : map.entrySet()) {
            sb.append(m.getKey().toString()).append(keyValSeparator).append(m.getValue().toString());
            sb.append(lineEnd);
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf(lineEnd));
        }
        return sb.toString();
    }

    public static String printPieceBoardSquareList(List<BoardSquare> squares) {
        StringBuilder sb = new StringBuilder();
        for (BoardSquare s : squares) {
            ChessPiece piece = s.getPiece();
            if (piece != null) {
                sb.append(piece.toString()).append(" on ");
            }
            sb.append(s.toString());
            sb.append(", ");
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf(", "));
        }
        return sb.toString();
    }

    public static String printArray(Object[] array) {
        StringBuilder sb = new StringBuilder();
        for (Object o : array) {
            sb.append(o.toString()).append(", ");
        }
        if (sb.length() > 0) {
            return sb.toString().substring(0, sb.lastIndexOf(", "));
        }
        return sb.toString();
    }

    public static void writeToFile(String filename, String content, boolean shouldAppend) {
        try {
            File f = new File(filename);
            if (!f.exists()) {
                File d = new File("logs");
                if (!d.exists() || !d.isDirectory()) {
                    d.mkdir();
                }
                f.createNewFile();
            }
        }
        catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (FileWriter fw = new FileWriter(filename, shouldAppend);
                BufferedWriter bw = new BufferedWriter(fw)) {
            bw.append(content);
        }
        catch (IOException ex) {
            Logger.getLogger(ComputerPlayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static JFileChooser useFileSaverDialog(Component parent, String saveFileName,
                                                  List<String[]> fileFilters, String dialogTitle, boolean acceptAllFilters) {
        // ensure user doesn't inadvertently overwrite a file
        JFileChooser saveRunFileChooser = new JFileChooser() {
            @Override
            public void approveSelection() {
                File saveFile = this.getSelectedFile();
                if (saveFile.exists()) {
                    // confirm there's no overwrite, or it's intentional
                    int response = JOptionPane.showConfirmDialog(this,
                            "Overwrite existing file?", "Confirm Overwrite",
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    switch (response) {
                        case JOptionPane.OK_OPTION:
                            super.approveSelection();
                            return;
                        default: // do nothing otherwise
                            return;
                    }
                }
                super.approveSelection();
            }
        };
        saveRunFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
        saveRunFileChooser.setDialogTitle(dialogTitle);
        saveRunFileChooser.setAcceptAllFileFilterUsed(acceptAllFilters);
        for (String[] s : fileFilters) {
            saveRunFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(s[0],
                    Arrays.copyOfRange(s, 1, s.length)));
        }
        saveRunFileChooser.setFileFilter(saveRunFileChooser.getFileFilter());
        saveRunFileChooser.setSelectedFile(new File(saveFileName));
        int result = saveRunFileChooser.showSaveDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            return saveRunFileChooser;
        }
        return null;
    }

    public static File useFileChooserDialog(Component parent, List<String[]> fileFilters, String dialogTitle,
                                            boolean acceptAllFilters) {
        // launch filechooser
        JFileChooser openFileChooser = new JFileChooser(new File(".")) {
            @Override
            public void approveSelection() {
                File openFile = this.getSelectedFile();
                if (openFile.exists()) {
                    super.approveSelection();
                }
                else {
                    JOptionPane.showConfirmDialog(this,
                            "Couldn't find the specified file.", "File Not Found",
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        // use this to limit accepted files to only project files (false)
        openFileChooser.setAcceptAllFileFilterUsed(acceptAllFilters);
        openFileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
        openFileChooser.setDialogTitle(dialogTitle);
        for (String[] s : fileFilters) {
            openFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(s[0], Arrays.copyOfRange(s, 1, s.length)));
        }
        int result = openFileChooser.showOpenDialog(parent);
        if (result == JFileChooser.APPROVE_OPTION) {
            File f = openFileChooser.getSelectedFile();
            return f;
        }
        return null;
    }
    
    public static String readFileContents(File f){
        String contents = "";
        try {
            contents = new String(Files.readAllBytes(Paths.get(f.getCanonicalPath())));
        }
        catch (FileNotFoundException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contents;
    }
}
