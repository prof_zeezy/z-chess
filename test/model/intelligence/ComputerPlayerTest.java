/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.intelligence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.BoardSquare;
import model.ChessBoard;
import model.chesspiece.ChessPiece;
import model.chesspiece.Pawn;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author e.ubachukwu
 */
public class ComputerPlayerTest {

    int[] filesSummary1 = new int[8];
    int[] filesSummary2 = new int[8];
    int[] filesSummary3 = new int[8];
    int[] filesSummary4 = new int[8];
    int[] filesSummary5 = new int[8];
    int[] filesSummary6 = new int[8];
    int[] filesSummary7 = new int[8];
    int[] filesSummary8 = new int[8];
    int[] filesSummary9 = new int[8];
    static ComputerPlayer instance;
    static ChessBoard board;

    public ComputerPlayerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        instance = new ComputerPlayer();
        board = new ChessBoard(null);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // 1111 1111
        for (int i = 0; i < 8; i++) {
            filesSummary1[i] = 1;
        }

        // 0023 1001
        filesSummary2[0] = 0;
        filesSummary2[1] = 0;
        filesSummary2[2] = 2;
        filesSummary2[3] = 3;
        filesSummary2[4] = 1;
        filesSummary2[5] = 0;
        filesSummary2[6] = 0;
        filesSummary2[7] = 1;

        // 1200 1202
        filesSummary3[0] = 1;
        filesSummary3[1] = 2;
        filesSummary3[2] = 0;
        filesSummary3[3] = 0;
        filesSummary3[4] = 1;
        filesSummary3[5] = 2;
        filesSummary3[6] = 0;
        filesSummary3[7] = 2;

        // 1010 1010
        for (int i = 0; i < 8; i++) {
            if (i % 2 == 0) {
                filesSummary4[i] = 1;
            }
            else {
                filesSummary4[i] = 0;
            }
        }
        // 1100 2010
        filesSummary5[0] = 1;
        filesSummary5[1] = 1;
        filesSummary5[2] = 0;
        filesSummary5[3] = 0;
        filesSummary5[4] = 2;
        filesSummary5[5] = 0;
        filesSummary5[6] = 1;
        filesSummary5[7] = 0;

        // 1110 0010
        filesSummary6[0] = 1;
        filesSummary6[1] = 1;
        filesSummary6[2] = 1;
        filesSummary6[3] = 0;
        filesSummary6[4] = 0;
        filesSummary6[5] = 0;
        filesSummary6[6] = 1;
        filesSummary6[7] = 0;

        // 0123 2000
        filesSummary7[0] = 0;
        filesSummary7[1] = 1;
        filesSummary7[2] = 2;
        filesSummary7[3] = 3;
        filesSummary7[4] = 2;
        filesSummary7[5] = 0;
        filesSummary7[6] = 0;
        filesSummary7[7] = 0;

        // 4100 0010
        filesSummary8[0] = 4;
        filesSummary8[1] = 1;
        filesSummary8[2] = 0;
        filesSummary8[3] = 0;
        filesSummary8[4] = 0;
        filesSummary8[5] = 0;
        filesSummary8[6] = 1;
        filesSummary8[7] = 0;

        // 0300 0020
        filesSummary9[0] = 2;
        filesSummary9[1] = 0;
        filesSummary9[2] = 0;
        filesSummary9[3] = 0;
        filesSummary9[4] = 0;
        filesSummary9[5] = 0;
        filesSummary9[6] = 2;
        filesSummary9[7] = 0;
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of evaluatePawnChainLengthAverage method, of class ComputerPlayer.
     */
    @Test
    public void testEvaluatePawnChainLengthAverage() {
        System.out.println("evaluatePawnChain");
        // 0000 0000
        double result = instance.evaluatePawnChainLengthAverage(new int[8]);
        assertEquals(0, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary1);
        assertEquals(8, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary2);
        assertEquals(2, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary3);
        assertEquals(5.0 / 3, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary4);
        assertEquals(1, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary5);
        assertEquals(4.0 / 3, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary6);
        assertEquals(2, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary7);
        assertEquals(4, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary8);
        assertEquals(3.0 / 2, result, 0.00000001);

        result = instance.evaluatePawnChainLengthAverage(filesSummary9);
        assertEquals(1, result, 0.00000001);
    }

    @Test
    public void testCalculateBoardPieceValue() {
        Map<Class<? extends ChessPiece>, List<BoardSquare>[]> locs
                = board.getAllPieceLocations();
        assertEquals(38, instance.calculateBoardPieceValue(locs), 0);
        assertEquals(instance.calculateBoardPieceValue(locs),
                instance.calculateBoardPieceValue(locs), 0);
    }

    @Test
    public void testEvaluatePawnAdvancement() {
        // 7 on R1; 1 on R5
        int[] rankSummary = {0, 7, 0, 0, 1, 0, 0, 0};
        assertEquals(3.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        assertEquals(37.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);

        rankSummary = new int[]{0, 4, 3, 0, 1, 0, 0, 0};
        assertEquals(6.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        assertEquals(34.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);

        // flip the above
        rankSummary = new int[]{0, 0, 0, 1, 0, 3, 4, 0};
        assertEquals(34.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        assertEquals(6.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);

        rankSummary = new int[]{0, 3, 2, 1, 1, 0, 0, 0};
        assertEquals(7.0 / 7, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        assertEquals(28.0 / 7, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);

        rankSummary = new int[]{0, 1, 1, 0, 1, 0, 1, 0};
        assertEquals(9.0 / 4, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        assertEquals(11.0 / 4, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);

        // side-biased: WHITE
        rankSummary = new int[]{0, 1, 1, 0, 1, 0, 1, 2};
        assertEquals(21.0 / 6, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        rankSummary = new int[]{0, 0, 4, 0, 1, 2, 0, 1};
        assertEquals(21.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        rankSummary = new int[]{0, 1, 1, 2, 1, 0, 1, 2};
        assertEquals(25.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        rankSummary = new int[]{0, 1, 3, 0, 1, 0, 1, 1};
        assertEquals(17.0 / 7, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);
        rankSummary = new int[]{0, 0, 0, 0, 0, 0, 0, 8};
        assertEquals(48.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 2), 0.0000001);

        // BLACK
        rankSummary = new int[]{2, 1, 1, 0, 1, 0, 1, 0};
        assertEquals(23.0 / 6, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);
        rankSummary = new int[]{1, 1, 1, 0, 1, 0, 1, 0};
        assertEquals(17.0 / 5, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);
        rankSummary = new int[]{3, 0, 0, 2, 0, 2, 1, 0};
        assertEquals(26.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);
        rankSummary = new int[]{5, 0, 0, 0, 1, 0, 1, 0};
        assertEquals(32.0 / 7, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);
        rankSummary = new int[]{8, 0, 0, 0, 0, 0, 0, 0};
        assertEquals(48.0 / 8, instance.evaluatePawnAdvancement(rankSummary, 7), 0.0000001);
    }

    @Test
    public void testEvaluatePawnFileOccupancy() {
        // check a full file
        double result = instance.evaluatePawnFileOccupancy(filesSummary1);
        assertEquals(2.5, result, 0.0000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary2);
        assertEquals(1.1547619046619, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary3);
        assertEquals(1.458333333333, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary4);
        assertEquals(2.5, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary5);
        assertEquals(1.63333333333333, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary6);
        assertEquals(2.5, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary7);
        assertEquals(1.083333333333, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary8);
        assertEquals(0.95, result, 0.00000001);

        result = instance.evaluatePawnFileOccupancy(filesSummary9);
        assertEquals(1.8333333333333, result, 0.00000001);
    }

    @Test
    public void testEvaluatePawnChainStrength() {
        System.out.println("testEvaluatePawnChainStrength()");

        List<BoardSquare> squares = new ArrayList<>();
        double result = instance.evaluatePawnChainStrength(squares);
        assertEquals(0, result, 0.000000001);

        ChessPiece.Side[] sides = {ChessPiece.Side.WHITE, ChessPiece.Side.BLACK};
        for (int i = 0; i < 2; i++) {
            squares.add(new BoardSquare('A', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('F', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 2, new Pawn(sides[i])));
            result = instance.evaluatePawnChainStrength(squares);
            double expected[] = {4.0 / 5, 4.0 / 5};
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('A', 7, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('F', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 7, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 7, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 3.0 / 5;
            expected[1] = 3.0 / 5;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 3, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 0.0 / 5;
            expected[1] = 0.0 / 5;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 4, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 1.0 / 6;
            expected[1] = 1.0 / 6;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('A', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('F', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 4.0 / 3;
            expected[1] = 2.0 / 5;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('A', 7, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('F', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 1, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 6.0 / 2;
            expected[1] = 6.0 / 2;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('A', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 6, new Pawn(sides[i])));
            squares.add(new BoardSquare('F', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 5, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 5.0 / 3;
            expected[1] = 3.0 / 5;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('A', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 0.0 / 5;
            expected[1] = 0.0 / 5;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();

            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('B', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
            squares.add(new BoardSquare('D', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 3, new Pawn(sides[i])));
            squares.add(new BoardSquare('H', 4, new Pawn(sides[i])));

            result = instance.evaluatePawnChainStrength(squares);
            expected[0] = 1.0 / 8;
            expected[1] = 2.0 / 7;
            assertEquals(expected[i], result, 0.000000001);
            squares.clear();
        }

    }

}
