/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Stack;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author e.ubachukwu
 */
public class ChessBoardTest {

    static ChessBoard instance;

    public ChessBoardTest() {

    }

    @BeforeClass
    public static void setUpClass() {
        instance = new ChessBoard(null);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
//
//    /**
//     * Test of getKingEvadingMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetKingEvadingMoves() {
//        System.out.println("getKingEvadingMoves");
//        BoardSquare kingSquare = null;
//        List<BoardSquare> attackingSquares = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getKingEvadingMoves(kingSquare, attackingSquares);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getPieceLocation method, of class ChessBoard.
//     */
//    @Test
//    public void testGetPieceLocation() {
//        System.out.println("getPieceLocation");
//        Class<? extends ChessPiece> type = null;
//        ChessPiece.Side side = null;
//        ChessBoard instance = null;
//        List<BoardSquare> expResult = null;
//        List<BoardSquare> result = instance.getPieceLocation(type, side);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getKingShieldingMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetKingShieldingMoves() {
//        System.out.println("getKingShieldingMoves");
//        BoardSquare kingSquare = null;
//        List<BoardSquare> attackingSquares = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getKingShieldingMoves(kingSquare, attackingSquares);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getKingAttackerCapturingMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetKingAttackerCapturingMoves() {
//        System.out.println("getKingAttackerCapturingMoves");
//        ChessPiece.Side kingSide = null;
//        List<BoardSquare> attackingSquares = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getKingAttackerCapturingMoves(kingSide, attackingSquares);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getKingAttackerPositions method, of class ChessBoard.
//     */
//    @Test
//    public void testGetKingAttackers() {
//        System.out.println("getKingAttackerPositions");
//        ChessPiece.Side kingSide = null;
//        ChessBoard instance = null;
//        Map<ChessPiece, BoardSquare> expResult = null;
//        Map<ChessPiece, BoardSquare> result = instance.getKingAttackerPositions(kingSide);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of redoMove method, of class ChessBoard.
//     */
//    @Test
//    public void testRedoMove() {
//        System.out.println("redoMove");
//        ChessBoard instance = null;
//        ChessMove expResult = null;
//        ChessMove result = instance.redoMove();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of undoMove method, of class ChessBoard.
//     */
//    @Test
//    public void testUndoMove() {
//        System.out.println("undoMove");
//        ChessBoard instance = null;
//        instance.undoMove();
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of isRepetition method, of class ChessBoard.
     */
    @Test
    public void testIsRepetition() {
        System.out.println("isRepetition");

        Stack<MovePlyProposal> repeated_5 = new Stack<>();
        // first move - no repeat
        repeated_5.push(new MovePlyProposal("e1", "e4"));
        repeated_5.push(new MovePlyProposal("d5", "d4"));
        // last moves
        repeated_5.push(new MovePlyProposal("e4", "e5"));
        repeated_5.push(new MovePlyProposal("d4", "d5"));
        repeated_5.push(new MovePlyProposal("e5", "e4"));
        repeated_5.push(new MovePlyProposal("d5", "d4"));
        
        repeated_5.push(new MovePlyProposal("e4", "e5"));
        repeated_5.push(new MovePlyProposal("d4", "d5"));
        repeated_5.push(new MovePlyProposal("e5", "e4"));
        repeated_5.push(new MovePlyProposal("d5", "d4"));
        
        repeated_5.push(new MovePlyProposal("e4", "e5"));
        repeated_5.push(new MovePlyProposal("d4", "d5"));
        repeated_5.push(new MovePlyProposal("e5", "e4"));
        repeated_5.push(new MovePlyProposal("d5", "d4"));
        
        Stack<MovePlyProposal> notRepeated_5 = new Stack<>();
        
        notRepeated_5.push(new MovePlyProposal("e4", "e5"));
        notRepeated_5.push(new MovePlyProposal("d1", "d5"));
        notRepeated_5.push(new MovePlyProposal("f7", "f2"));
        notRepeated_5.push(new MovePlyProposal("c1", "h1"));
        
        notRepeated_5.push(new MovePlyProposal("a1", "h8"));
        notRepeated_5.push(new MovePlyProposal("g3", "h6"));
        notRepeated_5.push(new MovePlyProposal("d8", "b6"));
        notRepeated_5.push(new MovePlyProposal("e4", "e5"));
        
        notRepeated_5.push(new MovePlyProposal("a6", "h8"));
        notRepeated_5.push(new MovePlyProposal("g3", "b6"));
        notRepeated_5.push(new MovePlyProposal("d8", "c6"));
        notRepeated_5.push(new MovePlyProposal("e3", "e5"));
        
        Stack<MovePlyProposal> repeatedSide1_5 = new Stack<>();
        
        repeatedSide1_5.push(new MovePlyProposal("e4", "e5"));
        repeatedSide1_5.push(new MovePlyProposal("f7", "f2"));
        repeatedSide1_5.push(new MovePlyProposal("e5", "e4"));
        repeatedSide1_5.push(new MovePlyProposal("d5", "d4"));
        
        repeatedSide1_5.push(new MovePlyProposal("e4", "e5"));
        repeatedSide1_5.push(new MovePlyProposal("d4", "d5"));
        repeatedSide1_5.push(new MovePlyProposal("e5", "e4"));
        repeatedSide1_5.push(new MovePlyProposal("g3", "h6"));
        
        repeatedSide1_5.push(new MovePlyProposal("e4", "e5"));
        repeatedSide1_5.push(new MovePlyProposal("d4", "d5"));
        repeatedSide1_5.push(new MovePlyProposal("e5", "e4"));
        repeatedSide1_5.push(new MovePlyProposal("g3", "h6"));
        
        Stack<MovePlyProposal> repeatedSide2_5 = new Stack<>();
        
        repeatedSide2_5.push(new MovePlyProposal("e4", "e5"));
        repeatedSide2_5.push(new MovePlyProposal("d4", "d5"));
        repeatedSide2_5.push(new MovePlyProposal("e5", "e4"));
        repeatedSide2_5.push(new MovePlyProposal("d5", "d4"));
        
        repeatedSide2_5.push(new MovePlyProposal("e5", "e1"));
        repeatedSide2_5.push(new MovePlyProposal("d4", "d5"));
        repeatedSide2_5.push(new MovePlyProposal("e5", "e4"));
        repeatedSide2_5.push(new MovePlyProposal("d5", "d4"));
        
        repeatedSide2_5.push(new MovePlyProposal("e4", "e6"));
        repeatedSide2_5.push(new MovePlyProposal("d4", "d5"));
        repeatedSide2_5.push(new MovePlyProposal("a1", "a5"));
        repeatedSide2_5.push(new MovePlyProposal("d5", "d4"));

        // tests ---------------
        instance.setMoveHistory(repeated_5);
        assertEquals(true, instance.isRepetition(1));
        assertEquals(true, instance.isRepetition(2));
        assertEquals(true, instance.isRepetition(3));
        assertEquals(true, instance.isRepetition(4));
        assertEquals(true, instance.isRepetition(5));
        assertEquals(false, instance.isRepetition(6));
        assertEquals(false, instance.isRepetition(7));
        
        instance.setMoveHistory(notRepeated_5);
        assertEquals(false, instance.isRepetition(1));
        assertEquals(false, instance.isRepetition(2));
        assertEquals(false, instance.isRepetition(3));
        assertEquals(false, instance.isRepetition(4));
        assertEquals(false, instance.isRepetition(5));
        assertEquals(false, instance.isRepetition(6));
        
        instance.setMoveHistory(repeatedSide1_5);
        assertEquals(false, instance.isRepetition(1));
        assertEquals(false, instance.isRepetition(2));
        assertEquals(false, instance.isRepetition(3));
        assertEquals(false, instance.isRepetition(4));
        assertEquals(false, instance.isRepetition(5));
        assertEquals(false, instance.isRepetition(6));
        
        instance.setMoveHistory(repeatedSide2_5);
        assertEquals(false, instance.isRepetition(1));
        assertEquals(false, instance.isRepetition(2));
        assertEquals(false, instance.isRepetition(3));
        assertEquals(false, instance.isRepetition(4));
        assertEquals(false, instance.isRepetition(5));
        assertEquals(false, instance.isRepetition(6));
    }
    
//    /**
//     * Test of movePiece method, of class ChessBoard.
//     */
//    @Test
//    public void testMovePiece() {
//        System.out.println("movePiece");
//        MovePlyProposal move = null;
//        String time = "";
//        ChessBoard instance = null;
//        boolean expResult = false;
//        boolean result = instance.movePiece(move, time);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getCheckResolutionMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetCheckResolutionMoves() {
//        System.out.println("getCheckResolutionMoves");
//        ChessBoard instance = null;
//        List[] expResult = null;
//        List[] result = instance.getCheckResolutionMoves();
//        assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getPossibleMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetPossibleMoves_0args() {
//        System.out.println("getPossibleMoves");
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getPossibleMoves();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getPossibleMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetPossibleMoves_List_List() {
//        System.out.println("getPossibleMoves");
//        List<String> excudedFromSquares = null;
//        List<String> excludedToSquares = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getPossibleMoves(excudedFromSquares, excludedToSquares);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getPossibleMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetPossibleMoves_3args() {
//        System.out.println("getPossibleMoves");
//        List<MovePlyProposal> excludedMoves = null;
//        List<String> excludedFromSquares = null;
//        List<String> excludedToSquares = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getPossibleMoves(excludedMoves, excludedFromSquares, excludedToSquares);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getPossibleMovesByPiece method, of class ChessBoard.
//     */
//    @Test
//    public void testGetPossibleMovesByPiece() {
//        System.out.println("getPossibleMovesByPiece");
//        BoardSquare pieceSquare = null;
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getPossibleMovesByPiece(pieceSquare);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of isKingCheck method, of class ChessBoard.
//     */
//    @Test
//    public void testIsKingCheck() {
//        System.out.println("isKingCheck");
//        ChessBoard instance = null;
//        boolean expResult = false;
//        boolean result = instance.isKingCheck();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getAllCheckResolutionMoves method, of class ChessBoard.
//     */
//    @Test
//    public void testGetAllCheckResolutionMoves() {
//        System.out.println("getAllCheckResolutionMoves");
//        ChessBoard instance = null;
//        List<MovePlyProposal> expResult = null;
//        List<MovePlyProposal> result = instance.getAllCheckResolutionMoves();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

}
