/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.chesspiece;

import java.util.List;
import model.BoardSquare;
import model.ChessBoard;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author e.ubachukwu
 */
public class QueenTest {

    static ChessBoard board;

    public QueenTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        board = new ChessBoard(null);
    }

    @After
    public void tearDown() {
    }
    /**
     * Test of isNormalMove method, of class Queen.
     */
    @Test
    public void testIsNormalMove() {
//        System.out.println("isNormalMove");
//        BoardSquare from = null;
//        BoardSquare to = null;
//        Queen board = null;
//        boolean expResult = false;
//        boolean result = board.isNormalMove(from, to);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of getBlockingSquares method, of class Queen.
     */
    @Test
    public void testGetBlockingSquares() {
        System.out.println("getBlockingSquares");
        BoardSquare from = null;
        BoardSquare to = null;
        Queen instance = null;
        List<BoardSquare> expResult = null;
        List<BoardSquare> result = instance.getBlockingSquares(from, to, QueenTest.board);
        assertEquals(expResult, result);
        System.out.println("testEvaluatePawnChainStrength()");
//
//        List<BoardSquare> squares = new ArrayList<>();
//        double result = board.evaluatePawnChainStrength(squares);
//        assertEquals(0, result, 0.000000001);
//
//        ChessPiece.Side[] sides = {ChessPiece.Side.WHITE, ChessPiece.Side.BLACK};
//        for (int i = 0; i < 2; i++) {
//            squares.add(new BoardSquare('A', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('F', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 2, new Pawn(sides[i])));
//            result = board.evaluatePawnChainStrength(squares);
//            double expected[] = {4.0 / 5, 4.0 / 5};
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('A', 7, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('F', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 7, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 7, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 3.0 / 5;
//            expected[1] = 3.0 / 5;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 3, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 0.0 / 5;
//            expected[1] = 0.0 / 5;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 4, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 1.0 / 6;
//            expected[1] = 1.0 / 6;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('A', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('F', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 4.0 / 3;
//            expected[1] = 2.0 / 5;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('A', 7, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('F', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 1, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 6.0 / 2;
//            expected[1] = 6.0 / 2;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('A', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 6, new Pawn(sides[i])));
//            squares.add(new BoardSquare('F', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 5, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 5.0 / 3;
//            expected[1] = 3.0 / 5;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('A', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('E', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('G', 4, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 0.0 / 5;
//            expected[1] = 0.0 / 5;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//
//            squares.add(new BoardSquare('B', 2, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('B', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('C', 5, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 4, new Pawn(sides[i])));
//            squares.add(new BoardSquare('D', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 3, new Pawn(sides[i])));
//            squares.add(new BoardSquare('H', 4, new Pawn(sides[i])));
//
//            result = board.evaluatePawnChainStrength(squares);
//            expected[0] = 1.0 / 8;
//            expected[1] = 2.0 / 7;
//            assertEquals(expected[i], result, 0.000000001);
//            squares.clear();
//        }
    }


}
